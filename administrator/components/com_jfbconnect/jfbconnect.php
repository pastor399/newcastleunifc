<?php
/**
 * @package		JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');

//JHtml::_('behavior.framework', false); // false = no Mootools More required

$document = JFactory::getDocument();
$document->addStyleSheet("components/com_jfbconnect/assets/default.css");
$document->addScript("components/com_jfbconnect/assets/jfbconnect-admin.js");

jimport('sourcecoast.utilities');
SCStringUtilities::loadLanguage('com_jfbconnect', JPATH_ADMINISTRATOR);

require_once (JPATH_COMPONENT . '/controller.php');

$view = JRequest::getCmd('controller', '');
if ($view == "")
	$view = JRequest::getCmd('view', '');

//Live Update
/*
require_once JPATH_ADMINISTRATOR . '/components/com_jfbconnect/liveupdate/liveupdate.php';
if(JRequest::getCmd('view','') == 'liveupdate')
{
    LiveUpdate::handleRequest();
    return;
}*/

if ($view != '' && $view != "jfbconnect") // Don't do this for the main landing page. Fix this system
{
    require_once (JPATH_COMPONENT . '/controllers/' . strtolower($view) . '.php');
    $controllerName = $view;
}
else
    $controllerName = "";


$classname = 'JFBConnectController' . ucfirst($controllerName);
$controller = new $classname();

$controller->execute(JRequest::getVar('task'));

if (JRequest::getCmd('tmpl') != 'component')
    include_once(JPATH_ADMINISTRATOR . '/components/com_jfbconnect/assets/footer/footer.php');

$controller->redirect();
