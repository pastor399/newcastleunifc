<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.html.sliders');
JHTML::_('behavior.tooltip');
?>
<div class="row-fluid">
    <div class="span9 autotune">
        <form method="post" id="adminForm" name="adminForm">
            <h1><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_BASICINFO_LABEL');?></h1>

            <p><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_BASICINFO_DESC');?></p>

            <div style="text-align:center; font-size:16px">
                <div style="clear:left">
                    <div style="width: 300px;float:left; text-align:right;"><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_BASICINFO_FB_APPID_LABEL');?></div>
                    <div style="width:300px;float:left; margin-left:15px;"><input type="text" name="facebook_app_id"
                                                                                  size="35"
                                                                                  style="font-weight:bold"
                                                                                  value="<?php echo $this->fbAppId; ?>"/>
                    </div>
                </div>
                <div style="clear:left">
                    <div style="width: 300px;float:left; text-align:right;"><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_BASICINFO_FB_SECRET_KEY_LABEL');?></div>
                    <div style="width:300px;float:left; margin-left:15px;"><input type="text" name="facebook_secret_key"
                                                                                  size="35" style="font-weight:bold"
                                                                                  value="<?php echo $this->fbSecretKey; ?>"/>
                    </div>
                </div>
                <div style="clear:left">
                    <div style="width: 300px;float:left; text-align:right;"><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_BASICINFO_SC_SUBSCRIBER_ID_LABEL');?></div>
                    <div style="width:300px;float:left; margin-left:15px;"><input type="text" name="subscriberId" size="35"
                                                                                  style="font-weight:bold"
                                                                                  value="<?php echo $this->subscriberId; ?>"/>
                    </div>
                </div>
                <div style="clear:both"></div>
                <input type="submit" value="Save" class="autotuneButton"/>
            </div>
            <br/>
            <ul>
                <li><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_BASICINFO_FB_KEYS_DESC');?>
                </li>
                <li><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_BASICINFO_SC_ID_DESC');?>
                    <ul>
                        <li><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_BASICINFO_SC_ID_DESC2');?>
                        </li>
                    </ul>
                </li>
            </ul>

            <input type="hidden" name="option" value="com_jfbconnect"/>
            <input type="hidden" name="view" value="autotune"/>
            <input type="hidden" name="task" value="saveBasicInfo"/>
        </form>
    </div>
</div>
