<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');


?>
<style>
    button {
        width: 170px;
        height: 70px;
        color: #FFFFFF;
        padding: 0px;
        font-weight: bold;
        font-size: 12px;
        float: left;
        margin: 0 20px;
    }
</style>
<div class="row-fluid">
    <div class="span9 autotune">
        <form method="post" id="adminForm" name="adminForm">
            <h1>New Application Detected</h1>

            <p><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_FBAPPNEW_DESC');?></p>
            <p style="height:80px">
                <button type="button" class="autotuneYes" onclick="Joomla.submitbutton('saveAppRecommendations');"><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_YES_AUTOCONFIGURE');?></button>
                <button type="button" class="autotuneNo" onclick="Joomla.submitbutton('fbapp');"><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_NO_AUTOCONFIGURE');?></button>
            </p>

            <p><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_NO_DESC');?></p>
            <input type="hidden" name="option" value="com_jfbconnect"/>
            <input type="hidden" name="view" value="autotune"/>
            <input type="hidden" name="task" value="saveBasicInfo"/>
        </form>
        <div style="clear:both"></div>
    </div>
</div>
