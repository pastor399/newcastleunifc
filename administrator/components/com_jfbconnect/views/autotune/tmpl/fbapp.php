<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');

$appConfig = $this->appConfig;

$boolOptions = array(
    JHTML::_('select.option', 'enabled', 'Enabled'),
    JHTML::_('select.option', 'disabled', 'Disabled'));
?>
<style>
div.current select {
    margin: 0px;
}

.autotuneButton {
    width: 200px;
}

.autotune_setting {
    width:200px;
    float:left;
}

.autotune_option {
    width:300px;
    float:left;
}

.autotune_description {
    width:9px;
}

.autotune .hasTip {
    background:none;
    color: #000000;
}
</style>
<div class="row-fluid">
    <div class="span9 autotune">
        <h1><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_FBAPP_LABEL');?></h1>
        <form method="post" id="adminForm" name="adminForm">
            <?php
            if (!isset($appConfig->group))
            {
                echo '<br/><p style="font-size:14px">Field data not found. You will need to make a successful connection to SourceCoast.com to load the most recent Facebook Application data.<br/>';
                echo '<p style="font-size:14px">Please check that your Subscriber ID is correct: <br/><strong>'.$this->subscriberId.'</strong> (<a href="index.php?option=com_jfbconnect&view=autotune&task=basicinfo">Change</a>)</p>';
                echo '<p>If the problem persists, please let us know in our <a href="http://www.sourcecoast.com/forums">support area</a></p>';
            }
            else
            {
                echo '<p>'.JText::_('COM_JFBCONNECT_AUTOTUNE_FBAPP_FB_INFO_DESC').'</p>';

                
                ?>
                <!-- Tab Header -->
                <div class="row-fluid">
                    <ul class="nav nav-tabs">
                        <?php
                        $activeStr = ' class="active"';
                        foreach ($appConfig->group as $group)
                        {
                            $group_name = preg_replace( '/\s+/', '', $group->name );
                            if ($group->numRecommendations == 0)
                                echo '<li'.$activeStr.'><a href="#'.$group_name.'" data-toggle="tab"><span class="autotuneGood">' . ucwords($group->name) . '</span></a></li>';
                            else
                                echo '<li'.$activeStr.'><a href="#'.$group_name.'" data-toggle="tab"><span class="autotuneBad">' . ucwords($group->name) . ' (' . $group->numRecommendations . ')</span></a></li>';

                            $activeStr = '';
                        }
                        ?>
                    </ul>
                </div>
                <div class="tab-content">
                <?php
                 //SC30

                 //SC16

                $activeStr = ' active';
                foreach ($appConfig->group as $group)
                {
                     //SC16

                    $group_name = preg_replace( '/\s+/', '', $group->name );
                    echo '<div class="tab-pane'.$activeStr.'" id="'.$group_name.'">';
                    $activeStr = '';
                    echo '<p>'.$group->description.'</p>';
                    ?>

                    <div class="config_row">
                        <div class="autotune_setting header"><strong><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_FBAPP_APPLICATION_SETTING_LABEL');?></strong></div>
                        <div class="autotune_option header"><strong><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_FBAPP_APP_SETTING_LABEL');?></strong></div>
                        <div class="autotune_option header"><strong><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_FBAPP_JFBC_RECOMMENDATION_LABEL');?></strong></div>
                        <div style="clear:both"></div>
                    </div>
                    <?php

                    foreach ($group->field as $field)
                    {
                        ?>
                        <div class="config_row">
                            <div class="autotune_setting hasTip" title="<?php echo $field->description; ?>"><?php echo $field->display ?></div>
                            <?php if ($field->type == 'image')
                            echo '<div class="autotune_option"><img src="' . $field->value . '" />&nbsp;</div>';
                        else if (isset($field->edit))
                        {
                            if ($field->type == 'text')
                                echo '<div class="autotune_option"><input type="text" name="' . $field->name . '" value="' . $field->value . '" size="45" /></div>';
                            else if ($field->type == 'array')
                                echo '<div class="autotune_option"><input type="text" name="' . $field->name . '" value="' . implode(', ', $field->value) . '" size="45" /></div>';
                            else if ($field->type == "bool")
                                echo '<div class="autotune_option">' . JHTML::_('select.genericlist', $boolOptions, $field->name, null, 'value', 'text', strtolower($field->value)) . '</div>';
                        }
                        else
                            echo '<div class="autotune_option">' . $field->value . '&nbsp;</div>';

                            $recStyle = $field->recommendMet ? 'autotuneGood' : 'autotuneBad';
                            ?>
                            <div class="autotune_option <?php echo $recStyle?>"><?php echo $field->recommend?>&nbsp;</div>
                            <div style="clear:both">
                            </div>
                        </div>
                        <?php
                    }

                    echo '</div>'; //tab-pane
                     //SC16
                }
                
                echo '</div>'; // tab-content
                 //SC30
                 //SC16
                ?>
                <br/>
                <p><strong><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_FBAPP_FBFETCHED_LABEL')?></strong><?php echo $this->appConfigUpdated; ?> | <strong><?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_FBAPP_SCFETCHED_LABEL');?></strong><?php echo $this->fieldsUpdated; ?></p>
                <div style="text-align: center">
                    <input type="button" value="Set All Recommendations" class="autotuneButton" onclick="Joomla.submitbutton('saveAppRecommendations');"/>
                    <input type="submit" value="Update Application" class="autotuneButton"/>
                </div>
                <br/>
                <?php echo JText::_('COM_JFBCONNECT_AUTOTUNE_FBAPP_FBFETCHED_DESC')?>

                <?php
            } // end of if/else for field descriptors loaded
            ?>
            <input type="hidden" name="option" value="com_jfbconnect"/>
            <input type="hidden" name="view" value="autotune"/>
            <input type="hidden" name="task" value="saveAppConfig"/>
        </form>
    </div>
</div>