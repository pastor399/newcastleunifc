<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.version');

$versionChecker = $this->versionChecker;
?>
<form method="post" id="adminForm" name="adminForm">
    <div>
        <div>
            <div id="cpanel" class="fltlft">
                <?php echo $this->addIcon('icon-48-action-sc.png', 'index.php?option=com_jfbconnect&view=opengraph&task=actions', JText::_('COM_JFBCONNECT_OPENGRAPH_ACTIONS'));?>
                <?php echo $this->addIcon('icon-48-object-sc.png', 'index.php?option=com_jfbconnect&view=opengraph&task=objects', JText::_('COM_JFBCONNECT_OPENGRAPH_OBJECTS'));?>
                <?php echo $this->addIcon('icon-48-activity-sc.png', 'index.php?option=com_jfbconnect&view=opengraph&task=activitylist', JText::_('COM_JFBCONNECT_OPENGRAPH_ACTIVITYLOGS'));?>
                <?php echo $this->addIcon('icon-48-config-sc.png', 'index.php?option=com_jfbconnect&view=opengraph&task=settings', JText::_('COM_JFBCONNECT_OPENGRAPH_CONFIGURATION'));?>
            </div>
            <div class="versions fltrt">
                <table>
                    <tr>
                        <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_OPENGRAPH_PLUGINS');?></th>
                        <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_INSTALLED');?></th>
                        <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_AVAILABLE');?></th>
                        <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_STATUS');?></th>
                    </tr>
                    <?php
                    // This is stupid, should update the sourcecoast.php library to not echo this out:
                    ob_start();
                    $versionChecker->_showVersionInfoRow('opengraph.content', 'plugin');
                    $versionChecker->_showVersionInfoRow('opengraph.custom', 'plugin');
                    $versionChecker->_showVersionInfoRow('opengraph.easyblog', 'plugin');
                    $versionChecker->_showVersionInfoRow('opengraph.jomsocial', 'plugin');
                    $versionChecker->_showVersionInfoRow('opengraph.jreviews', 'plugin');
                    $versionChecker->_showVersionInfoRow('opengraph.k2', 'plugin');
                    $plugins = ob_get_clean();
                    echo str_replace("OpenGraph - ", '', $plugins);
                    ?>
                </table>
            </div>
        </div>
        <div style="clear:both"/>
        <hr/>
        <div class="example">
            <p style="font-size:1.5em"><?php echo JText::sprintf('COM_JFBCONNECT_OPENGRAPH_CONFIGURATION_GUIDE_LINK', '<a
                    href="http://www.sourcecoast.com/jfbconnect/docs/facebook-open-graph-actions-for-joomla" target="_blank">', '</a>');?>
            </p>

            <div class="fltlft" style="font-size:1.091em">
                <img style="border:solid 1px grey;margin:9px;" class="fltrt" src="components/com_jfbconnect/assets/images/open-graph-example.png"/>

                <?php echo JText::_('COM_JFBCONNECT_OPENGRAPH_OVERVIEW_DESCRIPTION'); ?>

                <p style="font-size:1.5em"><?php echo JText::sprintf('COM_JFBCONNECT_OPENGRAPH_CONFIGURATION_GUIDE_LINK', '<a
                        href="http://www.sourcecoast.com/jfbconnect/docs/facebook-open-graph-actions-for-joomla" target="_blank">', '</a>');?>
                </p>
            </div>
        </div>

    </div>
    <input type="hidden" name="option" value="com_jfbconnect"/>
    <input type="hidden" name="controller" value="opengraph"/>
    <input type="hidden" name="task" value=""/>
    <?php echo JHTML::_('form.token'); ?>
</form>