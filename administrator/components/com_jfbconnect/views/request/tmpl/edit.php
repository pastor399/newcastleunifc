<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
?>

<style type="text/css">
    div.config_setting {
        width: 150px;
    }
    div.config_option {
        width: 450px;
    }
</style>

<form action="index.php" method="post" name="adminForm" id="adminForm">
    <div>
        <div style="float:left; width:75%">
            <div class="config_row">
                <div class="config_setting header"><?php echo JText::_("COM_JFBCONNECT_REQUEST_REQUEST_SETTING");?></div>
                <div class="config_option header"><?php echo JText::_("COM_JFBCONNECT_REQUEST_OPTIONS");?></div>
                <div style="clear:both"></div>
                <div><?php echo JText::_("COM_JFBCONNECT_REQUEST_NOTE");?></div>
            </div>
            <div class="config_row">
                <div class="config_setting hasTip"
                     title="<?php echo JText::_('COM_JFBCONNECT_REQUEST_TITLE_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_REQUEST_TITLE_LABEL');?>:</div>
                <div class="config_option">
                    <input id="title" type="text" size="60" name="title" maxlength="50"
                                   value="<?php echo $this->request->title; ?>">
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="config_row">
                <div class="config_setting hasTip"
                     title="<?php echo JText::_('COM_JFBCONNECT_REQUEST_MESSAGE_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_REQUEST_MESSAGE_LABEL');?>:</div>
                <div class="config_option">
                    <textarea id="message" name="message" rows="3" cols="55"
                                      maxlength="250"><?php echo $this->request->message; ?></textarea>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="config_row">
                <div class="config_setting hasTip"
                     title="<?php echo JText::_('COM_JFBCONNECT_REQUEST_DESTINATION_URL_DESC');?>"><?php echo JText::_("COM_JFBCONNECT_REQUEST_DESTINATION_URL_LABEL");?>:</div>
                <div class="config_option">
                    <input id="destination_url" type="text" size="60" maxlength="250" name="destination_url"
                                   value="<?php echo $this->request->destination_url; ?>">
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="config_row">
                <div class="config_setting hasTip"
                     title="<?php echo JText::_('COM_JFBCONNECT_REQUEST_THANKYOU_URL_DESC');?>"><?php echo JText::_("COM_JFBCONNECT_REQUEST_THANKYOU_URL_LABEL");?>:</div>
                <div class="config_option">
                    <input id="thanks_url" type="text" size="60" maxlength="250" name="thanks_url"
                                   value="<?php echo $this->request->thanks_url; ?>">
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="config_row">
                <div class="config_setting hasTip"
                     title='<?php echo JText::_("COM_JFBCONNECT_REQUEST_REDIRECT_DESC");?>'><?php echo JText::_("COM_JFBCONNECT_REQUEST_REDIRECT_LABEL");?>:</div>
                <div class="config_option">
                    <input id="breakout_canvas" type="checkbox" name="breakout_canvas"
                                   <?php echo $this->request->breakout_canvas ? 'checked="checked"' : ""; ?>">
                </div>
                <div style="clear:both"></div>
            </div>
            <br/>
            <p><?php echo JText::_("COM_JFBCONNECT_REQUEST_DESC");?></p>
            <p><strong><?php echo JText::_("COM_JFBCONNECT_REQUEST_REQUEST_TAG_EXAMPLES");?></strong>
            <ul>
                <li>{JFBCRequest request_id=1 link_text=Invite Friends}</li>
                <li>{JFBCRequest request_id=1 link_image=http://www.sourcecoast.com/images/stories/extensions/jfbconnect/home_jfbconn.png}</li>
            </ul>
            </p>
        </div>
        <div style="float:left; width:25%">
            <table style="width:100%; border:1px dashed silver; padding: 5px; margin: 8px 0 10px 0;">
                    <tbody>
                    <tr>
                        <td width="75px"><strong><?php echo JText::_("COM_JFBCONNECT_REQUEST_REQUEST_ID");?>:</strong></td>
                        <td><?php echo $this->request->id ?></td>
                    </tr>
                    <tr>
                        <td><strong><?php echo JText::_("COM_JFBCONNECT_REQUEST_PUBLISHED");?>:</strong></td>
                        <td><input type="radio" name="published" value="0" id="published0" <?php echo !$this->request->published ? 'checked="checked"' : ""; ?>><label for="published0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                            <input type="radio" name="published" value="1" id="published1" <?php echo $this->request->published ? 'checked="checked"' : ""; ?>><label for="published1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                        </td>
                    </tr>
                    <tr>
                        <td><strong><?php echo JText::_("COM_JFBCONNECT_REQUEST_CREATED");?>:</strong></td>
                        <td><?php echo $this->request->created; ?></td>
                    </tr>
                    <tr>
                        <td><strong><?php echo JText::_("COM_JFBCONNECT_REQUEST_MODIFIED");?>:</strong></td>
                        <td><?php echo $this->request->modified; ?></td>
                    </tr>
                    </tbody>
                </table>
                <table style="width:100%; border:1px dashed silver; padding: 5px; margin: 8px 0 10px 0;>
                    <tbody>
                    <tr colspan="2">
                        <td width="125"><strong><?php echo JText::_("COM_JFBCONNECT_REQUEST_NOTIFICATIONS");?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?php echo JText::_("COM_JFBCONNECT_REQUEST_TOTAL");?>:</strong></td>
                        <td><?php echo $this->totalNotifications;?></td>
                    </tr>
                    <tr>
                        <td><strong><?php echo JText::_("COM_JFBCONNECT_REQUEST_PENDING");?>:</strong></td>
                        <td><?php echo $this->pendingNotifications;?></td>
                    </tr>
                    <tr>
                        <td><strong><?php echo JText::_("COM_JFBCONNECT_REQUEST_READ");?>:</strong></td>
                        <td><?php echo $this->readNotifications;?></td>
                    </tr>
                    <tr>
                        <td><strong><?php echo JText::_("COM_JFBCONNECT_REQUEST_EXPIRED");?>:</strong></td>
                        <td><?php echo $this->expiredNotifications;?></td>
                    </tr>
                    <tr>
                        <td style="text-align: center"><a href="<?php echo JRoute::_('index.php?option=com_jfbconnect&controller=notification&task=display&requestid='.$this->request->id);?>"><?php echo JText::_("COM_JFBCONNECT_REQUEST_SEE_ALL");?></a></td>
                    </tr>
                    </tbody>
                </table>
        </div>
        <div style="clear:both"></div>
    </div>
    <input type="hidden" name="option" value="com_jfbconnect"/>
    <input type="hidden" name="view" value="<?php echo JRequest::getVar('view'); ?>"/>
    <input type="hidden" name="task" value="apply"/>
    <input type="hidden" name="id" value="<?php echo $this->request->id; ?>" />
    <?php echo JHTML::_('form.token'); ?>
</form>