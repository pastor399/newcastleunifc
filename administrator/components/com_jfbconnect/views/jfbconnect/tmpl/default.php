<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.version');
jimport('sourcecoast.utilities');

$configModel = $this->configModel;
$autotuneModel = $this->autotuneModel;
$jfbcLibrary = $this->jfbcLibrary;
$usermapModel = $this->usermapModel;
$appStats = $this->appStats;
$versionChecker = $this->versionChecker;
?>

<!--<div style="float: left; padding: 0 20px 0 0"><img src="<?php echo JURI::root() ?>components/com_jfbconnect/images/jfbconn.png" /></div>-->
<div style="margin:0 0 0 10px; padding: 0 10px 10px 10px">
    <h2><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_FB_APP');?></h2>

    <div style="float:left;width:550px">
        <fieldset style="padding:5px">
            <legend><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_CONFIG_INFO');?></legend>
            <div>
                <div style="float:left;width:80px">
                    <?php
                    $logoUrl = $autotuneModel->getAppConfigField('logo_url')->get('value');
                    if ($logoUrl != "")
                        echo '<img src="' . $logoUrl . '" />';
                    else
                        echo "<?php echo JText::_('COM_JFBCONNECT_OVERVIEW_NO_APP_LOGO_SET');?>";
                    ?>
                </div>
                <div style="float: left; margin: 0 0 0 20px">
                    <?php
                    if ($jfbcLibrary->facebookAppId)
                    {
                        ?>
                        <p style="margin:0 0 5px 0;"><b><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_APP_NAME_LABEL');?>: </b><?php echo $autotuneModel->getAppConfigField('name')->get('value'); ?></p>
                        <?php
                    }
                    else
                    {
                        ?>
                        <p style="margin:0 0 5px 0;"><b><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_APP_NAME_LABEL');?>:<br/> <span style="color:#FF0000"><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_APP_NAME_DESC');?></span></b>
                        </p>
                        <?php } ?>
                    <p style="margin:0 0 5px 0;"><b><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_SITE_URL');?>: </b><?php echo $autotuneModel->getAppConfigField('website_url')->get('value'); ?></p>
                    <?php $appDomains = $autotuneModel->getAppConfigField('app_domains')->get('value');
                    if (is_array($appDomains))
                    {
                        ?>
                        <p style="margin:0 0 5px 0;"><b>Site
                            Domain(s): </b><?php echo implode(', ', $appDomains); ?></p>
                        <?php
                        if (!$autotuneModel->getAppConfigField('app_domains')->get('recommendMet'))
                        {
                            if (!$autotuneModel->getAppConfigField('connect_url')->get('recommendMet'))
                            {
                                print "<b style=\"color:#FF1410\">\n";
                                print '<?php echo JText::_("COM_JFBCONNECT_OVERVIEW_AUTOTUNE_WARN");?><a href="index.php?option=com_jfbconnect&view=autotune"><?php echo JText::_("COM_JFBCONNECT_OVERVIEW_AUTOTUNE_WIZARD_LABEL");?></a>.';
                                print "</b><br/>";
                            }
                        }
                    } else
                    {
                        ?>
                        <p style="margin:0 0 5px 0;"><b><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_SITE_DOMAINS');?>:</p>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </fieldset>
    </div>
    <div style="float:left; margin-left:15px;width: 300px">
        <fieldset style="padding: 5px;">
            <legend><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_STATISTICS');?></legend>
            <p style="margin:0 0 5px 0;"><b><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_TOTAL_CONNECT_USERS');?>:</b> <?php echo $usermapModel->getTotalMappings(); ?>
            </p>

            <p style="margin:0 0 5px 0;"><b><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_ACTIVE_MONTHLY_USERS');?>:</b> <?php echo $appStats['monthly_active_users']; ?>
            </p>

            <p style="margin:0 0 5px 0;"><b><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_ACTIVE_WEEKLY_USERS');?>:</b> <?php echo $appStats['weekly_active_users']; ?></p>

            <p style="margin:0 0 5px 0;"><b><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_ACTIVE_DAILY_USERS');?>:</b> <?php echo $appStats['daily_active_users']; ?></p>

            <p><a target="_BLANK"
                  href="http://www.facebook.com/insights/?sk=ao_<?php echo $jfbcLibrary->facebookAppId; ?>"><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_FB_INSIGHTS_LABEL');?></a> <?php echo JText::_('COM_JFBCONNECT_OVERVIEW_FB_INSIGHTS_DESC');?></p>
        </fieldset>
    </div>
    <!--<div style="float:left; margin-left:15px;text-align:center">
        <?php
        //echo LiveUpdate::getIcon();
        ?>
    </div>-->
    <div style="clear:both"></div>
    <div style="float:left">
        <h2><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_EXTENSION_CHECK');?></h2>
        <?php
        $app = JFactory::getApplication();
        $version = new JVersion();
        $versionStr = $version->getShortVersion();
        $found15Version = SCStringUtilities::startsWith($versionStr, "1.5.");
        if ($found15Version)
            $app->enqueueMessage("<?php echo JText::_('COM_JFBCONNECT_OVERVIEW_INCORRECT_VERSION_WARN');?>", "error");
        ?>
        <div style="float:left; margin: 0 10px">
            <table>
                <tr>
                    <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_REQ_EXTENSIONS');?></th>
                    <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_INSTALLED');?></th>
                    <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_AVAILABLE');?></th>
                    <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_STATUS');?></th>
                </tr>
                <?php
                echo $versionChecker->_showVersionInfoRow('com_jfbconnect', 'component');
                //echo $versionChecker->_showVersionInfoRow('sourcecoast', 'library');
                echo $versionChecker->_showVersionInfoRow('mod_sclogin', 'module');
                echo $versionChecker->_showVersionInfoRow('authentication.jfbconnectauth', 'plugin');
                echo $versionChecker->_showVersionInfoRow('system.jfbcsystem', 'plugin');
                echo $versionChecker->_showVersionInfoRow('user.jfbconnectuser', 'plugin');
                ?>
            </table>
        </div>
        <div style="float:left; margin: 0 10px">
            <table>
                <tr>
                    <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_SOCIAL_EXTENSIONS');?></th>
                    <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_INSTALLED');?></th>
                    <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_AVAILABLE');?></th>
                    <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_STATUS');?></th>
                </tr>
                <?php
                echo $versionChecker->_showVersionInfoRow('content.jfbccontent', 'plugin');
                echo $versionChecker->_showVersionInfoRow('mod_jfbcfan', 'module');
                echo $versionChecker->_showVersionInfoRow('mod_jfbclike', 'module');
                echo $versionChecker->_showVersionInfoRow('mod_jfbcsend', 'module');
                echo $versionChecker->_showVersionInfoRow('mod_jfbccomments', 'module');
                echo $versionChecker->_showVersionInfoRow('mod_jfbcrecommendations', 'module');
                echo $versionChecker->_showVersionInfoRow('mod_jfbcrecommendationsbar', 'module');
                echo $versionChecker->_showVersionInfoRow('mod_jfbcfriends', 'module');
                echo $versionChecker->_showVersionInfoRow('mod_jfbcfeed', 'module');
                echo $versionChecker->_showVersionInfoRow('mod_jfbcrequest', 'module');
                echo $versionChecker->_showVersionInfoRow('mod_jfbcfollow', 'module');
                ?>
            </table>
        </div>
        <div style="float:left; margin: 0 10px">
            <table>
                <tr>
                    <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_PROFILE_INTEGRATION');?></th>
                    <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_INSTALLED');?></th>
                    <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_AVAILABLE');?></th>
                    <th><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_STATUS');?></th>
                </tr>
                <?php
                echo $versionChecker->_showVersionInfoRow('socialprofiles.communitybuilder', 'plugin');
                echo $versionChecker->_showVersionInfoRow('socialprofiles.jomsocial', 'plugin');
                echo $versionChecker->_showVersionInfoRow('socialprofiles.kunena', 'plugin');
                echo $versionChecker->_showVersionInfoRow('socialprofiles.k2', 'plugin');
                echo $versionChecker->_showVersionInfoRow('socialprofiles.virtuemart2', 'plugin');

                echo $versionChecker->_showVersionInfoRow('community.jfbcjsactivity', 'plugin');
                ?>
            </table>
        </div>
    </div>
    <div style="clear:both"></div>
    <img alt="<?php echo JText::_('COM_JFBCONNECT_OVERVIEW_INSTALLED_PUBLISHED_DESC');?>" src="components/com_jfbconnect/assets/images/icon-16-allow.png" width="10"
         height="10"/> - <?php echo JText::_('COM_JFBCONNECT_OVERVIEW_INSTALLED_PUBLISHED');?> |
    <img alt="<?php echo JText::_('COM_JFBCONNECT_OVERVIEW_INSTALLED_UNPUBLISHED_DESC');?>" src="components/com_jfbconnect/assets/images/icon-16-notice-note.png" width="10"
         height="10"/> - <?php echo JText::_('COM_JFBCONNECT_OVERVIEW_NOT_PUBLISHED');?> |
    <img alt="<?php echo JText::_('COM_JFBCONNECT_OVERVIEW_NOT_INSTALLED');?>" src="components/com_jfbconnect/assets/images/icon-16-deny.png" width="10" height="10"/> - <?php echo JText::_('COM_JFBCONNECT_OVERVIEW_NOT_INSTALLED');?>
</div>



<div style="margin:0 0 0 10px; padding: 0 10px 10px 10px">
    <h2><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_ADDITIONAL_INFO_SUPPORT');?></h2>
    <ul>
        <li><a target="_blank" href="http://www.sourcecoast.com/jfbconnect/docs/configuration-guide"><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_JFBC_SETUP_INSTRUCTIONS');?></a></li>
        <li><a target="_blank" href="http://developers.facebook.com/"><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_FB_DEVELOPER_PORTAL');?></a></li>
        <li><a target="_blank" href="http://developers.facebook.com/policy/"><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_FB_PLATFORM_POLICIES');?></a></li>
    </ul>

    <p><?php echo JText::_('COM_JFBCONNECT_OVERVIEW_CHANGELOG_DESC');?></p>
</div>
<div style="clear: both"></div>

<form method="post" id="adminForm" name="adminForm">
    <input type="hidden" name="option" value="com_jfbconnect"/>
    <input type="hidden" name="task" value=""/>
</form>
