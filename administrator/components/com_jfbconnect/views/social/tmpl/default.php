<?php
/**
 * @package		JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filesystem.folder');
jimport('sourcecoast.utilities');
JHTML::_('behavior.tooltip');
$model = $this->model;
$k2IsInstalled = SCSocialUtilities::isJoomlaComponentEnabled('com_k2');
?>
<script type="text/javascript">
    function toggleHide(rowId, styleType)
    {
        document.getElementById(rowId).style.display = styleType;
    }
</script>
<style type="text/css">
    div.config_setting {
        width: 225px;
    }
    div.config_option {
        width: 250px;
    }
    div.config_setting_option{
        width:475px;
    }
</style>

<form method="post" id="adminForm" name="adminForm">
<?php

?>
    <div class="row-fluid">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#social_content_comment" data-toggle="tab"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_MENU_CONTENT_COMMENTS');?></a></li>
            <li><a href="#social_content_like" data-toggle="tab"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_MENU_CONTENT_LIKE');?></a></li>

            <?php
            if($k2IsInstalled)
            {
                echo '<li><a href="#social_content_k2_comment" data-toggle="tab">'.JText::_('COM_JFBCONNECT_SOCIAL_MENU_CONTENT_K2_COMMENTS').'</a></li>';
                echo '<li><a href="#social_content_k2_like" data-toggle="tab">'.JText::_('COM_JFBCONNECT_SOCIAL_MENU_CONTENT_K2_LIKE').'</a></li>';
            }
            ?>
            <li><a href="#social_notifications" data-toggle="tab"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_MENU_NOTIFICATIONS');?></a></li>
            <li><a href="#social_misc" data-toggle="tab"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_MENU_MISC');?></a></li>
            <li><a href="#social_examples" data-toggle="tab"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_MENU_EXAMPLES');?></a></li>
        </ul>
    </div>
<div class="tab-content">
<?php
 //SC30
 //SC16
?>
    <!-- Begin Tabs -->
    <?php
     //SC16
    ?>
    <div class="tab-pane active" id="social_content_comment">
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ARTICLE_SETTING_LABEL");?></div>
            <div class="config_option header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_OPTIONS");?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_COMMENTS_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COMMENTS_LABEL");?>:</div>
            <div class="config_option">
                <input type="text" name="social_article_comment_max_num" value="<?php echo $model->getSetting('social_article_comment_max_num') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_WIDTH_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_WIDTH_LABEL");?>:</div>
            <div class="config_option">
                <input type="text" name="social_article_comment_width" value="<?php echo $model->getSetting('social_article_comment_width') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_article_comment_color_scheme" class="radio">
                    <input type="radio" id="social_article_comment_color_schemeL" name="social_article_comment_color_scheme" value="light" <?php echo $model->getSetting('social_article_comment_color_scheme') == 'light' ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_comment_color_schemeL"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LIGHT");?></label>
                    <input type="radio" id="social_article_comment_color_schemeD" name="social_article_comment_color_scheme" value="dark" <?php echo $model->getSetting('social_article_comment_color_scheme') == 'dark' ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_comment_color_schemeD"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DARK");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_article_comment_order_by" class="radio">
                    <input type="radio" id="social_article_comment_order_byS" name="social_article_comment_order_by" value="social" <?php echo $model->getSetting('social_article_comment_order_by') == 'social' ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_comment_order_byS"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_SOCIAL");?></label>
                    <input type="radio" id="social_article_comment_order_byR" name="social_article_comment_order_by" value="reverse_time" <?php echo $model->getSetting('social_article_comment_order_by') == 'reverse_time' ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_comment_order_byR"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_REVERSETIME");?></label>
                    <input type="radio" id="social_article_comment_order_byT" name="social_article_comment_order_by" value="time" <?php echo $model->getSetting('social_article_comment_order_by') == 'time' ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_comment_order_byT"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_TIME");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BLOG_SETTING_LABEL");?></div>
            <div class="config_option header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_OPTIONS");?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_COMMENTS_DESC2");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COMMENTS_LABEL");?>:</div>
            <div class="config_option">
                <input type="text" name="social_blog_comment_max_num" value="<?php echo $model->getSetting('social_blog_comment_max_num') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_WIDTH_DESC2");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_WIDTH_LABEL");?>:</div>
            <div class="config_option">
                <input type="text" name="social_blog_comment_width" value="<?php echo $model->getSetting('social_blog_comment_width') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DESC2");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_blog_comment_color_scheme" class="radio">
                    <input type="radio" id="social_blog_comment_color_schemeL" name="social_blog_comment_color_scheme" value="light" <?php echo $model->getSetting('social_blog_comment_color_scheme') == 'light' ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_comment_color_schemeL"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LIGHT");?></label>
                    <input type="radio" id="social_blog_comment_color_schemeD" name="social_blog_comment_color_scheme" value="dark" <?php echo $model->getSetting('social_blog_comment_color_scheme') == 'dark' ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_comment_color_schemeD"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DARK");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_blog_comment_order_by" class="radio">
                    <input type="radio" id="social_blog_comment_order_byS" name="social_blog_comment_order_by" value="social" <?php echo $model->getSetting('social_blog_comment_order_by') == 'social' ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_comment_order_byS"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_SOCIAL");?></label>
                    <input type="radio" id="social_blog_comment_order_byR" name="social_blog_comment_order_by" value="reverse_time" <?php echo $model->getSetting('social_blog_comment_order_by') == 'reverse_time' ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_comment_order_byR"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_REVERSETIME");?></label>
                    <input type="radio" id="social_blog_comment_order_byT" name="social_blog_comment_order_by" value="time" <?php echo $model->getSetting('social_blog_comment_order_by') == 'time' ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_comment_order_byT"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_TIME");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_VIEW_SETTING_LABEL");?></div>
            <div class="config_option header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_OPTIONS");?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_ARTICLE_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_ARTICLE_LABEL");?>:</div>
            <div class="config_option">
                <?php $socialCommentArticleView = $model->getSetting('social_comment_article_view');?>
                <select name="social_comment_article_view">
                    <option value="1" <?php echo ($socialCommentArticleView == '1') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_TOP_LABEL");?></option>
                    <option value="2" <?php echo ($socialCommentArticleView == '2') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL");?></option>
                    <option value="3" <?php echo ($socialCommentArticleView == '3') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOTH_LABEL");?></option>
                    <option value="0" <?php echo ($socialCommentArticleView == '0') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_NONE_LABEL");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_FRONTPAGE_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_FRONTPAGE_LABEL");?>:</div>
            <div class="config_option">
                <?php $socialCommentFrontPageView = $model->getSetting('social_comment_frontpage_view');?>
                <select name="social_comment_frontpage_view">
                    <option value="1" <?php echo ($socialCommentFrontPageView == '1') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_TOP_LABEL");?></option>
                    <option value="2" <?php echo ($socialCommentFrontPageView == '2') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL");?></option>
                    <option value="3" <?php echo ($socialCommentFrontPageView == '3') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOTH_LABEL");?></option>
                    <option value="0" <?php echo ($socialCommentFrontPageView == '0') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_NONE_LABEL");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_CATEGORY_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_CATEGORY_LABEL");?>:</div>
            <div class="config_option">
                <?php $socialCommentCategoryView = $model->getSetting('social_comment_category_view');?>
                <select name="social_comment_category_view">
                    <option value="1" <?php echo ($socialCommentCategoryView == '1') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_TOP_LABEL");?></option>
                    <option value="2" <?php echo ($socialCommentCategoryView == '2') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL");?></option>
                    <option value="3" <?php echo ($socialCommentCategoryView == '3') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOTH_LABEL");?></option>
                    <option value="0" <?php echo ($socialCommentCategoryView == '0') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_NONE_LABEL");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting_option header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_CATEGORY_SETTING");?></div>
            <div style="clear:both"></div>
        </div>

        <div class="config_row">
            <div class="config_setting_option hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_CATEGORY_SETTING_DESC");?>">
                <?php $catType = $model->getSetting('social_comment_cat_include_type'); ?>
                <fieldset id="social_comment_cat_include_type" class="radio">
                    <input type="radio" id="social_comment_cat_include_type0" name="social_comment_cat_include_type" value="0" <?php echo ($catType == '0' ? 'checked="checked"' : ""); ?> onclick="toggleHide('comment_cat_ids', 'none')" />
                    <label for="social_comment_cat_include_type0"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ALL_LABEL");?></label>
                    <input type="radio" id="social_comment_cat_include_type1" name="social_comment_cat_include_type" value="1" <?php echo ($catType == '1' ? 'checked="checked"' : ""); ?> onclick="toggleHide('comment_cat_ids', '')" />
                    <label for="social_comment_cat_include_type1"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_INCLUDE_LABEL");?></label>
                    <input type="radio" id="social_comment_cat_include_type2" name="social_comment_cat_include_type" value="2" <?php echo ($catType == '2' ? 'checked="checked"' : ""); ?> onclick="toggleHide('comment_cat_ids', '')" />
                    <label for="social_comment_cat_include_type2"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_EXCLUDE_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row" id="comment_cat_ids" style="display:<?php echo ($catType == "0" ? 'none' : ''); ?>">
<?php
            $catids = $model->getSetting('social_comment_cat_ids');
            $categories = unserialize($catids);

            $db = JFactory::getDBO();
            $query = "SELECT id, title FROM #__categories WHERE extension='com_content'";
            $db->setQuery($query);
            $cats = $db->loadAssocList();
            $attribs = 'multiple="multiple"';
            echo '<td>' . JHTML::_('select.genericlist', $cats, 'social_comment_cat_ids[]', $attribs, 'id', 'title', $categories, 'social_comment_cat_ids') . '</td>';
?>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ARTICLE_SETTING");?></div>
            <div class="config_option header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_OPTIONS");?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_ARTICLE_INCLUDE_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_INCLUDE_LABEL");?>:</div>
            <div class="config_option">
                <input type="text" name="social_comment_article_include_ids" value="<?php echo $model->getSetting('social_comment_article_include_ids'); ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_ARTICLE_EXCLUDE_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_EXCLUDE_LABEL");?>:</div>
            <div class="config_option">
                <input type="text" name="social_comment_article_exclude_ids" value="<?php echo $model->getSetting('social_comment_article_exclude_ids'); ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
    </div>

<?php
    
?>
    <div class="tab-pane" id="social_content_like">
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ARTICLE_VIEW_SETTING");?></div>
            <div class="config_option header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_OPTIONS");?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_LAYOUT_STYLE_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_LAYOUT_STYLE_LABEL");?>:</div>
            <div class="config_option">
                <select name="social_article_like_layout_style">
                    <option value="standard" <?php echo $model->getSetting('social_article_like_layout_style') == 'standard' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_STANDARD_LABEL");?></option>
                    <option value="box_count" <?php echo $model->getSetting('social_article_like_layout_style') == 'box_count' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOX_COUNT_LABEL");?></option>
                    <option value="button_count" <?php echo $model->getSetting('social_article_like_layout_style') == 'button_count' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BUTTON_COUNT_LABEL");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_FACES_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_FACES_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_article_like_show_faces" class="radio">
                    <input type="radio" id="social_article_like_show_faces1" name="social_article_like_show_faces" value="1" <?php echo $model->getSetting('social_article_like_show_faces') ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_like_show_faces1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_article_like_show_faces0" name="social_article_like_show_faces" value="0" <?php echo $model->getSetting('social_article_like_show_faces') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_article_like_show_faces0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_SEND_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_SEND_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_article_like_show_send_button" class="radio">
                    <input type="radio" id="social_article_like_show_send_button1" name="social_article_like_show_send_button" value="1" <?php echo $model->getSetting('social_article_like_show_send_button') ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_like_show_send_button1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_article_like_show_send_button0" name="social_article_like_show_send_button" value="0" <?php echo $model->getSetting('social_article_like_show_send_button') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_article_like_show_send_button0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_LINKEDIN_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_LINKEDIN_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_article_like_show_linkedin" class="radio">
                    <input type="radio" id="social_article_like_show_linkedin1" name="social_article_like_show_linkedin" value="1" <?php echo $model->getSetting('social_article_like_show_linkedin') ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_like_show_linkedin1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_article_like_show_linkedin0" name="social_article_like_show_linkedin" value="0" <?php echo $model->getSetting('social_article_like_show_linkedin') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_article_like_show_linkedin0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_TWITTER_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_TWITTER_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_article_like_show_twitter" class="radio">
                    <input type="radio" id="social_article_like_show_twitter1" name="social_article_like_show_twitter" value="1" <?php echo $model->getSetting('social_article_like_show_twitter') ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_like_show_twitter1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_article_like_show_twitter0" name="social_article_like_show_twitter" value="0" <?php echo $model->getSetting('social_article_like_show_twitter') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_article_like_show_twitter0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_GOOGLE_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_GOOGLE_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_article_like_show_googleplus" class="radio">
                    <input type="radio" id="social_article_like_show_googleplus1" name="social_article_like_show_googleplus" value="1" <?php echo $model->getSetting('social_article_like_show_googleplus') ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_like_show_googleplus1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_article_like_show_googleplus0" name="social_article_like_show_googleplus" value="0" <?php echo $model->getSetting('social_article_like_show_googleplus') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_article_like_show_googleplus0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_VERB_DISPLAY_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_VERB_DISPLAY_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_article_like_verb_to_display" class="radio">
                    <input type="radio" id="social_article_like_verb_to_displayLike" name="social_article_like_verb_to_display" value="like" <?php echo $model->getSetting('social_article_like_verb_to_display') == 'like' ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_like_verb_to_displayLike"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_LIKE_LABEL");?></label>
                    <input type="radio" id="social_article_like_verb_to_displayRec" name="social_article_like_verb_to_display" value="recommend" <?php echo $model->getSetting('social_article_like_verb_to_display') == 'recommend' ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_like_verb_to_displayRec"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_RECOMMEND_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DESC3");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_article_like_color_scheme" class="radio">
                    <input type="radio" id="social_article_like_color_schemeL" name="social_article_like_color_scheme" value="light" <?php echo $model->getSetting('social_article_like_color_scheme') == 'light' ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_like_color_schemeL"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LIGHT");?></label>
                    <input type="radio" id="social_article_like_color_schemeD" name="social_article_like_color_scheme" value="dark" <?php echo $model->getSetting('social_article_like_color_scheme') == 'dark' ? 'checked="checked"' : ""; ?> />
                    <label for="social_article_like_color_schemeD"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DARK");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT_LABEL");?>:</div>
            <div class="config_option">
                <select name="social_article_like_font">
                    <option value="" <?php echo $model->getSetting('social_article_like_font') == 'arial' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT1");?></option>
                    <option value="lucida grande" <?php echo $model->getSetting('social_article_like_font') == 'lucida grande' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT2");?></option>
                    <option value="segoe ui" <?php echo $model->getSetting('social_article_like_font') == 'segoe ui' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT3");?></option>
                    <option value="tahoma" <?php echo $model->getSetting('social_article_like_font') == 'tahoma' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT4");?></option>
                    <option value="trebuchet ms" <?php echo $model->getSetting('social_article_like_font') == 'trebuchet ms' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT5");?></option>
                    <option value="verdana" <?php echo $model->getSetting('social_article_like_font') == 'verdana' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT6");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_WIDTH_DESC3");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_WIDTH_LABEL");?>:</div>
            <div class="config_option">
                <input type="text" name="social_article_like_width" value="<?php echo $model->getSetting('social_article_like_width') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BLOG_SETTING_LABEL");?></div>
            <div class="config_option header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_OPTIONS");?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_LAYOUT_STYLE_DESC2");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_LAYOUT_STYLE_LABEL");?>:</div>
            <div class="config_option">
                <select name="social_blog_like_layout_style">
                    <option value="standard" <?php echo $model->getSetting('social_blog_like_layout_style') == 'standard' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_STANDARD_LABEL");?></option>
                    <option value="box_count" <?php echo $model->getSetting('social_blog_like_layout_style') == 'box_count' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOX_COUNT_LABEL");?></option>
                    <option value="button_count" <?php echo $model->getSetting('social_blog_like_layout_style') == 'button_count' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BUTTON_COUNT_LABEL");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_FACES_DESC2");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_FACES_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_blog_like_show_faces" class="radio">
                    <input type="radio" id="social_blog_like_show_faces1" name="social_blog_like_show_faces" value="1" <?php echo $model->getSetting('social_blog_like_show_faces') ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_like_show_faces1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_blog_like_show_faces0" name="social_blog_like_show_faces" value="0" <?php echo $model->getSetting('social_blog_like_show_faces') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_blog_like_show_faces0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_SEND_DESC2");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_SEND_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_blog_like_show_send_button" class="radio">
                    <input type="radio" id="social_blog_like_show_send_button1" name="social_blog_like_show_send_button" value="1" <?php echo $model->getSetting('social_blog_like_show_send_button') ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_like_show_send_button1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_blog_like_show_send_button0" name="social_blog_like_show_send_button" value="0" <?php echo $model->getSetting('social_blog_like_show_send_button') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_blog_like_show_send_button0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_LINKEDIN_DESC2");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_LINKEDIN_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_blog_like_show_linkedin" class="radio">
                    <input type="radio" id="social_blog_like_show_linkedin1" name="social_blog_like_show_linkedin" value="1" <?php echo $model->getSetting('social_blog_like_show_linkedin') ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_like_show_linkedin1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_blog_like_show_linkedin0" name="social_blog_like_show_linkedin" value="0" <?php echo $model->getSetting('social_blog_like_show_linkedin') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_blog_like_show_linkedin0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_TWITTER_DESC2");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_TWITTER_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_blog_like_show_twitter" class="radio">
                    <input type="radio" id="social_blog_like_show_twitter1" name="social_blog_like_show_twitter" value="1" <?php echo $model->getSetting('social_blog_like_show_twitter') ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_like_show_twitter1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_blog_like_show_twitter0" name="social_blog_like_show_twitter" value="0" <?php echo $model->getSetting('social_blog_like_show_twitter') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_blog_like_show_twitter0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_GOOGLE_DESC2");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_GOOGLE_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_blog_like_show_googleplus" class="radio">
                    <input type="radio" id="social_blog_like_show_googleplus1" name="social_blog_like_show_googleplus" value="1" <?php echo $model->getSetting('social_blog_like_show_googleplus') ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_like_show_googleplus1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_blog_like_show_googleplus0" name="social_blog_like_show_googleplus" value="0" <?php echo $model->getSetting('social_blog_like_show_googleplus') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_blog_like_show_googleplus0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_VERB_DISPLAY_DESC2");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_VERB_DISPLAY_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_blog_like_verb_to_display" class="radio">
                    <input type="radio" id="social_blog_like_verb_to_displayLike" name="social_blog_like_verb_to_display" value="like" <?php echo $model->getSetting('social_blog_like_verb_to_display') == 'like' ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_like_verb_to_displayLike"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_LIKE_LABEL");?></label>
                    <input type="radio" id="social_blog_like_verb_to_displayRec" name="social_blog_like_verb_to_display" value="recommend" <?php echo $model->getSetting('social_blog_like_verb_to_display') == 'recommend' ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_like_verb_to_displayRec"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_RECOMMEND_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DESC2");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_blog_like_color_scheme" class="radio">
                    <input type="radio" id="social_blog_like_color_schemeL" name="social_blog_like_color_scheme" value="light" <?php echo $model->getSetting('social_blog_like_color_scheme') == 'light' ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_like_color_schemeL"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LIGHT");?></label>
                    <input type="radio" id="social_blog_like_color_schemeD" name="social_blog_like_color_scheme" value="dark" <?php echo $model->getSetting('social_blog_like_color_scheme') == 'dark' ? 'checked="checked"' : ""; ?> />
                    <label for="social_blog_like_color_schemeD"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DARK");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT_DESC2");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT_LABEL");?>:</div>
            <div class="config_option">
                <select name="social_blog_like_font">
                    <option value="arial" <?php echo $model->getSetting('social_blog_like_font') == 'arial' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT1");?></option>
                    <option value="lucida grande" <?php echo $model->getSetting('social_blog_like_font') == 'lucida grande' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT2");?></option>
                    <option value="segoe ui" <?php echo $model->getSetting('social_blog_like_font') == 'segoe ui' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT3");?></option>
                    <option value="tahoma" <?php echo $model->getSetting('social_blog_like_font') == 'tahoma' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT4");?></option>
                    <option value="trebuchet ms" <?php echo $model->getSetting('social_blog_like_font') == 'trebuchet ms' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT5");?></option>
                    <option value="verdana" <?php echo $model->getSetting('social_blog_like_font') == 'verdana' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT6");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_WIDTH_DESC4");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_WIDTH_LABEL");?>:</div>
            <div class="config_option">
                <input type="text" name="social_blog_like_width" value="<?php echo $model->getSetting('social_blog_like_width') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_VIEW_SETTING_LABEL");?></div>
            <div class="config_option header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_OPTIONS");?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_ARTICLE_DESC2');?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_ARTICLE_LABEL");?>:</div>
            <div class="config_option">
                <?php $socialLikeArticleView = $model->getSetting('social_like_article_view');?>
                <select name="social_like_article_view">
                    <option value="1" <?php echo ($socialLikeArticleView == '1') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_TOP_LABEL");?></option>
                    <option value="2" <?php echo ($socialLikeArticleView == '2') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL");?></option>
                    <option value="3" <?php echo ($socialLikeArticleView == '3') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOTH_LABEL");?></option>
                    <option value="0" <?php echo ($socialLikeArticleView == '0') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_NONE_LABEL");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_FRONTPAGE_DESC2');?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_FRONTPAGE_LABEL");?>:</div>
            <div class="config_option">
                <?php $socialLikeFrontPageView = $model->getSetting('social_like_frontpage_view');?>
                <select name="social_like_frontpage_view">
                    <option value="1" <?php echo ($socialLikeFrontPageView == '1') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_TOP_LABEL");?></option>
                    <option value="2" <?php echo ($socialLikeFrontPageView == '2') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL");?></option>
                    <option value="3" <?php echo ($socialLikeFrontPageView == '3') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOTH_LABEL");?></option>
                    <option value="0" <?php echo ($socialLikeFrontPageView == '0') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_NONE_LABEL");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_CATEGORY_DESC2');?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_SHOW_CATEGORY_LABEL");?>:</div>
            <div class="config_option">
                <?php $socialLikeCategoryView = $model->getSetting('social_like_category_view');?>
                <select name="social_like_category_view">
                    <option value="1" <?php echo ($socialLikeCategoryView == '1') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_TOP_LABEL");?></option>
                    <option value="2" <?php echo ($socialLikeCategoryView == '2') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL");?></option>
                    <option value="3" <?php echo ($socialLikeCategoryView == '3') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOTH_LABEL");?></option>
                    <option value="0" <?php echo ($socialLikeCategoryView == '0') ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_NONE_LABEL");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting_option header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_CATEGORY_SETTING");?></div>
            <div style="clear:both"></div>
        </div>

        <div class="config_row">
            <div class="config_setting_option hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_CATEGORY_SETTING_DESC2');?>">
                <?php $catType = $model->getSetting('social_like_cat_include_type'); ?>
                <fieldset id="social_like_cat_include_type" class="radio">
                    <input type="radio" id="social_like_cat_include_type0" name="social_like_cat_include_type" value="0" <?php echo ($catType == '0' ? 'checked="checked"' : ""); ?> onclick="toggleHide('like_cat_ids', 'none')" />
                    <label for="social_like_cat_include_type0"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ALL_LABEL");?></label>
                    <input type="radio" id="social_like_cat_include_type1" name="social_like_cat_include_type" value="1" <?php echo ($catType == '1' ? 'checked="checked"' : ""); ?> onclick="toggleHide('like_cat_ids', '')" />
                    <label for="social_like_cat_include_type1"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_INCLUDE_LABEL");?></label>
                    <input type="radio" id="social_like_cat_include_type2" name="social_like_cat_include_type" value="2" <?php echo ($catType == '2' ? 'checked="checked"' : ""); ?> onclick="toggleHide('like_cat_ids', '')" />
                    <label for="social_like_cat_include_type2"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_EXCLUDE_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row" id="like_cat_ids" style="display:<?php echo ($catType == "0" ? 'none' : ''); ?>">
<?php
            $catids = $model->getSetting('social_like_cat_ids');
            $categories = unserialize($catids);

            $db = JFactory::getDBO();
            $query = "SELECT id, title FROM #__categories WHERE extension='com_content'";
            $db->setQuery($query);
            $cats = $db->loadAssocList();
            $attribs = 'multiple="multiple"';
            echo '<td>' . JHTML::_('select.genericlist', $cats, 'social_like_cat_ids[]', $attribs, 'id', 'title', $categories, 'social_like_cat_ids') . '</td>';
?>
        <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ARTICLE_SETTING");?></div>
            <div class="config_option header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_OPTIONS");?></div>
            <div style="clear:both"></div>
        </div>

        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_ARTICLE_INCLUDE_DESC');?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_INCLUDE_LABEL");?>:</div>
            <div class="config_option">
                <input type="text" name="social_like_article_include_ids" value="<?php echo $model->getSetting('social_like_article_include_ids'); ?>" size="20">
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="config_row">
                <div class="config_setting hasTip"
                     title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_ARTICLE_EXCLUDE_DESC');?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_EXCLUDE_LABEL");?>:</div>
                <div class="config_option">
                    <input type="text" name="social_like_article_exclude_ids" value="<?php echo $model->getSetting('social_like_article_exclude_ids'); ?>" size="20">
                </div>
                <div style="clear:both"></div>
            </div>
        </div>
<?php
     //SC16
    if($k2IsInstalled)
    {
        
?>
    <div class="tab-pane" id="social_content_k2_comment">
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ITEM_VIEW_SETTING");?></div>
            <div class="config_option header"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_OPTIONS");?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_COMMENTS_DESC3');?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COMMENTS_LABEL");?>:</div>
            <div class="config_option">
                <input type="text" name="social_k2_item_comment_max_num" value="<?php echo $model->getSetting('social_k2_item_comment_max_num') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_WIDTH_DESC5');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_WIDTH_LABEL');?>:</div>
            <div class="config_option">
                <input type="text" name="social_k2_item_comment_width" value="<?php echo $model->getSetting('social_k2_item_comment_width') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DESC4');?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_item_comment_color_scheme" class="radio">
                    <input type="radio" id="social_k2_item_comment_color_schemeL" name="social_k2_item_comment_color_scheme" value="light" <?php echo $model->getSetting('social_k2_item_comment_color_scheme') == 'light' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_comment_color_schemeL"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LIGHT");?></label>
                    <input type="radio" id="social_k2_item_comment_color_schemeD" name="social_k2_item_comment_color_scheme" value="dark" <?php echo $model->getSetting('social_k2_item_comment_color_scheme') == 'dark' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_comment_color_schemeD"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DARK");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_item_comment_order_by" class="radio">
                    <input type="radio" id="social_k2_item_comment_order_byS" name="social_k2_item_comment_order_by" value="social" <?php echo $model->getSetting('social_k2_item_comment_order_by') == 'social' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_comment_order_byS"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_SOCIAL");?></label>
                    <input type="radio" id="social_k2_item_comment_order_byR" name="social_k2_item_comment_order_by" value="reverse_time" <?php echo $model->getSetting('social_k2_item_comment_order_by') == 'reverse_time' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_comment_order_byR"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_REVERSETIME");?></label>
                    <input type="radio" id="social_k2_item_comment_order_byT" name="social_k2_item_comment_order_by" value="time" <?php echo $model->getSetting('social_k2_item_comment_order_by') == 'time' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_comment_order_byT"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_TIME");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BLOG_SETTING_LABEL');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_COMMENTS_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_COMMENTS_LABEL');?>:</div>
            <div class="config_option">
                <input type="text" name="social_k2_blog_comment_max_num" value="<?php echo $model->getSetting('social_k2_blog_comment_max_num') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_WIDTH_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_WIDTH_LABEL');?>:</div>
            <div class="config_option">
                <input type="text" name="social_k2_blog_comment_width" value="<?php echo $model->getSetting('social_k2_blog_comment_width') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_blog_comment_color_scheme" class="radio">
                    <input type="radio" id="social_k2_blog_comment_color_schemeL" name="social_k2_blog_comment_color_scheme" value="light" <?php echo $model->getSetting('social_k2_blog_comment_color_scheme') == 'light' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_comment_color_schemeL"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LIGHT");?></label>
                    <input type="radio" id="social_k2_blog_comment_color_schemeD" name="social_k2_blog_comment_color_scheme" value="dark" <?php echo $model->getSetting('social_k2_blog_comment_color_scheme') == 'dark' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_comment_color_schemeD"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DARK");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_DESC");?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_LABEL");?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_blog_comment_order_by" class="radio">
                    <input type="radio" id="social_k2_blog_comment_order_byS" name="social_k2_blog_comment_order_by" value="social" <?php echo $model->getSetting('social_k2_blog_comment_order_by') == 'social' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_comment_order_byS"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_SOCIAL");?></label>
                    <input type="radio" id="social_k2_blog_comment_order_byR" name="social_k2_blog_comment_order_by" value="reverse_time" <?php echo $model->getSetting('social_k2_blog_comment_order_by') == 'reverse_time' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_comment_order_byR"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_REVERSETIME");?></label>
                    <input type="radio" id="social_k2_blog_comment_order_byT" name="social_k2_blog_comment_order_by" value="time" <?php echo $model->getSetting('social_k2_blog_comment_order_by') == 'time' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_comment_order_byT"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_ORDERBY_TIME");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_VIEW_SETTING_LABEL');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_ITEM_VIEW_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_ITEM_VIEW_LABEL');?>:</div>
            <div class="config_option">
                <?php $socialK2CommentItemView = $model->getSetting('social_k2_comment_item_view');?>
                <select name="social_k2_comment_item_view">
                    <option value="1" <?php echo ($socialK2CommentItemView == '1') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_TOP_LABEL');?></option>
                    <option value="2" <?php echo ($socialK2CommentItemView == '2') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL');?></option>
                    <option value="3" <?php echo ($socialK2CommentItemView == '3') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTH_LABEL');?></option>
                    <option value="0" <?php echo ($socialK2CommentItemView == '0') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NONE_LABEL');?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_CATEGORY_VIEW_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_CATEGORY_VIEW_LABEL');?>:</div>
            <div class="config_option">
                <?php $socialK2CommentCategoryView = $model->getSetting('social_k2_comment_category_view');?>
                <select name="social_k2_comment_category_view">
                    <option value="1" <?php echo ($socialK2CommentCategoryView == '1') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_TOP_LABEL');?></option>
                    <option value="2" <?php echo ($socialK2CommentCategoryView == '2') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL');?></option>
                    <option value="3" <?php echo ($socialK2CommentCategoryView == '3') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTH_LABEL');?></option>
                    <option value="0" <?php echo ($socialK2CommentCategoryView == '0') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NONE_LABEL');?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_TAG_VIEW_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_TAG_VIEW_LABEL');?>:</div>
            <div class="config_option">
                <?php $socialK2CommentTagView = $model->getSetting('social_k2_comment_tag_view');?>
                <select name="social_k2_comment_tag_view">
                    <option value="1" <?php echo ($socialK2CommentTagView == '1') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_TOP_LABEL');?></option>
                    <option value="2" <?php echo ($socialK2CommentTagView == '2') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL');?></option>
                    <option value="3" <?php echo ($socialK2CommentTagView == '3') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTH_LABEL');?></option>
                    <option value="0" <?php echo ($socialK2CommentTagView == '0') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NONE_LABEL');?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_USERPAGE_VIEW_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_USERPAGE_VIEW_LABEL');?>:</div>
            <div class="config_option">
                <?php $socialK2CommentUserpageView = $model->getSetting('social_k2_comment_userpage_view');?>
                <select name="social_k2_comment_userpage_view">
                    <option value="1" <?php echo ($socialK2CommentUserpageView == '1') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_TOP_LABEL');?></option>
                    <option value="2" <?php echo ($socialK2CommentUserpageView == '2') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL');?></option>
                    <option value="3" <?php echo ($socialK2CommentUserpageView == '3') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTH_LABEL');?></option>
                    <option value="0" <?php echo ($socialK2CommentUserpageView == '0') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NONE_LABEL');?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_LATEST_VIEW_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_LATEST_VIEW_LABEL');?>:</div>
            <div class="config_option">
                <?php $socialK2CommentLatestView = $model->getSetting('social_k2_comment_latest_view');?>
                <select name="social_k2_comment_latest_view">
                    <option value="1" <?php echo ($socialK2CommentLatestView == '1') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_TOP_LABEL');?></option>
                    <option value="2" <?php echo ($socialK2CommentLatestView == '2') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL');?></option>
                    <option value="3" <?php echo ($socialK2CommentLatestView == '3') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTH_LABEL');?></option>
                    <option value="0" <?php echo ($socialK2CommentLatestView == '0') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NONE_LABEL');?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <!-- Categories -->
        <div class="config_row">
            <div class="config_setting_option header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_K2_CATEGORY_SETTING');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting_option hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_K2_CATEGORY_SETTING_DESC');?>">
                <?php $k2CatType = $model->getSetting('social_k2_comment_cat_include_type'); ?>
                <fieldset id="social_k2_comment_cat_include_type" class="radio">
                    <input type="radio" id="social_k2_comment_cat_include_type0" name="social_k2_comment_cat_include_type" value="0" <?php echo ($k2CatType == '0' ? 'checked="checked"' : ""); ?> onclick="toggleHide('k2_comment_cat_ids', 'none')" />
                    <label for="social_k2_comment_cat_include_type0"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_ALL_LABEL');?></label>
                    <input type="radio" id="social_k2_comment_cat_include_type1" name="social_k2_comment_cat_include_type" value="1" <?php echo ($k2CatType == '1' ? 'checked="checked"' : ""); ?> onclick="toggleHide('k2_comment_cat_ids', '')" />
                    <label for="social_k2_comment_cat_include_type1"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_INCLUDE_LABEL');?></label>
                    <input type="radio" id="social_k2_comment_cat_include_type2" name="social_k2_comment_cat_include_type" value="2" <?php echo ($k2CatType == '2' ? 'checked="checked"' : ""); ?> onclick="toggleHide('k2_comment_cat_ids', '')" />
                    <label for="social_k2_comment_cat_include_type2"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXCLUDE_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row" id="k2_comment_cat_ids" style="display:<?php echo ($k2CatType == "0" ? 'none' : ''); ?>">
<?php
            $k2catids = $model->getSetting('social_k2_comment_cat_ids');
            $k2categories = unserialize($k2catids);

            $query = "SELECT `id`, `name` FROM #__k2_categories";
            $db = JFactory::getDBO();
            $db->setQuery($query);
            $k2cats = $db->loadAssocList();
            $attribs = 'multiple="multiple"';
            echo '<td>' . JHTML::_('select.genericlist', $k2cats, 'social_k2_comment_cat_ids[]', $attribs, 'id', 'name', $k2categories, 'social_k2_comment_cat_ids') . '</td>';
?>
            <div style="clear:both"></div>
        </div>
        <!-- End Categories -->
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_K2_ITEM_SETTING');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_K2_ITEM_INCLUDE_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_INCLUDE_LABEL');?>:</div>
            <div class="config_option">
                <input type="text" name="social_k2_comment_item_include_ids" value="<?php echo $model->getSetting('social_k2_comment_item_include_ids'); ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_K2_ITEM_EXCLUDE_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXCLUDE_LABEL');?>:</div>
            <div class="config_option">
                <input type="text" name="social_k2_comment_item_exclude_ids" value="<?php echo $model->getSetting('social_k2_comment_item_exclude_ids'); ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
<?php
    
?>
    <div class="tab-pane" id="social_content_k2_like">
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_ITEM_VIEW_SETTING');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_LAYOUT_STYLE_DESC3');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_LAYOUT_STYLE_LABEL');?>:</div>
            <div class="config_option">
                <select name="social_k2_item_like_layout_style">
                    <option value="standard" <?php echo $model->getSetting('social_k2_item_like_layout_style') == 'standard' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_STANDARD_LABEL");?></option>
                    <option value="box_count" <?php echo $model->getSetting('social_k2_item_like_layout_style') == 'box_count' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOX_COUNT_LABEL");?></option>
                    <option value="button_count" <?php echo $model->getSetting('social_k2_item_like_layout_style') == 'button_count' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BUTTON_COUNT_LABEL");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_FACES_DESC3');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_FACES_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_item_like_show_faces" class="radio">
                    <input type="radio" id="social_k2_item_like_show_faces1" name="social_k2_item_like_show_faces" value="1" <?php echo $model->getSetting('social_k2_item_like_show_faces') ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_like_show_faces1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_k2_item_like_show_faces0" name="social_k2_item_like_show_faces" value="0" <?php echo $model->getSetting('social_k2_item_like_show_faces') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_k2_item_like_show_faces0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_SEND_DESC3');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_SEND_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_item_like_show_send_button" class="radio">
                    <input type="radio" id="social_k2_item_like_show_send_button1" name="social_k2_item_like_show_send_button" value="1" <?php echo $model->getSetting('social_k2_item_like_show_send_button') ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_like_show_send_button1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_k2_item_like_show_send_button0" name="social_k2_item_like_show_send_button" value="0" <?php echo $model->getSetting('social_k2_item_like_show_send_button') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_k2_item_like_show_send_button0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_LINKEDIN_DESC3');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_LINKEDIN_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_item_like_show_linkedin" class="radio">
                    <input type="radio" id="social_k2_item_like_show_linkedin1" name="social_k2_item_like_show_linkedin" value="1" <?php echo $model->getSetting('social_k2_item_like_show_linkedin') ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_like_show_linkedin1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_k2_item_like_show_linkedin0" name="social_k2_item_like_show_linkedin" value="0" <?php echo $model->getSetting('social_k2_item_like_show_linkedin') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_k2_item_like_show_linkedin0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_TWITTER_DESC3');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_TWITTER_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_item_like_show_twitter" class="radio">
                    <input type="radio" id="social_k2_item_like_show_twitter1" name="social_k2_item_like_show_twitter" value="1" <?php echo $model->getSetting('social_k2_item_like_show_twitter') ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_like_show_twitter1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_k2_item_like_show_twitter0" name="social_k2_item_like_show_twitter" value="0" <?php echo $model->getSetting('social_k2_item_like_show_twitter') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_k2_item_like_show_twitter0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_GOOGLE_DESC3');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_GOOGLE_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_item_like_show_googleplus" class="radio">
                    <input type="radio" id="social_k2_item_like_show_googleplus1" name="social_k2_item_like_show_googleplus" value="1" <?php echo $model->getSetting('social_k2_item_like_show_googleplus') ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_like_show_googleplus1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_k2_item_like_show_googleplus0" name="social_k2_item_like_show_googleplus" value="0" <?php echo $model->getSetting('social_k2_item_like_show_googleplus') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_k2_item_like_show_googleplus0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_VERB_DISPLAY_DESC3');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_VERB_DISPLAY_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_item_like_verb_to_display" class="radio">
                    <input type="radio" id="social_k2_item_like_verb_to_displayLike" name="social_k2_item_like_verb_to_display" value="like" <?php echo $model->getSetting('social_k2_item_like_verb_to_display') == 'like' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_like_verb_to_displayLike"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_LIKE_LABEL');?></label>
                    <input type="radio" id="social_k2_item_like_verb_to_displayRec" name="social_k2_item_like_verb_to_display" value="recommend" <?php echo $model->getSetting('social_k2_item_like_verb_to_display') == 'recommend' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_like_verb_to_displayRec"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_RECOMMEND_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DESC4');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_item_like_color_scheme" class="radio">
                    <input type="radio" id="social_k2_item_like_color_schemeL" name="social_k2_item_like_color_scheme" value="light" <?php echo $model->getSetting('social_k2_item_like_color_scheme') == 'light' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_like_color_schemeL"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LIGHT");?></label>
                    <input type="radio" id="social_k2_item_like_color_schemeD" name="social_k2_item_like_color_scheme" value="dark" <?php echo $model->getSetting('social_k2_item_like_color_scheme') == 'dark' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_item_like_color_schemeD"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DARK");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_FONT_DESC3');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_FONT_LABEL');?>:</div>
            <div class="config_option">
                <select name="social_k2_item_like_font">
                    <option value="arial" <?php echo $model->getSetting('social_k2_item_like_font') == 'arial' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT1");?></option>
                    <option value="lucida grande" <?php echo $model->getSetting('social_k2_item_like_font') == 'lucida grande' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT2");?></option>
                    <option value="segoe ui" <?php echo $model->getSetting('social_k2_item_like_font') == 'segoe ui' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT3");?></option>
                    <option value="tahoma" <?php echo $model->getSetting('social_k2_item_like_font') == 'tahoma' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT4");?></option>
                    <option value="trebuchet ms" <?php echo $model->getSetting('social_k2_item_like_font') == 'trebuchet ms' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT5");?></option>
                    <option value="verdana" <?php echo $model->getSetting('social_k2_item_like_font') == 'verdana' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT6");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_WIDTH_DESC6');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_WIDTH_LABEL');?>:</div>
            <div class="config_option">
                <input type="text" name="social_k2_item_like_width" value="<?php echo $model->getSetting('social_k2_item_like_width') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BLOG_SETTING_LABEL');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_LAYOUT_STYLE_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_LAYOUT_STYLE_LABEL');?>:</div>
            <div class="config_option">
                <select name="social_k2_blog_like_layout_style">
                    <option value="standard" <?php echo $model->getSetting('social_k2_blog_like_layout_style') == 'standard' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_STANDARD_LABEL");?></option>
                    <option value="box_count" <?php echo $model->getSetting('social_k2_blog_like_layout_style') == 'box_count' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BOX_COUNT_LABEL");?></option>
                    <option value="button_count" <?php echo $model->getSetting('social_k2_blog_like_layout_style') == 'button_count' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_BUTTON_COUNT_LABEL");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_FACES_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_FACES_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_blog_like_show_faces" class="radio">
                    <input type="radio" id="social_k2_blog_like_show_faces1" name="social_k2_blog_like_show_faces" value="1" <?php echo $model->getSetting('social_k2_blog_like_show_faces') ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_like_show_faces1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_k2_blog_like_show_faces0" name="social_k2_blog_like_show_faces" value="0" <?php echo $model->getSetting('social_k2_blog_like_show_faces') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_k2_blog_like_show_faces0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_SEND_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_SEND_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_blog_like_show_send_button" class="radio">
                    <input type="radio" id="social_k2_blog_like_show_send_button1" name="social_k2_blog_like_show_send_button" value="1" <?php echo $model->getSetting('social_k2_blog_like_show_send_button') ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_like_show_send_button1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_k2_blog_like_show_send_button0" name="social_k2_blog_like_show_send_button" value="0" <?php echo $model->getSetting('social_k2_blog_like_show_send_button') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_k2_blog_like_show_send_button0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_LINKEDIN_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_LINKEDIN_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_blog_like_show_linkedin" class="radio">
                    <input type="radio" id="social_k2_blog_like_show_linkedin1" name="social_k2_blog_like_show_linkedin" value="1" <?php echo $model->getSetting('social_k2_blog_like_show_linkedin') ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_like_show_linkedin1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_k2_blog_like_show_linkedin0" name="social_k2_blog_like_show_linkedin" value="0" <?php echo $model->getSetting('social_k2_blog_like_show_linkedin') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_k2_blog_like_show_linkedin0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_TWITTER_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_TWITTER_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_blog_like_show_twitter" class="radio">
                    <input type="radio" id="social_k2_blog_like_show_twitter1" name="social_k2_blog_like_show_twitter" value="1" <?php echo $model->getSetting('social_k2_blog_like_show_twitter') ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_like_show_twitter1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_k2_blog_like_show_twitter0" name="social_k2_blog_like_show_twitter" value="0" <?php echo $model->getSetting('social_k2_blog_like_show_twitter') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_k2_blog_like_show_twitter0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_GOOGLE_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_GOOGLE_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_blog_like_show_googleplus" class="radio">
                    <input type="radio" id="social_k2_blog_like_show_googleplus1" name="social_k2_blog_like_show_googleplus" value="1" <?php echo $model->getSetting('social_k2_blog_like_show_googleplus') ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_like_show_googleplus1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_k2_blog_like_show_googleplus0" name="social_k2_blog_like_show_googleplus" value="0" <?php echo $model->getSetting('social_k2_blog_like_show_googleplus') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_k2_blog_like_show_googleplus0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_VERB_DISPLAY_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_VERB_DISPLAY_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_blog_like_verb_to_display" class="radio">
                    <input type="radio" id="social_k2_blog_like_verb_to_displayLike" name="social_k2_blog_like_verb_to_display" value="like" <?php echo $model->getSetting('social_k2_blog_like_verb_to_display') == 'like' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_like_verb_to_displayLike"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_LIKE_LABEL');?></label>
                    <input type="radio" id="social_k2_blog_like_verb_to_displayRec" name="social_k2_blog_like_verb_to_display" value="recommend" <?php echo $model->getSetting('social_k2_blog_like_verb_to_display') == 'recommend' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_like_verb_to_displayRec"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_RECOMMEND_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="social_k2_blog_like_color_scheme" class="radio">
                    <input type="radio" id="social_k2_blog_like_color_schemeL" name="social_k2_blog_like_color_scheme" value="light" <?php echo $model->getSetting('social_k2_blog_like_color_scheme') == 'light' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_like_color_schemeL"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_LIGHT");?></label>
                    <input type="radio" id="social_k2_blog_like_color_schemeD" name="social_k2_blog_like_color_scheme" value="dark" <?php echo $model->getSetting('social_k2_blog_like_color_scheme') == 'dark' ? 'checked="checked"' : ""; ?> />
                    <label for="social_k2_blog_like_color_schemeD"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_COLOR_SCHEME_DARK");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_FONT_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_FONT_LABEL');?>:</div>
            <div class="config_option">
                <select name="social_k2_blog_like_font">
                    <option value="arial" <?php echo $model->getSetting('social_k2_blog_like_font') == 'arial' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT1");?></option>
                    <option value="lucida grande" <?php echo $model->getSetting('social_k2_blog_like_font') == 'lucida grande' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT2");?></option>
                    <option value="segoe ui" <?php echo $model->getSetting('social_k2_blog_like_font') == 'segoe ui' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT3");?></option>
                    <option value="tahoma" <?php echo $model->getSetting('social_k2_blog_like_font') == 'tahoma' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT4");?></option>
                    <option value="trebuchet ms" <?php echo $model->getSetting('social_k2_blog_like_font') == 'trebuchet ms' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT5");?></option>
                    <option value="verdana" <?php echo $model->getSetting('social_k2_blog_like_font') == 'verdana' ? 'selected' : ""; ?>><?php echo JText::_("COM_JFBCONNECT_SOCIAL_FONT6");?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_WIDTH_DESC4');?>"><?php echo JText::_("COM_JFBCONNECT_SOCIAL_WIDTH_LABEL");?>:</div>
            <div class="config_option">
                <input type="text" name="social_k2_blog_like_width" value="<?php echo $model->getSetting('social_k2_blog_like_width') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_VIEW_SETTING_LABEL');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_ITEM_VIEW_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_ITEM_VIEW_LABEL');?>:</div>
            <div class="config_option">
                <?php $socialK2LikeItemView = $model->getSetting('social_k2_like_item_view');?>
                <select name="social_k2_like_item_view">
                    <option value="1" <?php echo ($socialK2LikeItemView == '1') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_TOP_LABEL');?></option>
                    <option value="2" <?php echo ($socialK2LikeItemView == '2') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL');?></option>
                    <option value="3" <?php echo ($socialK2LikeItemView == '3') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTH_LABEL');?></option>
                    <option value="0" <?php echo ($socialK2LikeItemView == '0') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NONE_LABEL');?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_CATEGORY_VIEW_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_CATEGORY_VIEW_LABEL');?>:</div>
            <div class="config_option">
                <?php $socialK2LikeCategoryView = $model->getSetting('social_k2_like_category_view');?>
                <select name="social_k2_like_category_view">
                    <option value="1" <?php echo ($socialK2LikeCategoryView == '1') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_TOP_LABEL');?></option>
                    <option value="2" <?php echo ($socialK2LikeCategoryView == '2') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL');?></option>
                    <option value="3" <?php echo ($socialK2LikeCategoryView == '3') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTH_LABEL');?></option>
                    <option value="0" <?php echo ($socialK2LikeCategoryView == '0') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NONE_LABEL');?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_TAG_VIEW_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_TAG_VIEW_LABEL');?>:</div>
            <div class="config_option">
                <?php $socialK2LikeTagView = $model->getSetting('social_k2_like_tag_view');?>
                <select name="social_k2_like_tag_view">
                    <option value="1" <?php echo ($socialK2LikeTagView == '1') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_TOP_LABEL');?></option>
                    <option value="2" <?php echo ($socialK2LikeTagView == '2') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL');?></option>
                    <option value="3" <?php echo ($socialK2LikeTagView == '3') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTH_LABEL');?></option>
                    <option value="0" <?php echo ($socialK2LikeTagView == '0') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NONE_LABEL');?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_USERPAGE_VIEW_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_USERPAGE_VIEW_LABEL');?>:</div>
            <div class="config_option">
                <?php $socialK2LikeUserpageView = $model->getSetting('social_k2_like_userpage_view');?>
                <select name="social_k2_like_userpage_view">
                    <option value="1" <?php echo ($socialK2LikeUserpageView == '1') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_TOP_LABEL');?></option>
                    <option value="2" <?php echo ($socialK2LikeUserpageView == '2') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL');?></option>
                    <option value="3" <?php echo ($socialK2LikeUserpageView == '3') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTH_LABEL');?></option>
                    <option value="0" <?php echo ($socialK2LikeUserpageView == '0') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NONE_LABEL');?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_LATEST_VIEW_DESC2');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SHOW_K2_LATEST_VIEW_LABEL');?>:</div>
            <div class="config_option">
                <?php $socialK2LikeLatestView = $model->getSetting('social_k2_like_latest_view');?>
                <select name="social_k2_like_latest_view">
                    <option value="1" <?php echo ($socialK2LikeLatestView == '1') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_TOP_LABEL');?></option>
                    <option value="2" <?php echo ($socialK2LikeLatestView == '2') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTTOM_LABEL');?></option>
                    <option value="3" <?php echo ($socialK2LikeLatestView == '3') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_BOTH_LABEL');?></option>
                    <option value="0" <?php echo ($socialK2LikeLatestView == '0') ? 'selected' : ""; ?>><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NONE_LABEL');?></option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <!-- Category -->
        <div class="config_row">
            <div class="config_setting_option header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_K2_CATEGORY_SETTING');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting_option hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_K2_CATEGORY_SETTING_DESC2');?>">
                <?php $k2catType = $model->getSetting('social_k2_like_cat_include_type'); ?>
                <fieldset id="social_k2_like_cat_include_type" class="radio">
                    <input type="radio" id="social_k2_like_cat_include_type0" name="social_k2_like_cat_include_type" value="0" <?php echo ($k2catType == '0' ? 'checked="checked"' : ""); ?> onclick="toggleHide('k2_like_cat_ids', 'none')" />
                    <label for="social_k2_like_cat_include_type0"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_ALL_LABEL');?></label>
                    <input type="radio" id="social_k2_like_cat_include_type1" name="social_k2_like_cat_include_type" value="1" <?php echo ($k2catType == '1' ? 'checked="checked"' : ""); ?> onclick="toggleHide('k2_like_cat_ids', '')" />
                    <label for="social_k2_like_cat_include_type1"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_INCLUDE_LABEL');?></label>
                    <input type="radio" id="social_k2_like_cat_include_type2" name="social_k2_like_cat_include_type" value="2" <?php echo ($k2catType == '2' ? 'checked="checked"' : ""); ?> onclick="toggleHide('k2_like_cat_ids', '')" />
                    <label for="social_k2_like_cat_include_type2"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXCLUDE_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row" id="k2_like_cat_ids" style="display:<?php echo ($k2catType == "0" ? 'none' : ''); ?>">
<?php
            $k2catids = $model->getSetting('social_k2_like_cat_ids');
            $k2categories = unserialize($k2catids);

            $query = "SELECT `id`, `name` FROM #__k2_categories";
            $db = JFactory::getDBO();
            $db->setQuery($query);
            $k2cats = $db->loadAssocList();
            $attribs = 'multiple="multiple"';
            echo '<td>' . JHTML::_('select.genericlist', $k2cats, 'social_k2_like_cat_ids[]', $attribs, 'id', 'name', $k2categories, 'social_k2_like_cat_ids') . '</td>';
?>
        <div style="clear:both"></div>
        </div>
        <!-- End Categories -->
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_K2_ITEM_SETTING');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></div>
            <div style="clear:both"></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_K2_ITEM_INCLUDE_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_INCLUDE_LABEL');?>:</div>
            <div class="config_option">
                <input type="text" name="social_k2_like_item_include_ids" value="<?php echo $model->getSetting('social_k2_like_item_include_ids'); ?>" size="20">
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="config_row">
                <div class="config_setting hasTip"
                     title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_K2_ITEM_EXCLUDE_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXCLUDE_LABEL');?>:</div>
                <div class="config_option">
                    <input type="text" name="social_k2_like_item_exclude_ids" value="<?php echo $model->getSetting('social_k2_like_item_exclude_ids'); ?>" size="20">
                </div>
                <div style="clear:both"></div>
            </div>
        </div>

<?php
         //SC16
    }
    
?>

    <div class="tab-pane" id="social_notifications">
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SETTING');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_NOTIFICATIONS_NEW_COMMENTS_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NOTIFICATIONS_NEW_COMMENTS_LABEL');?></div>
            <div class="config_option">
                <fieldset id="social_notification_comment_enabled" class="radio">
                    <input type="radio" id="social_notification_comment_enabled1" name="social_notification_comment_enabled" value="1" <?php echo $model->getSetting('social_notification_comment_enabled') ? 'checked="checked"' : ""; ?> />
                    <label for="social_notification_comment_enabled1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_notification_comment_enabled0" name="social_notification_comment_enabled" value="0" <?php echo $model->getSetting('social_notification_comment_enabled') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_notification_comment_enabled0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_NOTIFICATIONS_NEW_LIKES_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NOTIFICATIONS_NEW_LIKES_LABEL');?></div>
            <div class="config_option">
                <fieldset id="social_notification_like_enabled" class="radio">
                    <input type="radio" id="social_notification_like_enabled1" name="social_notification_like_enabled" value="1" <?php echo $model->getSetting('social_notification_like_enabled') ? 'checked="checked"' : ""; ?> />
                    <label for="social_notification_like_enabled1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_notification_like_enabled0" name="social_notification_like_enabled" value="0" <?php echo $model->getSetting('social_notification_like_enabled') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_notification_like_enabled0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_NOTIFICATIONS_ADDRESS_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NOTIFICATIONS_ADDRESS_LABEL');?></div>
            <div class="config_option">
                <textarea name="social_notification_email_address" rows="3" cols="30"><?php echo $model->getSetting('social_notification_email_address') ?></textarea><br/>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_NOTIFICATIONS_GOOGLE_ANALYTICS_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_NOTIFICATIONS_GOOGLE_ANALYTICS_LABEL');?></div>
            <div class="config_option">
                <fieldset id="social_notification_google_analytics" class="radio">
                    <input type="radio" id="social_notification_google_analytics1" name="social_notification_google_analytics" value="1" <?php echo $model->getSetting('social_notification_google_analytics') ? 'checked="checked"' : ""; ?> />
                    <label for="social_notification_google_analytics1"><?php echo JText::_("COM_JFBCONNECT_YES_LABEL");?></label>
                    <input type="radio" id="social_notification_google_analytics0" name="social_notification_google_analytics" value="0" <?php echo $model->getSetting('social_notification_google_analytics') ? '""' : 'checked="checked"'; ?> />
                    <label for="social_notification_google_analytics0"><?php echo JText::_("COM_JFBCONNECT_NO_LABEL");?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
<?php
    
?>
    <div class="tab-pane" id="social_misc">
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_SOCIAL_SETTING');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_TAG_ADMIN_KEY_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_TAG_ADMIN_KEY_LABEL');?></div>
            <div class="config_option">
                <input type="text" name="social_tag_admin_key"  value="<?php echo $model->getSetting('social_tag_admin_key') ?>" size="20">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_SOCIAL_POINTS_INTEGRATION_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_POINTS_INTEGRATION_LABEL');?></div>
            <div class="config_option">
                <fieldset id="social_alphauserpoints_enabled" class="radio">
                                    <input type="radio" id="social_alphauserpoints_enabled1" name="social_alphauserpoints_enabled" value="1" <?php echo $model->getSetting('social_alphauserpoints_enabled') ? 'checked="checked"' : ""; ?> />
                                    <label for="social_alphauserpoints_enabled1"><?php echo JText::_('COM_JFBCONNECT_ENABLED_LABEL');?></label>
                                    <input type="radio" id="social_alphauserpoints_enabled0" name="social_alphauserpoints_enabled" value="0" <?php echo $model->getSetting('social_alphauserpoints_enabled') ? '""' : 'checked="checked"'; ?> />
                                    <label for="social_alphauserpoints_enabled0"><?php echo JText::_('COM_JFBCONNECT_DISABLED_LABEL');?></label>
                                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
<?php
    
?>
    <div class="tab-pane" id="social_examples">
        <p><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESC');?></p>
        <p><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESC2');?></p>

        <p> </p>
        <h3><?php echo JText::_('COM_JFBCONNECT_SOCIAL_COMMENTS');?>:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCComments}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCComments href=http://www.sourcecoast.com width=550 num_posts=10 colorscheme=dark mobile=false order_by=time}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">href</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_COMMENTS_HREF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">width</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="odd">500</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_WIDTH_DESC');?></td>
            </tr>
            <tr>
                <td class="even">num_posts</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="even">2</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_COMMENTS_NUM_POSTS');?></td>
            </tr>
            <tr>
                <td class="odd">colorscheme</td>
                <td class="odd">light, dark</td>
                <td class="odd">light</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_COLOR_SCHEME_DESC');?></td>
            </tr>
            <tr>
                <td class="even">mobile</td>
                <td class="even">true, 1, false, 0</td>
                <td class="even">true</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_MOBILE_DESC');?></td>
            </tr>
            <tr>
                <td class="even">order_by</td>
                <td class="even">social, reverse_time, time</td>
                <td class="even">social</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_ORDERBY_DESC');?></td>
            </tr>
            <tr>
                <td class="even">key</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3>Comments Count:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCCommentsCount}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCCommentsCount href=http://www.sourcecoast.com}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">href</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_COMMENTS_COUNT_HREF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">key</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="odd"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3>Fan:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCFan}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCFan height=200 width=200 colorscheme=light href=http://www.sourcecoast.com show_faces=true stream=false header=true border_color=blue force_wall=false}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">height</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="even">200</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_HEIGHT_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">width</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="odd">292</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_WIDTH_DESC');?></td>
            </tr>
            <tr>
                <td class="even">colorscheme</td>
                <td class="even">light, dark</td>
                <td class="even">light</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_COLOR_SCHEME_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">href</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="odd"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FAN_HREF_DESC');?></td>
            </tr>
            <tr>
                <td class="even">show_faces</td>
                <td class="even">true, 1, false, 0</td>
                <td class="even">true</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FAN_SHOW_FACES_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">stream</td>
                <td class="odd">true, 1, false, 0</td>
                <td class="odd">true</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FAN_STREAM_DESC');?></td>
            </tr>
            <tr>
                <td class="even">header</td>
                <td class="even">true, 1, false, 0</td>
                <td class="even">true</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FAN_HEADER_DESC');?>
                </td>
            </tr>
            <tr>
                <td class="odd">border_color</td>
                <td class="odd">black, red, blue, etc.</td>
                <td class="odd">black</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BORDER_DESC');?></td>
            </tr>
            <tr>
                <td class="even">force_wall</td>
                <td class="even">true, 1, false, 0</td>
                <td class="even">false</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FAN_FORCE_WALL_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">key</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="odd"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3>Feed:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCFeed}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCFeed site=http://www.sourcecoast.com height=300 width=300 colorscheme=light font=Verdana recommendations=true header=false link_target=_top}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">site</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SITE_DEFAULT');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SITE_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">height</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="odd">300</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_HEIGHT_DESC');?></td>
            </tr>
            <tr>
                <td class="even">width</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="even">300</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_WIDTH_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">colorscheme</td>
                <td class="odd">light, dark</td>
                <td class="odd">light</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_COLOR_SCHEME_DESC');?></td>
            </tr>
            <tr>
                <td class="even">font</td>
                <td class="even">Arial, Lucida Grande, Segoe UI, Tahoma, Trebuchet MS, Verdana</td>
                <td class="even">Arial</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FONT_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">recommendations</td>
                <td class="odd">true, 1, false, 0</td>
                <td class="odd">true</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FEED_RECOMMENDATIONS');?></td>
            </tr>
            <tr>
                <td class="even">header</td>
                <td class="even">true, 1, false, 0</td>
                <td class="even">true</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_HEADER_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">link_target</td>
                <td class="odd">_blank, _top, _parent</td>
                <td class="odd">_blank</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_LINK_TARGET_DESC');?></td>
            </tr>
            <tr>
                <td class="even">filter</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_ALPHANUMERIC_VALUE');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FEED_FILTER_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">action</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_ACTION_VALUE');?></td>
                <td class="odd"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_ACTION_DESC');?></td>
            </tr>
            <tr>
                <td class="even">ref</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_REF_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SEND_REF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">max_age</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_MAXAGE_OPTIONS');?></td>
                <td class="odd">0</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_MAXAGE_DESC');?></td>
            </tr>
            <tr>
                <td class="even">key</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3>Friends:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCFriends}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCFriends href=http://www.sourcecoast.com max_rows=5 width=400 colorscheme=dark size=small show_count=true action=vote,comment}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">href</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FRIENDS_HREF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">max_rows</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="odd">1</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_MAX_ROWS_DESC');?></td>
            </tr>
            <tr>
                <td class="even">width</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="even">200</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_WIDTH_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">colorscheme</td>
                <td class="odd">light, dark</td>
                <td class="odd">light</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_COLOR_SCHEME_DESC');?></td>
            </tr>
            <tr>
                <td class="even">size</td>
                <td class="even">small, large</td>
                <td class="even">small</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SIZE_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">show_count</td>
                <td class="odd">true, 1, false, 0</td>
                <td class="odd">true</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SHOW_COUNT_DESC');?></td>
            </tr>
            <tr>
                <td class="even">action</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_ACTION_VALUE');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_ACTION_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">key</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="odd"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3><?php echo JText::_('COM_JFBCONNECT_SOCIAL_LIKE_LABEL');?>:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCLike}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCLike href=http://www.sourcecoast.com layout=standard show_faces=true show_send_button=true width=300 action=like font=Verdana colorscheme=light ref=homepage}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">href</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PAGE_DEFAULT');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_LIKE_HREF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">layout</td>
                <td class="odd">standard, box_count, button_count</td>
                <td class="odd">standard</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_LAYOUT_DESC');?></td>
            </tr>
            <tr>
                <td class="even">show_faces</td>
                <td class="even">true, 1, false, 0</td>
                <td class="even">true</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SHOW_FACES_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">show_send_button</td>
                <td class="odd">true, 1, false, 0</td>
                <td class="odd">true</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SEND_BUTTON_DESC');?></td>
            </tr>
            <tr>
                <td class="even">width</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="even">450</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_WIDTH_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">action</td>
                <td class="odd">like, recommend</td>
                <td class="odd">like</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_ACTION_DESC');?></td>
            </tr>
            <tr>
                <td class="even">font</td>
                <td class="even">Arial, Lucida Grande, Segoe UI, Tahoma, Trebuchet MS, Verdana</td>
                <td class="even">Arial</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FONT_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">colorscheme</td>
                <td class="odd">light, dark</td>
                <td class="odd">light</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_COLOR_SCHEME_DESC');?></td>
            </tr>
            <tr>
                <td class="even">ref</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_REF_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SEND_REF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">key</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="odd"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3>Send:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCSend}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCSend href=http://www.sourcecoast.com font=Verdana colorscheme=light ref=homepage}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">href</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PAGE_DEFAULT');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_LIKE_HREF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">font</td>
                <td class="odd">Arial, Lucida Grande, Segoe UI, Tahoma, Trebuchet MS, Verdana</td>
                <td class="odd">Arial</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FONT_DESC');?></td>
            </tr>
            <tr>
                <td class="even">colorscheme</td>
                <td class="even">light, dark</td>
                <td class="even">light</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_COLOR_SCHEME_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">ref</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_REF_OPTIONS');?></td>
                <td class="odd">homepage</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SEND_REF_DESC');?></td>
            </tr>
            <tr>
                <td class="even">key</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3>Recommendations:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCRecommendations}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCRecommendations site=http://www.sourcecoast.com width=350 height=350 colorscheme=light header=false font=Verdana link_target=_top}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">site</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SITE_DEFAULT');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SITE_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">width</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="odd">300</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_WIDTH_DESC');?></td>
            </tr>
            <tr>
                <td class="even">height</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="even">300</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_HEIGHT_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">colorscheme</td>
                <td class="odd">light, dark</td>
                <td class="odd">light</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_COLOR_SCHEME_DESC');?></td>
            </tr>
            <tr>
                <td class="even">header</td>
                <td class="even">true, 1, false, 0</td>
                <td class="even">true</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_HEADER_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">font</td>
                <td class="odd">Arial, Lucida Grande, Segoe UI, Tahoma, Trebuchet MS, Verdana</td>
                <td class="odd">Arial</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FONT_DESC');?></td>
            </tr>
            <tr>
                <td class="even">link_target</td>
                <td class="even">_blank, _top, _parent</td>
                <td class="even">_blank</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_LINK_TARGET_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">action</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_ACTION_VALUE');?></td>
                <td class="odd"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_ACTION_DESC');?></td>
            </tr>
            <tr>
                <td class="even">ref</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_REF_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SEND_REF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">max_age</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_MAXAGE_OPTIONS');?></td>
                <td class="odd">0</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_MAXAGE_DESC');?></td>
            </tr>
            <tr>
                <td class="even">key</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
    <h3>RecommendationsBar:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCRecommendationsBar}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCRecommendationsBar href=http://www.sourcecoast.com trigger=onvisible read_time=30 action=like side=right}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">href</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PAGE_DEFAULT');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECOMMENDATIONS_HREF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">trigger</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_TRIGGER_OPTIONS');?></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_TRIGGER_DEFAULT');?></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_TRIGGER_DESC');?></td>
            </tr>
            <tr>
                <td class="even">read_time</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?>. <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_READ_TIME_OPTIONS');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_READ_TIME_DEFAULT');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_READ_TIME_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">action</td>
                <td class="odd">like, recommend</td>
                <td class="odd">like</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_ACTION_DESC');?></td>
            </tr>
            <tr>
                <td class="even">side</td>
                <td class="even">left, right</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_SIDE_DEFAULT');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_SIDE_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">site</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_SITE_DEFAULT');?></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_SITE_DESC');?></td>
            </tr>
            <tr>
                <td class="even">ref</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_REF_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECOMMENDATIONS_REF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">num_recommendations</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?>. <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_NUM_REC_OPTIONS');?></td>
                <td class="odd">2</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_NUM_REC_DESC');?></td>
            </tr>
            <tr>
                <td class="even">max_age</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_MAXAGE_OPTIONS');?></td>
                <td class="even">0</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_RECBAR_MAXAGE_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">key</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="odd"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3>Subscribe:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCSubscribe}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCSubscribe href=https://www.facebook.com/zuck layout=standard show_faces=true width=300 action=like font=Verdana colorscheme=light}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">href</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SUBSCRIBE_HREF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">layout</td>
                <td class="odd">standard, box_count, button_count</td>
                <td class="odd">standard</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_LAYOUT_DESC');?></td>
            </tr>
            <tr>
                <td class="even">show_faces</td>
                <td class="even">true, 1, false, 0</td>
                <td class="even">true</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SHOW_FACES_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">width</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="odd">450</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_WIDTH_DESC');?></td>
            </tr>
            <tr>
                <td class="even">font</td>
                <td class="even">Arial, Lucida Grande, Segoe UI, Tahoma, Trebuchet MS, Verdana</td>
                <td class="even">Arial</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_FONT_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">colorscheme</td>
                <td class="odd">light, dark</td>
                <td class="odd">light</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_COLOR_SCHEME_DESC');?></td>
            </tr>
            <tr>
                <td class="even">key</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3>Request:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCRequest request_id=1 link_image=http://www.sourcecoast.com/images/stories/extensions/jfbconnect/home_jfbconn.png}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCRequest request_id=1 link_text=Invite Friends}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">request_id</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_REQUEST_ID_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_REQUEST_ID_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">link_text</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_REQUEST_LINK_TEXT_OPTIONS');?></td>
                <td class="odd"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_REQUEST_LINK_TEXT_DESC');?></td>
            </tr>
            <tr>
                <td class="even">link_image</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_REQUEST_LINK_IMG_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">key</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="odd"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3>Login:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCLogin}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JFBCLogin size=medium logout=true show_faces=true max_rows=2}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">size</td>
                <td class="even">small, medium, large, xlarge</td>
                <td class="even">medium</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_LOGIN_SIZE_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">logout</td>
                <td class="odd">true, 1, false, 0</td>
                <td class="odd">false</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_LOGIN_LOGOUT_DESC');?></td>
            </tr>
            <tr>
                <td class="even">show_faces</td>
                <td class="even">true, 1, false, 0</td>
                <td class="even">false</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_LOGIN_SHOW_FACES_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">max_rows</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_INTEGER_VALUE');?></td>
                <td class="odd">1</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_LOGIN_MAXROWS_DESC');?></td>
            </tr>
            <tr>
                <td class="even">key</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3>Graph:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_GRAPH_URL');?><br/><br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {SCOpenGraph url=http://www.sourcecoast.com}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {SCOpenGraph image=http://www.sourcecoast.com/images/stories/extensions/jfbconnect/home_jfbconn.jpg}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {SCOpenGraph description=Facebook connect integration for Joomla! Let users register and log into your site with their Facebook credentials.}
        <br/><br/><strong><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_GRAPH_INSTRUCTIONS');?></em></strong><br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?></th>
            </tr>
            <tr>
                <td class="even">title</td>
                <td class="even">title=JFBConnect</td>
            </tr>
            <tr>
                <td class="odd">type</td>
                <td class="odd">type=company</td>
            </tr>
            <tr>
                <td class="even">url</td>
                <td class="even">url=http://joomla-facebook.com</td>
            </tr>
            <tr>
                <td class="odd">image</td>
                <td class="odd">image=http://www.sourcecoast.com/images/stories/extensions/jfbconnect/home_jfbconn.jpg</td>
            </tr>
            <tr>
                <td class="even">site_name</td>
                <td class="even">site_name=SourceCoast</td>
            </tr>
            <tr>
                <td class="odd">description</td>
                <td class="odd">description=Joomla Facebook Connect integration, payment systems, and custom Joomla development
                    based in Austin, TX
                </td>
            </tr>
        </table>
        <p></p>
        <h3>Twitter Share Button:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {SCTwitterShare}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {SCTwitterShare href=http://www.sourcecoast.com data_count=horizontal}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">href</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PAGE_DEFAULT');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SHARE_HREF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">data_count</td>
                <td class="odd">horizontal, vertical, none</td>
                <td class="odd">horizontal</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_TWITTER_DATA_COUNT_DESC');?></td>
            </tr>
            <tr>
                <td class="even">key</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3>Google +1 Button:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {SCGooglePlusOne}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {SCGooglePlusOne href=http://www.sourcecoast.com annotation=inline size=standard}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">href</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PAGE_DEFAULT');?></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_SHARE_HREF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">annotation</td>
                <td class="odd">inline, bubble, none</td>
                <td class="odd">inline</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_GOOGLEPLUS_ANNOTATION_DESC');?></td>
            </tr>
            <tr>
                <td class="even">size</td>
                <td class="even">small, medium, standard, tall</td>
                <td class="even">standard</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_GOOGLEPLUS_SIZE_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">key</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="odd"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
        <p></p>
        <h3>LinkedIn Share Button:</h3>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JLinkedShare}<br/>
        <?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_EXAMPLE');?>: {JLinkedShare href=http://www.sourcecoast.com/jlinked/ counter=top}<br/>
        <br/>
        <table class="options">
            <tr>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_PARAMETER');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_OPTIONS');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DEFAULT');?></th>
                <th><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_DESCRIPTION');?></th>
            </tr>
            <tr>
                <td class="even">href</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_VALID_URL');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_JLINKED_HREF_DESC');?></td>
            </tr>
            <tr>
                <td class="odd">counter</td>
                <td class="odd">top, right, no_count</td>
                <td class="odd">no_count</td>
                <td class="odd"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_JLINKED_COUNTER_DESC');?></td>
            </tr>
            <tr>
                <td class="even">key</td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_OPTIONS');?></td>
                <td class="even"><em><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_BLANK');?></em></td>
                <td class="even"><?php echo JText::_('COM_JFBCONNECT_SOCIAL_EXAMPLES_KEY_DESC');?></td>
            </tr>
        </table>
    </div>
<?php
    
    echo '</div>';
     //SC30
     //SC16
?>
    <input type="hidden" name="option" value="com_jfbconnect" />
    <input type="hidden" name="controller" value="social" />
    <input type="hidden" name="cid[]" value="0" />
    <input type="hidden" name="task" value="" />
    <?php echo JHTML::_('form.token'); ?>

</form>
