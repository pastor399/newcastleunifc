<?php
/**
 * @package		JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
?>
<style type="text/css">
    div.config_setting {
        width: 225px;
    }
    div.config_option {
        width: 250px;
    }
    div.config_setting_option {
        width: 350px;
    }
</style>

<form method="post" id="adminForm" name="adminForm">

<?php echo JText::_('COM_JFBCONNECT_PROFILES_DESC');?>
<?php
if (count($this->profilePlugins) > 0)
{
    
    $activeStr = ' class="active"';
    echo '<div class="row-fluid">';
    echo '<ul class="nav nav-tabs">';
    foreach ($this->profilePlugins as $profile)
    {
        echo '<li'.$activeStr.'><a href="#'.$profile->getName().'" data-toggle="tab">'.JText::_($profile->getName()).'</a></li>';
        $activeStr = '';
    }
    echo '</ul>';
    echo '</div>';

    //Begin Tabs
    $activeStr = ' active';
    echo '<div class="tab-content">';
    foreach ($this->profilePlugins as $profile)
    {
        echo '<div class="tab-pane'.$activeStr.'" id="'.$profile->getName().'">';
        echo $profile->getConfigurationTemplate('facebook');
        echo '</div>';
        $activeStr = '';
    }
    echo '</div>';
     //SC30

     //SC16
}
else
{
    echo JText::_('COM_JFBCONNECT_PROFILES_NO_PROFILE_PLUGINS');
}
?>

    <input type="hidden" name="option" value="com_jfbconnect" />
    <input type="hidden" name="controller" value="profiles" />
    <input type="hidden" name="task" value="" />
<?php echo JHTML::_('form.token'); ?>

</form>