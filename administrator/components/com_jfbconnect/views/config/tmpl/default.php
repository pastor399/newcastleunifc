<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.html.sliders');
JHTML::_('behavior.tooltip');

$model = $this->model;
$isLocaleSupported = $this->isLocaleSupported;

 //SC16


jimport('sourcecoast.adminHelper');
$menuItems = SCAdminHelper::menu_linkoptions();
 //SC30
?>
<form method="post" id="adminForm" name="adminForm">
<?php

?>
<div class="row-fluid">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#config_user" data-toggle="tab"><?php echo JText::_('COM_JFBCONNECT_CONFIG_MENU_USER');?></a></li>
        <li><a href="#config_redirection" data-toggle="tab"><?php echo JText::_('COM_JFBCONNECT_CONFIG_MENU_LOGIN_LOGOUT');?></a></li>
        <li><a href="#config_status" data-toggle="tab"><?php echo JText::_('COM_JFBCONNECT_CONFIG_MENU_STATUS_WALL');?></a></li>
        <li><a href="#config_facebook_api" data-toggle="tab"><?php echo JText::_('COM_JFBCONNECT_CONFIG_MENU_FACEBOOK_API');?></a></li>
        <li><a href="#config_misc" data-toggle="tab"><?php echo JText::_('COM_JFBCONNECT_CONFIG_MENU_MISC');?></a></li>
    </ul>
</div>
<div class="tab-content">
<?php
 //SC30
 //SC16
?>
    <div class="tab-pane active" id="config_user">
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_SETTING');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REGISTRATION_FLOW_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REGISTRATION_FLOW_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="create_new_users" class="radio">
                    <input type="radio" id="create_new_users1" name="create_new_users" value="1"
                           onclick="jfbcAdmin.config.setUserCreation('fullJoomla')" <?php echo $model->getSetting('create_new_users')
                            ? 'checked="checked"' : ""; ?> onclick="jfbcAdmin.config.setUserCreation('fullJoomla')">
                    <label for="create_new_users1"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REGISTRATION_FLOW_RADIO1');?></label>
                    <input type="radio" id="create_new_users0" name="create_new_users" value="0"
                           onclick="jfbcAdmin.config.setUserCreation('facebookOnly')" <?php echo $model->getSetting('create_new_users')
                            ? '""' : 'checked="checked"'; ?> onclick="jfbcAdmin.config.setUserCreation('facebookOnly')">
                    <label for="create_new_users0"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REGISTRATION_FLOW_RADIO2');?></label>
                </fieldset>
            </div>
             <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_AUTOMATIC_FB_LINK_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_AUTOMATIC_FB_LINK_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="facebook_auto_map_by_email" class="radio">
                    <input type="radio" id="facebook_auto_map_by_email1" name="facebook_auto_map_by_email"
                           value="1" <?php echo $model->getSetting('facebook_auto_map_by_email') ? 'checked="checked"'
                            : ""; ?> />
                    <label for="facebook_auto_map_by_email1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                    <input type="radio" id="facebook_auto_map_by_email0" name="facebook_auto_map_by_email"
                           value="0" <?php echo $model->getSetting('facebook_auto_map_by_email') ? '""'
                            : 'checked="checked"'; ?>  />
                    <label for="facebook_auto_map_by_email0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_SKIP_JOOMLA_ACTIVATION_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_SKIP_JOOMLA_ACTIVATION_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="joomla_skip_newuser_activation" class="radio">
                    <input type="radio" id="joomla_skip_newuser_activation1" name="joomla_skip_newuser_activation"
                           value="1" <?php echo $model->getSetting('joomla_skip_newuser_activation') ? 'checked="checked"'
                            : ""; ?> />
                    <label for="joomla_skip_newuser_activation1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                    <input type="radio" id="joomla_skip_newuser_activation0" name="joomla_skip_newuser_activation"
                           value="0" <?php echo $model->getSetting('joomla_skip_newuser_activation') ? '""'
                            : 'checked="checked"'; ?>  />
                    <label for="joomla_skip_newuser_activation0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="config_row fullJoomla">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_GENERATE_USERNAME_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_GENERATE_USERNAME_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="registration_generate_username" class="radio">
                    <input type="radio" id="registration_generate_username1" name="registration_generate_username" value="1" <?php echo $model->getSetting('registration_generate_username') ? 'checked="checked"' : ""; ?> />
                    <label for="registration_generate_username1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                    <input type="radio" id="registration_generate_username0" name="registration_generate_username" value="0" <?php echo $model->getSetting('registration_generate_username') ? '""' : 'checked="checked"'; ?>  />
                    <label for="registration_generate_username0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_AUTO_USERNAME_PREFIX_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_AUTO_USERNAME_PREFIX_LABEL');?>:</div>
            <div class="config_option">
                <select name="auto_username_format">
                    <option value="0" <?php echo ($model->getSetting('auto_username_format') == '0') ? 'selected' : ""; ?>>
                        <?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_AUTO_USERNAME_PREFIX_FORMAT0');?>
                    </option>
                    <option value="1" <?php echo ($model->getSetting('auto_username_format') == '1') ? 'selected' : ""; ?>>
                        <?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_AUTO_USERNAME_PREFIX_FORMAT1');?>
                    </option>
                    <option value="2" <?php echo ($model->getSetting('auto_username_format') == '2') ? 'selected' : ""; ?>>
                        <?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_AUTO_USERNAME_PREFIX_FORMAT2');?>
                    </option>
                    <option value="3" <?php echo ($model->getSetting('auto_username_format') == '3') ? 'selected' : ""; ?>>
                        <?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_AUTO_USERNAME_PREFIX_FORMAT3');?>
                    </option>
                </select>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_RANDOM_PASSWORD_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_RANDOM_PASSWORD_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="generate_random_password" class="radio">
                    <input type="radio" id="generate_random_password1" name="generate_random_password" value="1" <?php echo $model->getSetting('generate_random_password') ? 'checked="checked"' : ""; ?> />
                    <label for="generate_random_password1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                    <input type="radio" id="generate_random_password0" name="generate_random_password" value="0" <?php echo $model->getSetting('generate_random_password') ? '""' : 'checked="checked"'; ?>  />
                    <label for="generate_random_password0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row fullJoomla">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_FB_REG_DISPLAY_SETTING');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row fullJoomla">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REG_DISPLAY_MODE_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REG_DISPLAY_MODE_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="registration_display_mode" class="radio">
                    <?php $setting = $model->getSetting('registration_display_mode'); ?>
                    <input type="radio" id="registration_display_mode_horizontal" name="registration_display_mode"
                           value="horizontal" <?php if ($setting == 'horizontal') echo 'checked="checked"'; ?> />
                    <label for="registration_display_mode_horizontal"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REG_DISPLAY_MODE_RADIO1');?></label>
                    <input type="radio" id="registration_display_mode_vertical" name="registration_display_mode"
                           value="vertical" <?php if ($setting == 'vertical') echo 'checked="checked"'; ?> />
                    <label for="registration_display_mode_vertical"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REG_DISPLAY_MODE_RADIO2');?></label>
                    <input type="radio" id="registration_display_mode_register_only" name="registration_display_mode"
                           value="register-only" <?php if ($setting == 'register-only') echo 'checked="checked"'; ?> />
                    <label for="registration_display_mode_register_only"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REG_DISPLAY_MODE_RADIO3');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row fullJoomla">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REG_EMAIL_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REG_EMAIL_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="registration_show_email" class="radio">
                    <input type="radio" id="registration_show_email0" name="registration_show_email"
                           value="0" <?php echo $model->getSetting('registration_show_email') ? '""'
                            : 'checked="checked"'; ?> />
                    <label for="registration_show_email0"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_HIDE');?></label>
                    <input type="radio" id="registration_show_email1" name="registration_show_email"
                           value="1" <?php echo $model->getSetting('registration_show_email') ? 'checked="checked"'
                            : ""; ?>  />
                    <label for="registration_show_email1"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_SHOW');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row fullJoomla">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REG_USERNAME_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REG_USERNAME_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="registration_show_username" class="radio">
                    <input type="radio" id="registration_show_username0" name="registration_show_username"
                           value="0" <?php echo $model->getSetting('registration_show_username') ? '""'
                            : 'checked="checked"'; ?> />
                    <label for="registration_show_username0"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_HIDE');?></label>
                    <input type="radio" id="registration_show_username1" name="registration_show_username"
                           value="1" <?php echo $model->getSetting('registration_show_username') ? 'checked="checked"'
                            : ""; ?>  />
                    <label for="registration_show_username1"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_SHOW');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row fullJoomla">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REG_PASSWORD_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_REG_PASSWORD_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="registration_show_password" class="radio">
                    <input type="radio" id="registration_show_password0" name="registration_show_password"
                           value="0" <?php echo $model->getSetting('registration_show_password') ? '""'
                            : 'checked="checked"'; ?> />
                    <label for="registration_show_password0"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_HIDE');?></label>
                    <input type="radio" id="registration_show_password1" name="registration_show_password"
                           value="1" <?php echo $model->getSetting('registration_show_password') ? 'checked="checked"'
                            : ""; ?>  />
                    <label for="registration_show_password1"><?php echo JText::_('COM_JFBCONNECT_CONFIG_USER_SHOW');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
    <?php
     //SC16
    ?>
    <div class="tab-pane" id="config_redirection">
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_FB_LOGIN_REDIRECTION_SETTING');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>

        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_EN_NEW_USER_REDIRECTION_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_EN_NEW_USER_REDIRECTION_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="facebook_new_user_redirect_enable" class="radio">
                    <input type="radio" id="facebook_new_user_redirect_enable1" name="facebook_new_user_redirect_enable"
                           value="1" <?php echo $model->getSetting('facebook_new_user_redirect_enable')
                            ? 'checked="checked"' : ""; ?>
                           onclick="jfbcAdmin.config.showOptions('newUserRedirection', true)"/>
                    <label for="facebook_new_user_redirect_enable1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                    <input type="radio" id="facebook_new_user_redirect_enable0" name="facebook_new_user_redirect_enable"
                           value="0" <?php echo $model->getSetting('facebook_new_user_redirect_enable') ? '""'
                            : 'checked="checked"'; ?>  onclick="jfbcAdmin.config.showOptions('newUserRedirection', false)"/>
                    <label for="facebook_new_user_redirect_enable0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row newUserRedirection">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_NEW_USER_REDIRECTION_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_NEW_USER_REDIRECTION_LABEL');?>:</div>
            <div class="config_option">
                <?php
                $selected = $model->getSetting('facebook_new_user_redirect');
                echo JHTML::_('select.genericlist', $menuItems, 'facebook_new_user_redirect', null, 'value', 'text', $selected);
                ?>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_EN_RET_USER_REDIRECTION_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_EN_RET_USER_REDIRECTION_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="facebook_login_redirect_enable" class="radio">
                    <input type="radio" id="facebook_login_redirect_enable1" name="facebook_login_redirect_enable"
                           value="1" <?php echo $model->getSetting('facebook_login_redirect_enable') ? 'checked="checked"'
                            : ""; ?> onclick="jfbcAdmin.config.showOptions('loginRedirection', true)"/>
                    <label for="facebook_login_redirect_enable1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                    <input type="radio" id="facebook_login_redirect_enable0" name="facebook_login_redirect_enable"
                           value="0" <?php echo $model->getSetting('facebook_login_redirect_enable') ? '""'
                            : 'checked="checked"'; ?>  onclick="jfbcAdmin.config.showOptions('loginRedirection', false)"/>
                    <label for="facebook_login_redirect_enable0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row loginRedirection">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_RET_USER_REDIRECTION_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_RET_USER_REDIRECTION_LABEL');?>:</div>
            <div class="config_option">
                <?php
                $selected = $model->getSetting('facebook_login_redirect');
                echo JHTML::_('select.genericlist', $menuItems, 'facebook_login_redirect', null, 'value', 'text', $selected);
                ?>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_FB_LOGIN_SETTING');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_AUTO_LOGIN_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_AUTO_LOGIN_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="facebook_auto_login" class="radio facebookAutoLogin">
                    <input type="radio" id="facebook_auto_login1" name="facebook_auto_login"
                           value="1" <?php echo $model->getSetting('facebook_auto_login') ? 'checked="checked"' : ""; ?>
                            />
                    <label for="facebook_auto_login1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                    <input type="radio" id="facebook_auto_login0" name="facebook_auto_login"
                           value="0" <?php echo $model->getSetting('facebook_auto_login') ? '""'
                            : 'checked="checked"'; ?> />
                    <label for="facebook_auto_login0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_LOGOUT_JOOMLA_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_LOGOUT_JOOMLA_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="logout_joomla_only" class="radio facebookLogout">
                    <input type="radio" id="logout_joomla_only1" name="logout_joomla_only"
                           value="1" <?php echo $model->getSetting('logout_joomla_only') ? 'checked="checked"' : ""; ?>
                            />
                    <label for="logout_joomla_only1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                    <input type="radio" id="logout_joomla_only0" name="logout_joomla_only"
                           value="0" <?php echo $model->getSetting('logout_joomla_only') ? '""' : 'checked="checked"'; ?> />
                    <label for="logout_joomla_only0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_INTEGRATE_JOOMLA_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_INTEGRATE_JOOMLA_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="show_login_with_joomla_reg" class="radio">
                    <input type="radio" id="show_login_with_joomla_reg1" name="show_login_with_joomla_reg" value="1" <?php echo $model->getSetting('show_login_with_joomla_reg') ? 'checked="checked"': ""; ?> />
                    <label for="show_login_with_joomla_reg1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                    <input type="radio" id="show_login_with_joomla_reg0" name="show_login_with_joomla_reg" value="0" <?php echo $model->getSetting('show_login_with_joomla_reg') ? '""': 'checked="checked"'; ?>  />
                    <label for="show_login_with_joomla_reg0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_SHOW_POPUP_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_SHOW_POPUP_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="facebook_login_show_modal" class="radio">
                    <input type="radio" id="facebook_login_show_modal1" name="facebook_login_show_modal" value="1" <?php echo $model->getSetting('facebook_login_show_modal') ? 'checked="checked"': ""; ?> />
                    <label for="facebook_login_show_modal1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                    <input type="radio" id="facebook_login_show_modal0" name="facebook_login_show_modal" value="0" <?php echo $model->getSetting('facebook_login_show_modal') ? '""': 'checked="checked"'; ?>  />
                    <label for="facebook_login_show_modal0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_FB_REDIRECTION_SETTING');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_EN_LOGOUT_REDIRECTION_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_EN_LOGOUT_REDIRECTION_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="facebook_logout_redirect_enable" class="radio">
                    <input type="radio" id="facebook_logout_redirect_enable1" name="facebook_logout_redirect_enable"
                           value="1" <?php echo $model->getSetting('facebook_logout_redirect_enable') ? 'checked="checked"'
                            : ""; ?> onclick="jfbcAdmin.config.showOptions('logoutRedirection', true)"/>
                    <label for="facebook_logout_redirect_enable1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                    <input type="radio" id="facebook_logout_redirect_enable0" name="facebook_logout_redirect_enable"
                           value="0" <?php echo $model->getSetting('facebook_logout_redirect_enable') ? '""'
                            : 'checked="checked"'; ?> onclick="jfbcAdmin.config.showOptions('logoutRedirection', false)"/>
                    <label for="facebook_logout_redirect_enable0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row logoutRedirection">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_LOGOUT_REDIRECTION_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_LOGIN_LOGOUT_REDIRECTION_LABEL');?>:</div>
            <div class="config_option">
                <?php
                $selected = $model->getSetting('facebook_logout_redirect');
                echo JHTML::_('select.genericlist', $menuItems, 'facebook_logout_redirect', null, 'value', 'text', $selected);
                ?>
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
    <?php
     //SC16
    ?>
    <div class="tab-pane" id="config_status">
        <div class="config_row">
            <p><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_COMPLIANCE_DESC');?></p>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_NEW_USER_STATUS_SETTINGS');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_OPTIONS');?></div>
            <div style="clear:both"></div>
            <div><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_NEW_USER_STATUS_SETTINGS_DESC');?></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_NEW_USER_MESSAGE_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_MESSAGE_LABEL');?>:</div>
            <div class="config_option">
                <textarea rows="3" cols="35"
                          name="facebook_new_user_status_msg"><?php echo $model->getSetting('facebook_new_user_status_msg'); ?></textarea>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_NEW_USER_URL_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_URL_LABEL');?>:</div>
            <div class="config_option">
                <input type="text" name="facebook_new_user_status_link"
                       value="<?php echo $model->getSetting('facebook_new_user_status_link') ?>" size="40">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_NEW_USER_PICTURE_URL_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_PICTURE_URL_LABEL');?>:</div>
            <div class="config_option"><input type="text" name="facebook_new_user_status_picture"
                                              value="<?php echo $model->getSetting('facebook_new_user_status_picture') ?>"
                                              size="40"></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_RET_USER_STATUS_SETTINGS');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_OPTIONS');?></div>
            <div style="clear:both"></div>
            <div><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_RET_USER_STATUS_SETTINGS_DESC');?></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_RET_USER_MESSAGE_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_MESSAGE_LABEL');?>:</div>
            <div class="config_option">
                <textarea rows="3" cols="35"
                          name="facebook_login_status_msg"><?php echo $model->getSetting('facebook_login_status_msg'); ?></textarea>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_RET_USER_URL_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_URL_LABEL');?>:</div>
            <div class="config_option">
                <input type="text" name="facebook_login_status_link"
                       value="<?php echo $model->getSetting('facebook_login_status_link') ?>" size="40">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_RET_USER_PICTURE_URL_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_STATUS_PICTURE_URL_LABEL');?>:</div>
            <div class="config_option">
                <input type="text" name="facebook_login_status_picture"
                       value="<?php echo $model->getSetting('facebook_login_status_picture') ?>" size="40">
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
    <?php
     //SC16
    ?>
    <div class="tab-pane" id="config_facebook_api">
        <div>
            <div class="config_row">
                <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_SETTING');?></div>
                <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_OPTIONS');?></div>
                <div style="clear:both"></div>
            </div>
            <div class="config_row">
                <div class="config_setting"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_APP_ID_LABEL');?>:</div>
                <div class="config_option"><input type="text" name="facebook_app_id"
                                                  value="<?php echo $model->getSetting('facebook_app_id') ?>" size="50"></div>
                <div style="clear:both"></div>
            </div>
            <div class="config_row">
                <div class="config_setting"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_SECRET_KEY_LABEL');?>:</div>
                <div class="config_option"><input type="text" name="facebook_secret_key"
                                                  value="<?php echo $model->getSetting('facebook_secret_key') ?>" size="50">
                </div>
                <div style="clear:both"></div>
            </div>
        </div>
        <br/>
        <div>
            <div>
                <div class="config_row">
                    <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_FB_PERMISSIONS_SETTING');?></div>
                    <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_OPTIONS');?></div>
                    <div style="clear:both"></div>
                    <div><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_FB_PERMISSIONS_SETTING_DESC');?></div>
                </div>

                <div class="config_row">
                    <div class="config_setting hasTip"
                         title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_ADD_PERMISSION_REQ_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_ADD_PERMISSION_REQ_LABEL');?>:</div>
                    <div class="config_option">
                        <textarea rows="3" cols="35"
                                  name="facebook_perm_custom"><?php echo $model->getSetting('facebook_perm_custom'); ?></textarea>
                    </div>
                    <div class="config_description"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_ADD_PERMISSION_REQ_DESC2');?></div>
                    <div style="clear:both"></div>
                </div>
            </div>
            <br/>
        </div>
        <div>
            <div>
                <div class="config_row">
                    <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_FB_DEBUG_SETTING');?></div>
                    <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_OPTIONS');?></div>
                    <div style="clear:both"></div>
                    <div><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_FB_DEBUG_SETTING_DESC');?></div>
                </div>
                <div class="config_row">
                    <div class="config_setting hasTip"
                         title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_DISPLAY_ERRORS_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_DISPLAY_ERRORS_LABEL');?>:</div>
                    <div class="config_option">
                        <fieldset id="facebook_display_errors" class="radio">
                            <input type="radio" id="facebook_display_errors1" name="facebook_display_errors"
                                   value="1" <?php echo $model->getSetting('facebook_display_errors') ? 'checked="checked"'
                                    : ""; ?>  />
                            <label for="facebook_display_errors1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                            <input type="radio" id="facebook_display_errors0" name="facebook_display_errors"
                                   value="0" <?php echo $model->getSetting('facebook_display_errors') ? '""'
                                    : 'checked="checked"'; ?>  />
                            <label for="facebook_display_errors0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                        </fieldset>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="config_row">
                    <div class="config_setting hasTip"
                         title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_DISABLE_SSL_VAL_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_DISABLE_SSL_VAL_LABEL');?>:</div>
                    <div class="config_option">
                        <fieldset id="facebook_curl_disable_ssl" class="radio">
                            <input type="radio" id="facebook_curl_disable_ssl1" name="facebook_curl_disable_ssl"
                                   value="1" <?php echo $model->getSetting('facebook_curl_disable_ssl')
                                    ? 'checked="checked"'
                                    : ""; ?>  />
                            <label for="facebook_curl_disable_ssl1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                            <input type="radio" id="facebook_curl_disable_ssl0" name="facebook_curl_disable_ssl"
                                   value="0" <?php echo $model->getSetting('facebook_curl_disable_ssl') ? '""'
                                    : 'checked="checked"'; ?>  />
                            <label for="facebook_curl_disable_ssl0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                        </fieldset>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="config_row">
                    <div class="config_setting hasTip"
                         title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_LANG_LOCALE_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_LANG_LOCALE_LABEL');?></div>
                    <div class="config_option">
                        <input type="text" name="facebook_language_locale"
                               value="<?php echo $model->getSetting('facebook_language_locale'); ?>" size="6" maxlength="5">
                    </div>
                    <div class="config_description">
                        <?php echo JText::_('COM_JFBCONNECT_CONFIG_FB_API_LANG_LOCALE_DESC2');?>
                        <?php
                        if (!$isLocaleSupported)
                        {
                            echo '<span style="color:red;"><?php echo JText::_("COM_JFBCONNECT_CONFIG_FB_API_LANG_LOCALE_DESC3");?></span>';
                        }
                        ?>
                    </div>
                    <div style="clear:both"></div>
                </div>

            </div>
        </div>
    </div>
    <?php
     //SC16
    ?>
    <div class="tab-pane" id="config_misc">
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_MISC_SETTING');?></div>
            <div class="config_option header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_MISC_OPTIONS');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_MISC_SOURCECOAST_SUBSCRIPTION_ID_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_MISC_SOURCECOAST_SUBSCRIPTION_ID_LABEL');?>:</div>
            <div class="config_option">
                <input type="text" name="sc_download_id" value="<?php echo $model->getSetting('sc_download_id');?>" size="40">
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_MISC_SHOW_POWERED_LINK_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_MISC_SHOW_POWERED_LINK_LABEL');?>:</div>
            <div class="config_option">
                <fieldset id="show_powered_by_link" class="radio">
                    <input type="radio" id="show_powered_by_link1" name="show_powered_by_link"
                           value="1" <?php echo $model->getSetting('show_powered_by_link') ? 'checked="checked"'
                            : ""; ?>  />
                    <label for="show_powered_by_link1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>
                    <input type="radio" id="show_powered_by_link0" name="show_powered_by_link"
                           value="0" <?php echo $model->getSetting('show_powered_by_link') ? '""'
                            : 'checked="checked"'; ?>  />
                    <label for="show_powered_by_link0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="<?php echo JText::_('COM_JFBCONNECT_CONFIG_MISC_AFFILIATE_ID_DESC');?>"><?php echo JText::_('COM_JFBCONNECT_CONFIG_MISC_AFFILIATE_ID_LABEL');?>:</div>
            <div class="config_option">
                <input type="text" name="affiliate_id" value="<?php echo $model->getSetting('affiliate_id') ?>"
                       size="40">
            </div>
            <div class="config_description"><?php echo JText::_('COM_JFBCONNECT_CONFIG_MISC_AFFILIATE_ID_DESC2');?></div>
            <div style="clear:both"></div>
        </div>
        <div class="config_row">
            <div class="config_setting header"><?php echo JText::_('COM_JFBCONNECT_CONFIG_EXPERIMENTAL');?></div>
            <div style="clear:both"></div>
        </div>
        <?php
        $experimentalSettings = $model->getSetting('experimental');
        $strip_head = $experimentalSettings->get('strip_head', true);
        ?>
        <!--- DO NOT translate these experimental settings. They are meant to be transient settings, and should be removed in a version or so.
         Will just make our language files more complex than necessary.
         It's also easier for us to reference them in support requests if we can say the exact name the user will see.
        -->
        <div class="config_row">
            <div class="config_setting hasTip"
                 title="Strip Easy-Tags from Header">Strip Easy-Tags from Header:</div>
            <div class="config_option">
                <fieldset id="experimental_strip_head" class="radio">
                    <input type="radio" id="experimental_strip_head1" name="experimental[strip_head]" value="1" <?php echo $strip_head ? 'checked="checked"' : '' ?>" />
                    <label for="experimental_strip_head1"><?php echo JText::_('COM_JFBCONNECT_YES_LABEL');?></label>

                    <input type="radio" id="experimental_strip_head0" name="experimental[strip_head]" value="0" <?php echo $strip_head ? '' : 'checked="checked"' ?>" />
                    <label for="experimental_strip_head0"><?php echo JText::_('COM_JFBCONNECT_NO_LABEL');?></label>
                </fieldset>
            </div>
            <div class="config_description">Attempt to strip {JFBCxyz} tags from the &lt;head&gt; section of output</div>
            <div style="clear:both"></div>
        </div>

    </div>
    <?php
    


echo '</div>';
 //SC30
    ?>

<input type="hidden" name="option" value="com_jfbconnect"/>
<input type="hidden" name="controller" value="config"/>
<input type="hidden" name="cid[]" value="0"/>
<input type="hidden" name="task" value=""/>
<?php echo JHTML::_('form.token'); ?>

</form>

<?php
$createUsers = $model->getSetting('create_new_users') == '1' ? 'fullJoomla' : 'facebookOnly';
$loginRedirection = $model->getSetting('facebook_login_redirect_enable') == '1' ? 'true' : 'false';
$logoutRedirection = $model->getSetting('facebook_logout_redirect_enable') == '1' ? 'true' : 'false';
$newUserRedirection = $model->getSetting('facebook_new_user_redirect_enable') == '1' ? 'true' : 'false';
?>

<script type="text/javascript">
    jfbcAdmin.config.setUserCreation('<?php echo $createUsers; ?>');
    jfbcAdmin.config.showOptions('newUserRedirection', <?php echo $newUserRedirection ?>);
    jfbcAdmin.config.showOptions('loginRedirection', <?php echo $loginRedirection ?>);
    jfbcAdmin.config.showOptions('logoutRedirection', <?php echo $logoutRedirection ?>);
</script>