<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');

require_once(JPATH_ADMINISTRATOR.'/components/com_jfbconnect/common/scconfig.php');

class JFBConnectModelConfig extends JFBCConfig
{
    var $_availableSettings = array();
    var $componentSettings = array(
        'facebook_app_id' => '',
        'facebook_secret_key' => '',
        'create_new_users' => '1',
        'registration_generate_username' => '0',
        'auto_username_format' => '0', //0 = fb_, 1=first.last, 2=firlas, 3=email
        'generate_random_password' => '1',
        'registration_show_username' => '1',
        'registration_show_password' => '1',
        'registration_show_email' => '0',
        'registration_display_mode' => 'horizontal',
        'joomla_skip_newuser_activation' => '1',
        'facebook_new_user_redirect' => "",
        'facebook_new_user_redirect_enable' => '0',
        'facebook_login_redirect' => "",
        'facebook_login_redirect_enable' => "0",
        'facebook_logout_redirect' => "",
        'facebook_logout_redirect_enable' => "0",
        'facebook_perm_custom' => '',
        'facebook_new_user_status_msg' => "",
        'facebook_new_user_status_link' => "",
        'facebook_new_user_status_picture' => "",
        'facebook_login_status_msg' => "",
        'facebook_login_status_link' => "",
        'facebook_login_status_picture' => "",
        'facebook_auto_login' => "0",
        'facebook_display_errors' => '0',
        'facebook_auto_map_by_email' => '0',
        'facebook_curl_disable_ssl' => '0',
        'facebook_language_locale' => '',
        'facebook_login_show_modal' => '0',
        'show_powered_by_link' => '1',
        'affiliate_id' => "",
        'sc_download_id' => "",
        'experimental' => "",
        'logout_joomla_only' => '0',
        'show_login_with_joomla_reg' => '1',
        'social_tag_admin_key' => '',
        'social_comment_article_include_ids' => '',
        'social_comment_article_exclude_ids' => '',
        'social_comment_cat_include_type' => '0', //0=ALL, 1=Include, 2=Exclude
        'social_comment_cat_ids' => '',
        'social_comment_sect_include_type' => '0',
        'social_comment_sect_ids' => '',
        'social_comment_article_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_comment_frontpage_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_comment_category_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_comment_section_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_article_comment_max_num' => '10',
        'social_article_comment_width' => '350',
        'social_article_comment_color_scheme' => 'light',
        'social_article_comment_order_by' => 'social',
        'social_blog_comment_max_num' => '10',
        'social_blog_comment_width' => '350',
        'social_blog_comment_color_scheme' => 'light',
        'social_blog_comment_order_by' => 'social',
        'social_k2_comment_item_include_ids' => '',
        'social_k2_comment_item_exclude_ids' => '',
        'social_k2_comment_cat_include_type' => '0', //0=ALL, 1=Include, 2=Exclude
        'social_k2_comment_cat_ids' => '',
        'social_k2_comment_item_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_k2_comment_category_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_k2_comment_tag_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_k2_comment_userpage_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_k2_comment_latest_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_k2_item_comment_max_num' => '10',
        'social_k2_item_comment_width' => '350',
        'social_k2_item_comment_color_scheme' => 'light',
        'social_k2_item_comment_order_by' => 'social',
        'social_k2_blog_comment_max_num' => '10',
        'social_k2_blog_comment_width' => '350',
        'social_k2_blog_comment_color_scheme' => 'light',
        'social_k2_blog_comment_order_by' => 'social',
        'social_like_article_include_ids' => '',
        'social_like_article_exclude_ids' => '',
        'social_like_cat_include_type' => '0',
        'social_like_cat_ids' => '',
        'social_like_sect_include_type' => '0',
        'social_like_sect_ids' => '',
        'social_like_article_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_like_frontpage_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_like_category_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_like_section_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_article_like_layout_style' => 'standard', //standard, box_count or button_count
        'social_article_like_show_faces' => '1', //1=Yes, 0=No
        'social_article_like_show_send_button' => '0', //0=No, 1=Yes
        'social_article_like_width' => '250',
        'social_article_like_verb_to_display' => 'like', //like or recommend
        'social_article_like_font' => 'arial', //arial, lucida grande, segoe ui, tahoma, trebuchet ms, verdana
        'social_article_like_color_scheme' => 'light', //light or dark
        'social_article_like_show_linkedin' => '0', //0=No, 1=Yes
        'social_article_like_show_twitter' => '0', //0=No, 1=Yes
        'social_article_like_show_googleplus' => '0', //0=No, 1=Yes
        'social_blog_like_layout_style' => 'standard', //standard, box_count or button_count
        'social_blog_like_show_faces' => '1', //1=Yes, 0=No
        'social_blog_like_show_send_button' => '0', //0=No, 1=Yes
        'social_blog_like_width' => '250',
        'social_blog_like_verb_to_display' => 'like', //like or recommend
        'social_blog_like_font' => 'arial', //arial, lucida grande, segoe ui, tahoma, trebuchet ms, verdana
        'social_blog_like_color_scheme' => 'light', //light or dark
        'social_blog_like_show_linkedin' => '0', //0=No, 1=Yes
        'social_blog_like_show_twitter' => '0', //0=No, 1=Yes
        'social_blog_like_show_googleplus' => '0', //0=No, 1=Yes
        'social_k2_like_item_include_ids' => '',
        'social_k2_like_item_exclude_ids' => '',
        'social_k2_like_cat_include_type' => '0',
        'social_k2_like_cat_ids' => '',
        'social_k2_like_item_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_k2_like_category_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_k2_like_tag_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_k2_like_userpage_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_k2_like_latest_view' => '0', //0=None, 1=Top, 2=Bottom, 3=Both
        'social_k2_item_like_layout_style' => 'standard', //standard, box_count or button_count
        'social_k2_item_like_show_faces' => '1', //1=Yes, 0=No
        'social_k2_item_like_show_send_button' => '0', //0=No, 1=Yes
        'social_k2_item_like_width' => '250',
        'social_k2_item_like_verb_to_display' => 'like', //like or recommend
        'social_k2_item_like_font' => 'arial', //arial, lucida grande, segoe ui, tahoma, trebuchet ms, verdana
        'social_k2_item_like_color_scheme' => 'light', //light or dark
        'social_k2_item_like_show_linkedin' => '0', //0=No, 1=Yes
        'social_k2_item_like_show_twitter' => '0', //0=No, 1=Yes
        'social_k2_item_like_show_googleplus' => '0', //0=No, 1=Yes
        'social_k2_blog_like_layout_style' => 'standard', //standard, box_count or button_count
        'social_k2_blog_like_show_faces' => '1', //1=Yes, 0=No
        'social_k2_blog_like_show_send_button' => '0', //0=No, 1=Yes
        'social_k2_blog_like_width' => '250',
        'social_k2_blog_like_verb_to_display' => 'like', //like or recommend
        'social_k2_blog_like_font' => 'arial', //arial, lucida grande, segoe ui, tahoma, trebuchet ms, verdana
        'social_k2_blog_like_color_scheme' => 'light', //light or dark
        'social_k2_blog_like_show_linkedin' => '0', //0=No, 1=Yes
        'social_k2_blog_like_show_twitter' => '0', //0=No, 1=Yes
        'social_k2_blog_like_show_googleplus' => '0', //0=No, 1=Yes
        'social_graph_fields' => '',
        'social_notification_comment_enabled' => '0',
        'social_notification_like_enabled' => '0',
        'social_notification_email_address' => '',
        'social_notification_google_analytics' => '0',
        'social_alphauserpoints_enabled' => '0',

        // Canvas Settings
        'canvas_tab_template' => '-1',
        'canvas_tab_reveal_article_id' => '',
        'canvas_canvas_template' => '-1',
        'canvas_tab_resize_enabled' => '0',
        'canvas_canvas_resize_enabled' => '0',

        // AutoTune
        'autotune_authorization' => '',
        'autotune_field_descriptors' => '',
        'autotune_app_config' => ''
    );

    function __construct()
    {
        $this->table = '#__jfbconnect_config';
        JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_jfbconnect/tables');

        parent::__construct();
    }

    static $instance;
    static function getSingleton()
    {
        if (!self::$instance)
            self::$instance = new JFBConnectModelConfig();
        return self::$instance;
    }

}
