<?php
/**
 * @package		JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

class JFBConnectControllerSocial extends JFBConnectController
{

    function __construct()
    {
        JRequest::setVar('view', 'Social');
        parent::__construct();
    }

    function display($cachable = false, $urlparams = false)
    {
        JRequest::setVar('layout', 'default');
        parent::display();
    }

    function apply()
    {
        $app = JFactory::getApplication();
        $configs = JRequest::get('POST', 4);
        $model = $this->getModel('config');
        $model->saveSettings($configs);
        $app->enqueueMessage(JText::_('COM_JFBCONNECT_MSG_SETTINGS_UPDATED'));
        $this->display();
    }

}