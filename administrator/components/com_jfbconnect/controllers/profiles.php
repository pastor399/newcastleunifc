<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Do this in case the system plugin is disabled
include_once (JPATH_SITE . '/components/com_jfbconnect/libraries/profile.php');

class JFBConnectControllerProfiles extends JFBConnectController
{

    function __construct()
    {
        JRequest::setVar('view', 'Profiles');
        parent::__construct();
    }

    function display($cachable = false, $urlparams = false)
    {
        JRequest::setVar('layout', 'default');
        parent::display();

    }

    function apply()
    {
        $app =JFactory::getApplication();
        $configs = JRequest::get('POST');
        $model = $this->getModel('config');

        JPluginHelper::importPlugin('socialprofiles');
        $profilePlugins = $app->triggerEvent('socialProfilesGetPlugins');

        foreach ($profilePlugins as $plugin)
        {
            $profileName = $plugin->getName();
            $settings = new JRegistry();
            $search = "profiles_".$profileName."_";
            foreach ($configs as $key => $value)
            {
                $pos = strpos($key, $search);
                if ($pos === 0)
                {
                    $key = str_replace($search, "", $key);
                    if (strpos($key, "field_map") === 0)
                    {
                        $key = str_replace("field_map", "", $key);
                        $settings->set('field_map.'.$key, $value);
                    }
                    else
                        $settings->set($key, $value);
                }
            }
            $model->update("profile_".$profileName, $settings->toString());
        }

        $app->enqueueMessage(JText::_('COM_JFBCONNECT_MSG_SETTINGS_UPDATED'));
        $this->display();
    }

}