<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

class com_JFBConnectInstallerScript
{
    var $jfbcVersion = "5.0.0"; // Same as the XML version
    var $finalDBVersion = "4.3.0"; // If database updates occur, increment this number
    var $libraryVersion = "2.0.0";
    var $packages = array();

    var $systemPluginEnabled = '0';
    var $authPluginEnabled = '0';
    var $userPluginEnabled = '0';
    var $contentPluginEnabled = '0';

    private $betaFixRequired = false; // Remove in v5.1

    public function __construct()
    {
         //SC16
        
            $jVersion = "j3.0";
         //SC30

        $this->packages[] = array('name' => 'JFBConnect Authentication Plugin', 'file' => 'plg_authentication_jfbconnectauth_' . $jVersion . '_v5.0.0.zip');
        $this->packages[] = array('name' => 'JFBConnect User Plugin', 'file' => 'plg_user_jfbconnectuser_' . $jVersion . '_v5.0.0.zip');
        $this->packages[] = array('name' => 'JFBConnect System Plugin', 'file' => 'plg_system_jfbcsystem_' . $jVersion . '_v5.0.0.zip');
        $this->packages[] = array('name' => 'JFBConnect Content Plugin', 'file' => 'plg_content_jfbccontent_' . $jVersion . '_v5.0.0.zip');
        $this->packages[] = array('name' => 'SCLogin / JFBConnect Login', 'file' => 'mod_sclogin_' . $jVersion . '_v2.0.0.zip');
        $this->packages[] = array('name' => 'SourceCoast Social Library', 'file' => 'lib_sourcecoast_' . $jVersion . '_v2.0.0.zip');
    }

    public function preflight($type, $parent)
    {
        // Check if Joomla version is correct. Mainly, J2.5 can't be installed on J3.0
        $jVersion = new JVersion();
        
        
            if (version_compare($jVersion->getShortVersion(), '3.0.0', '<'))
            {
                Jerror::raiseWarning(null, 'This installer is for Joomla 3.x. Please download the Joomla 2.5 installer from <a href="http://www.sourcecoast.com/" target="_BLANK">SourceCoast</a>.');
                return false;
            }
        

        // TODO: Remove this query in v5.1. Was required for a J3.0 issue in the 5.0 beta
        if ($type == 'update')
        {
            $db = JFactory::getDbo();
            $db->setQuery('SELECT manifest_cache FROM #__extensions WHERE name = "jfbconnect"');
            $manifest = json_decode($db->loadResult(), true);
            $oldRelease = $manifest['version'];
            if ($oldRelease == '5.0.0Beta')
                $this->betaFixRequired = true;
        }
        return true;
    }

    public function install($parent)
    {
        return true;
    }

    public function update($parent)
    {
        $this->savePluginState();
        $this->disablePlugins();

        return true;
    }

    public function postflight($type, $parent)
    {
        // Run required updates that may not have happened if user uninstalls and does a fresh install
        $this->updateDatabase();
        $this->installPackages();
        $this->migrateProfilePluginSettings();

        $this->showDoneMessage();
        $this->enablePlugins($this->systemPluginEnabled, $this->authPluginEnabled, $this->userPluginEnabled, $this->contentPluginEnabled);
    }

    /*
         * $parent is the class calling this method
         * uninstall runs before any other action is taken (file removal or database processing).
         */
    public function uninstall($parent)
    {
        $this->disablePlugins();
    }

    // CUSTOM JFBCONNECT FUNCTIONS
    private function savePluginState()
    {
        $db = JFactory::getDBO();
        $db->setQuery(
            "SELECT element, enabled FROM #__extensions " .
                    "WHERE (element = 'jfbconnectauth' AND folder = 'authentication' AND type = 'plugin') " .
                    "	OR (element = 'jfbcsystem' AND folder = 'system' AND type = 'plugin') " .
                    "	OR (element = 'jfbccontent' AND folder = 'content' AND type = 'plugin') " .
                    "	OR (element = 'jfbconnectuser' AND folder = 'user' AND type = 'plugin')");
        $pluginValues = $db->loadObjectList();

        if ($pluginValues)
        {
            foreach ($pluginValues as $plugin)
            {
                $pluginName = $plugin->element;
                $pluginPublished = $plugin->enabled;

                if ($pluginName == 'jfbconnectauth')
                    $this->authPluginEnabled = $pluginPublished;
                else if ($pluginName == 'jfbconnectuser')
                    $this->userPluginEnabled = $pluginPublished;
                else if ($pluginName == 'jfbcsystem')
                    $this->systemPluginEnabled = $pluginPublished;
                else if ($pluginName == 'jfbccontent')
                    $this->contentPluginEnabled = $pluginPublished;
            }
        }
    }

    private function disablePlugins()
    {
        $this->enablePlugins("0", "0", "0", "0");
    }

    private function enablePlugins($enableSystem, $enableAuth, $enableUser, $enableContent)
    {
        $db = JFactory::getDBO();
        $db->setQuery("UPDATE #__extensions SET enabled=" . $enableSystem . " WHERE element = 'jfbcsystem' AND folder = 'system' AND type = 'plugin';");
        $db->execute();
        $db->setQuery("UPDATE #__extensions SET enabled=" . $enableAuth . " WHERE element = 'jfbconnectauth' AND folder = 'authentication' AND type = 'plugin';");
        $db->execute();
        $db->setQuery("UPDATE #__extensions SET enabled=" . $enableUser . " WHERE element = 'jfbconnectuser' AND folder = 'user' AND type = 'plugin';");
        $db->execute();
        $db->setQuery("UPDATE #__extensions SET enabled=" . $enableContent . " WHERE element = 'jfbccontent' AND folder = 'content' AND type = 'plugin';");
        $db->execute();
    }

    private function updateDatabase()
    {
        // If user uninstalled JFBConnect and is re-installing, the tables will exist, but upgrade SQL files won't be called
        // Add SQL calls here as a backup
        $db = JFactory::getDBO();
        $db->setDebug(0);
        $db->setQuery("SELECT value FROM #__jfbconnect_config WHERE `setting` = 'db_version'");
        $dbVersion = $db->loadResult();
        switch ($dbVersion)
        {
            case null:
            case "":
                // TODO: Remove this query in v5.1. Was required for a J3.0 issue in the 5.0 beta
                if ($this->betaFixRequired)
                {
                    $db->setQuery("ALTER TABLE #__jfbconnect_user_map DROP COLUMN `access_token`");
                    $db->execute();
                }
                $this->runUpdateSQL('4.1.0');
            case "4.1.0":
                // TODO: Remove this query in v5.1. Was required for a J3.0 issue in the 5.0 beta
                if ($this->betaFixRequired)
                {
                    $db->setQuery("ALTER TABLE #__jfbconnect_user_map DROP COLUMN `authorized`");
                    $db->execute();
                }
                $this->runUpdateSQL('4.2.0');
            case "4.2.0":
                $this->runUpdateSQL('4.3.0');
                break;
            default:
                break;
        }
    }

    private function runUpdateSQL($version)
    {
        $buffer = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_jfbconnect/install/sql/updates/' . $version . '.sql');

        // Graceful exit and rollback if read not successful
        if ($buffer === false)
        {
            JError::raiseWarning(1, JText::_('JLIB_INSTALLER_ERROR_SQL_READBUFFER'));
            return false;
        }

        $db = JFactory::getDBO();

        // Create an array of queries from the sql file
        
            $queries = JDatabaseDriver::splitSql($buffer);
        
        

        $update_count = 0;
        if (count($queries) != 0)
        {
            // Process each query in the $queries array (split out of sql file).
            foreach ($queries as $query)
            {
                $query = trim($query);
                if ($query != '' && $query{0} != '#')
                {
                    $db->setQuery($query);

                    if (!$db->execute())
                    {
                        JLog::add(JText::sprintf('JLIB_INSTALLER_ERROR_SQL_ERROR', $db->stderr(true)), JLog::WARNING, 'jerror');
                        return false;
                    }
                    $update_count++;
                }
            }
        }
    }

    private function installPackages()
    {
        // Get current version number
        ?>

    <table>
        <tr>
            <td width="100px"><img
                    src="<?php print JURI::root(); ?>/administrator/components/com_jfbconnect/assets/images/jfbconn.png"
                    width="100px"></td>
            <td><h2>JFBConnect v<?php echo $this->jfbcVersion; ?></h2></td>
        </tr>
    </table>
    <h3>Installing...</h3>

    <?php
        jimport('joomla.installer.helper');
        jimport('joomla.installer.adapters.plugin');
        $installer = new JInstaller();
        $installer->setOverwrite(true);

        foreach ($this->packages as $package)
        {
            $pkgName = $package['name'];
            $pkgFile = $package['file'];
            $pkg = JInstallerHelper::unpack(JPATH_ADMINISTRATOR . '/components/com_jfbconnect/install/packages/' . $pkgFile);
            if ($installer->install($pkg['dir']))
            {
                ?>
            <table bgcolor="#E0FFE0" width="100%">
                <tr style="height:30px">
                    <td width="50px"><img
                            src="<?php print JURI::root(); ?>/administrator/components/com_jfbconnect/assets/images/icon-16-allow.png"
                            height="20px" width="20px"></td>
                    <td><font size="2"><b><?php echo $pkgName; ?> successfully installed.</b></font></td>
                </tr>
            </table>
            <?php
            } else
            {
                ?>
            <table bgcolor="#FFD0D0" width="100%">
                <tr style="height:30px">
                    <td width="50px"><img
                            src="<?php print JURI::root(); ?>/administrator/components/com_jfbconnect/assets/images/icon-16-deny.png"
                            height="20px" width="20px"></td>
                    <td><font size="2"><b>ERROR: Could not install the <?php echo $pkgName; ?>. Please install
                        manually.</b></font></td>
                </tr>
            </table>
            <?php
            }
        }
    }

    private function showDoneMessage()
    {
        ?>
    <p style="font-weight:bold; margin-top:20px">To configure and optimize JFBConnect, it's recommended to run Autotune whenever you install or upgrade:</p>
    <center><a href="index.php?option=com_jfbconnect&view=autotune"
               style="background-color:#025A8D;color:#FFFFFF;height:35px;padding:15px 45px;font-weight:bold;font-size:18px;line-height:60px;text-decoration:none;">Run
        Autotune Now</a></center>
    <?php
    }

    /*
     * JFBConnect v5.0 - Removed JFBCProfile plugins and moved to SocialProfiles plugins. Old plugins used a 1-row per setting in
     * #__jfbconnect_config. New plugins use a 1-row json variable for all settings that are loaded into a registry.
     * This function converts old settings to new format, if they exist.
     */
    private function migrateProfilePluginSettings()
    {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select("setting,value")->from("#__jfbconnect_config")->where('`setting` LIKE "profiles_%"')->order('setting');
        $rows = $db->setQuery($query)->loadObjectList();

        if (!empty($rows))
        {
            ?>
            <table width="100%">
                <tr style="height:30px; background-color:#ffff88;">
                    <td width="50px"><img
                            src="<?php print JURI::root(); ?>/administrator/components/com_jfbconnect/assets/images/icon-16-allow.png"
                            height="20px" width="20px"></td>
                    <td colspan="2"><font size="2"><b>JFBCProfile Plugins detected. Your settings are being migrated. Please install the new "Social
                        Profile" replacement plugins and verify settings.</b></font></td>
                </tr>
            <?php

            require_once(JPATH_ADMINISTRATOR . '/components/com_jfbconnect/models/config.php');
            $configModel = new JFBConnectModelConfig();
            $pluginName = "";
            $reg = new JRegistry();
            foreach ($rows as $row)
            {
                $values = explode("_", $row->setting);
                if ($pluginName != $values[1])
                {
                    $pluginName = $values[1];
                    // Remove the previous profiles_xyz settings from the config table
                    $query = $db->getQuery(true);
                    $query->delete('#__jfbconnect_config')->where('`setting` LIKE "profiles_' . $pluginName . '%"');
                    $db->setQuery($query)->execute();
                    ?>
                    <tr style="height:30px;background-color:#E0FFE0">
                        <td width="50px"></td>
                        <td width="50px"><img
                                src="<?php print JURI::root(); ?>/administrator/components/com_jfbconnect/assets/images/icon-16-allow.png"
                                height="20px" width="20px"></td>
                        <td><font size="2"><b><?php echo $pluginName; ?> settings migrated.</b></font></td>
                    </tr>
                <?php
                }
                unset($values[0]);
                unset($values[1]);
                $settingName = implode("_", $values);
                $value = $row->value;
                if ($settingName == "status_updates_pull_from_fb")
                    $settingName = "import_status";
                if ($settingName == "field_map")
                {
                    $fieldMap = unserialize($value);
                    $newMap = new stdClass();
                    foreach ($fieldMap as $key => $value)
                    {
                        if ($pluginName == "jomsocial")
                            $key = 'field' . $key;
                        $newMap->$key = $value;
                    }
                    $value = $newMap;
                }
                $reg->set($settingName, $value);
                $configModel->update('profile_' . $pluginName, $reg->toString());
            }
            echo "</table>";
        }
    }

}