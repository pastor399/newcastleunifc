<?php
/**
 * @package SourceCoast Extensions (JFBConnect, JLinked)
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('sourcecoast.utilities');

class SCEasyTags
{
    /*
     * Determines if the Easy-Tag can be rendered. If it can, then remove the render key
     */
    static function cannotRenderEasyTag(&$easyTag, $renderKey)
    {
        $key1 = 'key=' . $renderKey . ' ';
        $key2 = 'key=' . $renderKey;

        $renderKeyCheck = strtolower($easyTag);

        $containsKey = strpos($renderKeyCheck, 'key=') !== false;
        $missingKey1 = strpos($renderKeyCheck, $key1) === false;
        $missingKey2 = SCStringUtilities::endswith($renderKeyCheck, $key2) == false;

        $cannotRender = ($renderKey != '' && $missingKey1 && $missingKey2) ||
                ($renderKey == '' && $containsKey && $missingKey1 && $missingKey2);

        if (!$cannotRender && $renderKey != '')
        {
            $easyTag = str_replace($key1, '', $easyTag);
            $easyTag = str_replace($key2, '', $easyTag);
            $easyTag = SCStringUtilities::trimNBSP($easyTag);
        }

        return $cannotRender;
    }

    static function _splitIntoTagParameters($paramList)
    {
        $params = explode(' ', $paramList);

        $count = count($params);
        for ($i = 0; $i < $count; $i++)
        {
            $params[$i] = str_replace('"', '', $params[$i]);
            if (strpos($params[$i], '=') === false && $i > 0)
            {
                $previousIndex = SCEasyTags::_findPreviousParameter($params, $i - 1);
                //Combine this with previous entry and space
                $combinedParamValue = $params[$previousIndex] . ' ' . $params[$i];
                $params[$previousIndex] = $combinedParamValue;
                unset($params[$i]);
            }
        }
        return $params;
    }

    static function _findPreviousParameter($params, $i)
    {
        for ($index = $i; $index >= 0; $index--)
        {
            if (isset($params[$index]))
                return $index;
        }
        return 0;
    }

    static $jLinkedLoginCSSIncluded = false;

    static function getJLinkedLogin($paramList)
    {
        $buttonHtml = '';
        $showLogoutButton = '';
        $buttonSize = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);
                    $paramValues[1] = trim($paramValues[1], ' '); //trim email address was not working

                    switch ($paramValues[0])
                    {
                        case 'logout':
                            $showLogoutButton = $paramValues[1];
                            break;
                        case 'size':
                            $buttonSize = ' ' . $paramValues[1];
                            break;
                    }
                }
            }
        }

        $jLinkedLibrary = JLinkedApiLibrary::getInstance();
        $user = JFactory::getUser();
        if ($user->guest) // Only show login button if user isn't logged in (no remapping for now)
        {
            $lang = JFactory::getLanguage();
            $lang->load('com_jlinked');
            $loginText = JText::_('COM_JLINKED_LOGIN_USING_LINKEDIN');

            if (!self::$jLinkedLoginCSSIncluded)
            {
                self::$jLinkedLoginCSSIncluded = true;
                $buttonHtml .= '<link rel="stylesheet" href="' . JURI::base() . 'components/com_jlinked/assets/jlinked.css" type="text/css" />';
            }
            $buttonHtml .= '<div class="jLinkedLogin"><a href="javascript:void(0)" onclick="jlinked.login.login();"><span class="jlinkedButton' . $buttonSize . '"></span><span class="jlinkedLoginButton' . $buttonSize . '">' . $loginText . '</span></a></div>';
        } else
        {
            if ($showLogoutButton == '1' || $showLogoutButton == 'true')
            {
                $buttonHtml .= $jLinkedLibrary->getLogoutButton();
            }
        }
        return $buttonHtml;
    }

    static function getJLinkedApply($paramList)
    {
        $companyId = ''; //ID or Name required
        $companyName = ''; //ID or Name required
        $recipientEmail = ''; //required
        $jobTitle = ''; //required
        $jobLocation = '';
        $companyLogo = '';
        $themeColor = '';
        $requirePhoneNumber = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);
                    $paramValues[1] = trim($paramValues[1], ' '); //trim email address was not working

                    switch ($paramValues[0])
                    {
                        case 'companyid':
                            $companyId = $paramValues[1];
                            break;
                        case 'companyname':
                            $companyName = $paramValues[1];
                            break;
                        case 'email':
                            $recipientEmail = $paramValues[1];
                            break;
                        case 'jobtitle':
                            $jobTitle = $paramValues[1];
                            break;
                        case 'joblocation':
                            $jobLocation = $paramValues[1];
                            break;
                        case 'logo':
                            $companyLogo = $paramValues[1];
                            break;
                        case 'themecolor':
                            $themeColor = $paramValues[1];
                            break;
                        case 'phone':
                            $requirePhoneNumber = strtolower($paramValues[1]);
                            break;
                    }
                }
            }
        }

        $tagButtonText = '<div class="jlinkedApply">';
        $tagButtonText .= '<script type="IN/Apply"';

        if ($companyId)
            $tagButtonText .= ' data-companyid="' . $companyId . '"';
        if ($companyName)
            $tagButtonText .= ' data-companyname="' . $companyName . '"';
        if ($recipientEmail)
            $tagButtonText .= ' data-email="' . $recipientEmail . '"';
        if ($jobTitle)
            $tagButtonText .= ' data-jobtitle="' . $jobTitle . '"';
        if ($jobLocation)
            $tagButtonText .= ' data-joblocation="' . $jobLocation . '"';
        if ($companyLogo)
            $tagButtonText .= ' data-logo="' . $companyLogo . '"';
        if ($themeColor)
            $tagButtonText .= ' data-themecolor="' . $themeColor . '"';
        if ($requirePhoneNumber == 'true' || $requirePhoneNumber == '1')
            $tagButtonText .= ' data-phone="required"';
        $tagButtonText .= '></script></div>';

        return $tagButtonText;
    }

    static function getJLinkedShare($paramList)
    {
        $url = '';
        $countMode = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'href':
                        case 'url': //DEPRECATED 3/6/12
                            $url = $paramValues[1];
                            break;
                        case 'counter':
                            $countMode = $paramValues[1];
                            break;
                    }
                }
            }
        }

        if (!$url)
            $url = SCSocialUtilities::getStrippedUrl();

        $tagButtonText = '<div class="jlinkedShare">';
        $tagButtonText .= '<script type="IN/Share"';

        if ($url)
            $tagButtonText .= ' data-url="' . $url . '"';
        if ($countMode && ($countMode == 'top' || $countMode == 'right'))
            $tagButtonText .= ' data-counter="' . $countMode . '"';

        $tagButtonText .= '></script></div>';

        return $tagButtonText;
    }

    static function getJLinkedMember($paramList)
    {
        $url = '';
        $displayMode = '';
        $displayBehavior = '';
        $displayText = '';
        $showConnections = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'href':
                        case 'url': //DEPRECATED 3/6/12
                            $url = $paramValues[1];
                            break;
                        case 'display_mode':
                            $displayMode = $paramValues[1];
                            break;
                        case 'display_behavior':
                            $displayBehavior = $paramValues[1];
                            break;
                        case 'display_text':
                            $displayText = $paramValues[1];
                            break;
                        case 'related':
                            $showConnections = strtolower(($paramValues[1]));
                            break;
                    }
                }
            }
        }


        $tagButtonText = '<div class="jlinkedMember"><style type="text/css">.IN-canvas-member iframe{left:20px !important; top:135px !important;}</style>';
        $tagButtonText .= '<span class="IN-canvas-member"><script type="IN/MemberProfile"';

        if ($url)
            $tagButtonText .= ' data-id="' . $url . '"';
        if ($showConnections == 'false' || $showConnections == '0')
            $tagButtonText .= ' data-related="false"';

        if ($displayMode == 'inline')
            $tagButtonText .= ' data-format="inline"';
        else if ($displayMode == 'icon_name')
        {
            $tagButtonText .= ' data-format="' . $displayBehavior . '"';
            $tagButtonText .= ' data-text="' . $displayText . '"';
        } else if ($displayMode == 'icon')
        {
            $tagButtonText .= ' data-format="' . $displayBehavior . '"';
        }

        $tagButtonText .= '></script></span></div>';

        return $tagButtonText;
    }

    static function getJLinkedCompanyInsider($paramList)
    {
        $companyId = '';
        $showInNetwork = '';
        $showNewHires = '';
        $showPromotions = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'companyid':
                            $companyId = $paramValues[1];
                            break;
                        case 'in_network':
                            $showInNetwork = strtolower($paramValues[1]);
                            break;
                        case 'new_hires':
                            $showNewHires = strtolower($paramValues[1]);
                            break;
                        case 'promotions_changes':
                            $showPromotions = strtolower($paramValues[1]);
                            break;
                    }
                }
            }
        }

        $tagButtonText = '<div class="jlinkedCompanyInsider">';
        $tagButtonText .= '<script type="IN/CompanyInsider"';

        if ($companyId)
            $tagButtonText .= ' data-id="' . $companyId . '"';

        $modules = array();
        if ($showInNetwork == '1' || $showInNetwork == 'true')
            $modules[] = 'innetwork';
        if ($showNewHires == '1' || $showNewHires == 'true')
            $modules[] = 'newhires';
        if ($showPromotions == '1' || $showPromotions == 'true')
            $modules[] = 'jobchanges';

        if (count($modules) > 0)
            $tagButtonText .= ' data-modules="' . implode(',', $modules) . '"';

        $tagButtonText .= '></script></div>';

        return $tagButtonText;
    }

    static function getJLinkedCompanyProfile($paramList)
    {
        $companyId = '';
        $displayMode = '';
        $displayBehavior = '';
        $displayText = '';
        $showConnections = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'companyid':
                            $companyId = $paramValues[1];
                            break;
                        case 'display_mode':
                            $displayMode = $paramValues[1];
                            break;
                        case 'display_behavior':
                            $displayBehavior = $paramValues[1];
                            break;
                        case 'display_text':
                            $displayText = $paramValues[1];
                            break;
                        case 'related':
                            $showConnections = strtolower(($paramValues[1]));
                            break;
                    }
                }
            }
        }

        $tagButtonText = '<div class="jlinkedCompanyProfile"><style type="text/css">.IN-canvas-company iframe{left:20px !important; top:135px !important;}</style>';
        $tagButtonText .= '<span class="IN-canvas-company"><script type="IN/CompanyProfile"';

        if ($companyId)
            $tagButtonText .= ' data-id="' . $companyId . '"';
        if ($showConnections == 'false' || $showConnections == '0')
            $tagButtonText .= ' data-related="false"';

        if ($displayMode == 'inline')
            $tagButtonText .= ' data-format="inline"';
        else if ($displayMode == 'icon_name')
        {
            $tagButtonText .= ' data-format="' . $displayBehavior . '"';
            $tagButtonText .= ' data-text="' . $displayText . '"';
        } else if ($displayMode == 'icon')
        {
            $tagButtonText .= ' data-format="' . $displayBehavior . '"';
        }

        $tagButtonText .= '></script></span></div>';

        return $tagButtonText;
    }

    static function getJLinkedRecommend($paramList)
    {
        $companyId = '';
        $productId = '';
        $countMode = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'companyid':
                            $companyId = $paramValues[1];
                            break;
                        case 'productid':
                            $productId = $paramValues[1];
                            break;
                        case 'counter':
                            $countMode = $paramValues[1];
                            break;
                    }
                }
            }
        }

        $tagButtonText = '<div class="jlinkedRecommend">';
        $tagButtonText .= '<script type="IN/RecommendProduct"';

        if ($companyId)
            $tagButtonText .= ' data-company="' . $companyId . '"';
        if ($productId)
            $tagButtonText .= ' data-product="' . $productId . '"';
        if ($countMode && ($countMode == 'top' || $countMode == 'right'))
            $tagButtonText .= ' data-counter="' . $countMode . '"';

        $tagButtonText .= '></script></div>';

        return $tagButtonText;
    }

    static function getJLinkedJobs($paramList)
    {
        $companyId = ''; //optional

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);
                    $paramValues[1] = trim($paramValues[1], ' '); //trim email address was not working

                    switch ($paramValues[0])
                    {
                        case 'companyid':
                            $companyId = $paramValues[1];
                            break;
                    }
                }
            }
        }

        $tagButtonText = '<div class="jlinkedJobs">';
        $tagButtonText .= '<script type="IN/JYMBII"';

        if ($companyId)
            $tagButtonText .= ' data-companyid="' . $companyId . '"';
        $tagButtonText .= ' data-format="inline"';
        $tagButtonText .= '></script></div>';

        return $tagButtonText;
    }

    static function getJLinkedFollowCompany($paramList)
    {
        $companyId = '';
        $countMode = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'companyid':
                            $companyId = $paramValues[1];
                            break;
                        case 'counter':
                            $countMode = $paramValues[1];
                            break;
                    }
                }
            }
        }

        $tagButtonText = '<div class="jlinkedFollowCompany">';
        $tagButtonText .= '<script type="IN/FollowCompany"';

        if ($companyId)
            $tagButtonText .= ' data-id="' . $companyId . '"';
        if ($countMode && ($countMode == 'top' || $countMode == 'right' || $countMode == 'none'))
            $tagButtonText .= ' data-counter="' . $countMode . '"';
        else
            $tagButtonText .= ' data-counter="none"';
        $tagButtonText .= '></script></div>';

        return $tagButtonText;
    }

    static function getSCTwitterShare($paramList)
    {
        $url = '';
        $dataCount = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'href':
                        case 'url': //DEPRECATED 3/6/12
                            $url = $paramValues[1];
                            break;
                        case 'data_count':
                            $dataCount = $paramValues[1];
                            break;
                    }
                }
            }
        }

        if (!$url)
            $url = SCSocialUtilities::getStrippedUrl();

        $tagButtonText = '<div class="sc_twittershare">';
        $tagButtonText .= '<a href="http://twitter.com/share" class="twitter-share-button" ';

        if ($url)
            $tagButtonText .= 'data-url="' . $url . '"';
        if ($dataCount == 'horizontal' || $dataCount == 'vertical' || $dataCount == 'none')
            $tagButtonText .= ' data-count="' . $dataCount . '"';

        $tagButtonText .= '>Tweet</a></div>';

        return $tagButtonText;
    }

    static function getSCGPlusOne($paramList)
    {
        $url = '';
        $annotation = '';
        $size = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'href':
                        case 'url': //DEPRECATED 3/6/12
                            $url = $paramValues[1];
                            break;
                        case 'annotation':
                            $annotation = $paramValues[1];
                            break;
                        case 'size':
                            $size = $paramValues[1];
                            break;
                    }
                }
            }
        }

        if (!$url)
            $url = SCSocialUtilities::getStrippedUrl();

        $tagButtonText = '<div class="sc_gplusone"><g:plusone';
        if ($size)
            $tagButtonText .= ' size="' . $size . '"';
        if ($annotation)
            $tagButtonText .= ' annotation="' . $annotation . '"';
        if ($url)
            $tagButtonText .= ' href="' . $url . '"';
        $tagButtonText .= '></g:plusone></div>';

        return $tagButtonText;
    }

    static function getJFBCLike($paramList)
    {
        $url = '';
        $buttonStyle = '';
        $showFaces = '';
        $showSendButton = '';
        $width = '';
        $verbToDisplay = '';
        $font = '';
        $colorScheme = '';
        $ref = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'href':
                        case 'url': //DEPRECATED 3/6/12
                            $url = $paramValues[1];
                            break;
                        case 'layout':
                            $buttonStyle = $paramValues[1];
                            break;
                        case 'show_faces':
                            $showFaces = strtolower($paramValues[1]);
                            break;
                        case 'show_send_button':
                            $showSendButton = strtolower($paramValues[1]);
                            break;
                        case 'width':
                            $width = $paramValues[1];
                            break;
                        case 'action':
                            $verbToDisplay = $paramValues[1];
                            break;
                        case 'font':
                            $font = $paramValues[1];
                            break;
                        case 'colorscheme':
                            $colorScheme = $paramValues[1];
                            break;
                        case 'ref':
                            $ref = $paramValues[1];
                            break;
                    }
                }
            }
        }

        if (!$url)
            $url = SCSocialUtilities::getStrippedUrl();

        $likeButtonText = '<div class="jfbclike"><div class="fb-like" data-href="' . $url . '"';
        if ($showFaces == "false" || $showFaces == "0")
            $likeButtonText .= ' data-show-faces="false"';
        else
            $likeButtonText .= ' data-show-faces="true"';

        if ($showSendButton == "false" || $showSendButton == "0")
            $likeButtonText .= ' data-send="false"';
        else
            $likeButtonText .= ' data-send="true"';

        if ($buttonStyle)
            $likeButtonText .= ' data-layout="' . $buttonStyle . '"';
        if ($width)
            $likeButtonText .= ' data-width="' . $width . '"';
        if ($verbToDisplay)
            $likeButtonText .= ' data-action="' . $verbToDisplay . '"';
        if ($font)
            $likeButtonText .= ' data-font="' . $font . '"';
        if ($colorScheme)
            $likeButtonText .= ' data-colorscheme="' . $colorScheme . '"';
        if ($ref)
            $likeButtonText .= ' data-ref="' . $ref .'"';
        $likeButtonText .= '></div></div>';
        return $likeButtonText;
    }

    static function getJFBCLogin($paramList)
    {
        $buttonSize = 'medium';
        $showFaces = '';
        $showLogoutButton = '';
        $maxRows = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'size':
                            $buttonSize = $paramValues[1];
                            break;
                        case 'logout':
                            $showLogoutButton = $paramValues[1];
                            break;
                        case 'show_faces':
                            $showFaces = $paramValues[1];
                            break;
                        case 'max_rows':
                            $maxRows = $paramValues[1];
                            break;
                    }
                }
            }
        }

        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $user = JFactory::getUser();
        if ($user->guest) // Only show login button if user isn't logged in (no remapping for now)
            $fbLogin = $jfbcLibrary->getLoginButton($buttonSize, $showFaces, $maxRows);
        else
        {
            $fbLogin = ""; // return blank for registered users

            if ($showLogoutButton == '1' || $showLogoutButton == 'true')
                $fbLogin = $jfbcLibrary->getLogoutButton();
        }

        return '<div class="jfbclogin">' . $fbLogin . '</div>';
    }

    static function getJFBCSend($paramList)
    {
        $url = '';
        $font = '';
        $colorScheme = '';
        $ref = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'href':
                        case 'url': //DEPRECATED 3/6/12
                            $url = $paramValues[1];
                            break;
                        case 'font':
                            $font = $paramValues[1];
                            break;
                        case 'colorscheme':
                            $colorScheme = $paramValues[1];
                            break;
                        case 'ref' :
                            $ref = $paramValues[1];
                            break;
                    }
                }
            }
        }

        if (!$url)
            $url = SCSocialUtilities::getStrippedUrl();

        $sendButtonText = '<div class="jfbcsend"><div class="fb-send" data-href="' . $url . '"';

        if ($font)
            $sendButtonText .= ' data-font="' . $font . '"';
        if ($colorScheme)
            $sendButtonText .= ' data-colorscheme="' . $colorScheme . '"';
        if ($ref)
            $sendButtonText .= ' data-ref="' . $ref . '"';
        $sendButtonText .= '></div></div>';
        return $sendButtonText;
    }

    static function getJFBCComments($paramList)
    {
        $href = '';
        $width = '';
        $numComments = '';
        $colorscheme = '';
        $mobile = '';
        $orderBy = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'href':
                            $href = $paramValues[1];
                            break;
                        case 'width':
                            $width = $paramValues[1];
                            break;
                        case 'num_posts':
                            $numComments = $paramValues[1];
                            break;
                        case 'colorscheme':
                            $colorscheme = $paramValues[1];
                            break;
                        case 'mobile':
                            $mobile = $paramValues[1];
                            break;
                        case 'order_by':
                            $orderBy = $paramValues[1];
                            break;
                    }
                }
            }
        }

        $commentString = '<div class="jfbccomments"><div class="fb-comments"';

        if ($href)
            $commentString .= ' data-href="' . $href . '"';
        else
        {
            $url = SCSocialUtilities::getStrippedUrl();
            $commentString .= ' data-href="' . $url . '"';
        }

        if ($width)
            $commentString .= ' data-width="' . $width . '"';
        if ($numComments || $numComments == "0")
            $commentString .= ' data-num-posts="' . $numComments . '"';
        if ($colorscheme)
            $commentString .= ' data-colorscheme="' . $colorscheme . '"';
        if ($mobile == "false" || $mobile == "0")
            $commentString .= ' data-mobile="false"';
        if ($orderBy == 'social' || $orderBy == 'reverse_time' || $orderBy == 'time')
            $commentString .= ' data-order-by="' . $orderBy . '"';

        $commentString .= '></div></div>';
        return $commentString;
    }

    static function getJFBCCommentsCount($paramList)
    {
        $href = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'href':
                            $href = $paramValues[1];
                            break;
                    }
                }
            }
        }

        //Get the Comments Count string
        $tagString = '<div class="fb-comments-count"';
        if ($href)
            $tagString .= ' data-href="' . $href . '"';
        else
        {
            $url = SCSocialUtilities::getStrippedUrl();
            $tagString .= ' data-href="' . $url . '"';
        }
        $tagString .= '></div>';

        $lang = JFactory::getLanguage();
        $lang->load('com_jfbconnect');

        $commentString = '<div class="jfbccomments_count">';
        $commentString .= JText::sprintf('COM_JFBCONNECT_COMMENTS_COUNT', $tagString);
        $commentString .= '</div>';
        return $commentString;
    }

    static function getJFBCFan($paramList)
    {
        $height = '';
        $width = '';
        $colorScheme = '';
        $href = '';
        $showFaces = '';
        $stream = '';
        $header = '';
        $borderColor = '';
        $forceWall = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'height': //Not shown - http://developers.facebook.com/docs/reference/plugins/like-box/
                            $height = $paramValues[1];
                            break;
                        case 'width':
                            $width = $paramValues[1];
                            break;
                        case 'colorscheme':
                            $colorScheme = $paramValues[1];
                            break;
                        case 'href':
                            $href = $paramValues[1];
                            break;
                        case 'show_faces':
                            $showFaces = $paramValues[1];
                            break;
                        case 'stream':
                            $stream = $paramValues[1];
                            break;
                        case 'header':
                            $header = $paramValues[1];
                            break;
                        case 'border_color':
                            $borderColor = $paramValues[1];
                            break;
                        case 'force_wall':
                            $forceWall = $paramValues[1];
                            break;
                    }
                }
            }
        }

        $fanString = '<div class="jfbcfan"><div class="fb-like-box"';

        if ($showFaces == "false" || $showFaces == "0")
            $fanString .= ' data-show-faces="false"';
        else
            $fanString .= ' data-show-faces="true"';

        if ($header == "false" || $header == "0")
            $fanString .= ' data-header="false"';
        else
            $fanString .= ' data-header="true"';

        if ($stream == "false" || $stream == "0")
            $fanString .= ' data-stream="false"';
        else
            $fanString .= ' data-stream="true"';

        if ($forceWall == "false" || $forceWall == "0")
            $fanString .= ' data-force-wall="false"';
        else
            $fanString .= ' data-force-wall="true"';

        if ($width)
            $fanString .= ' data-width="' . $width . '"';
        if ($height)
            $fanString .= ' data-height="' . $height . '"';
        if ($href)
            $fanString .= ' data-href="' . $href . '"';
        if ($colorScheme)
            $fanString .= ' data-colorscheme="' . $colorScheme . '"';
        if ($borderColor)
            $fanString .= ' data-border-color="' . $borderColor . '"';

        $fanString .= '></div></div>';
        return $fanString;
    }

    static function getJFBCFeed($paramList)
    {
        $site = '';
        $height = '';
        $width = '';
        $colorScheme = '';
        $font = '';
        $recommendations = '';
        $header = '';
        $linkTarget = '';
        $filter = '';
        $action = '';
        $ref = '';
        $max_age = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'site':
                            $site = $paramValues[1];
                            break;
                        case 'height':
                            $height = $paramValues[1];
                            break;
                        case 'width':
                            $width = $paramValues[1];
                            break;
                        case 'colorscheme':
                            $colorScheme = $paramValues[1];
                            break;
                        case 'font':
                            $font = $paramValues[1];
                            break;
                        case 'recommendations':
                            $recommendations = $paramValues[1];
                            break;
                        case 'header':
                            $header = $paramValues[1];
                            break;
                        case 'link_target':
                            $linkTarget = $paramValues[1];
                            break;
                        case 'action':
                            $action = $paramValues[1];
                            break;
                        case 'filter':
                            $filter = $paramValues[1];
                            break;
                        case 'ref':
                            $ref = $paramValues[1];
                            break;
                        case 'max_age':
                            $max_age = $paramValues[1];
                            break;
                    }
                }
            }
        }

        $feedString = '<div class="jfbcfeed"><div class="fb-activity"';

        if ($recommendations == "false" || $recommendations == "0")
            $feedString .= ' data-recommendations="false"';
        else
            $feedString .= ' data-recommendations="true"';

        if ($header == "false" || $header == "0")
            $feedString .= ' data-header="false"';
        else
            $feedString .= ' data-header="true"';

        if ($width)
            $feedString .= ' data-width="' . $width . '"';
        if ($height)
            $feedString .= ' data-height="' . $height . '"';
        if ($site)
            $feedString .= ' data-site="' . $site . '"';
        if ($colorScheme)
            $feedString .= ' data-colorscheme="' . $colorScheme . '"';
        if ($font)
            $feedString .= ' data-font="' . $font . '"';
        if ($linkTarget)
            $feedString .= ' data-linktarget="' . $linkTarget . '"';
        if ($action)
            $feedString .= ' data-action="' . $action . '"';
        if ($filter)
            $feedString .= ' data-filter="' . $filter . '"';
        if ($ref)
            $feedString .= ' data-ref="' . $ref . '"';
        if ($max_age)
            $feedString .= ' data-max-age="' . $max_age . '"';

        $feedString .= '></div></div>';
        return $feedString;
    }

    static function getJFBCFriends($paramList)
    {
        $href = '';
        $width = '';
        $maxRows = '';
        $colorScheme = '';
        $size = '';
        $action = '';
        $showCount = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'href':
                            $href = $paramValues[1];
                            break;
                        case 'width':
                            $width = $paramValues[1];
                            break;
                        case 'max_rows':
                            $maxRows = $paramValues[1];
                            break;
                        case 'colorscheme':
                            $colorScheme = $paramValues[1];
                            break;
                        case 'size':
                            $size = $paramValues[1];
                            break;
                        case 'action':
                            $action =  $paramValues[1];
                            break;
                        case 'show_count':
                            $showCount = $paramValues[1];
                            break;
                    }
                }
            }
        }

        $friendsString = '<div class="jfbcfriends"><div class="fb-facepile"';

        if ($href)
            $friendsString .= ' data-href="' . $href . '"';
        if ($width)
            $friendsString .= ' data-width="' . $width . '"';
        if ($maxRows)
            $friendsString .= ' data-max-rows="' . $maxRows . '"';
        if ($colorScheme)
            $friendsString .= ' data-colorscheme="' . $colorScheme . '"';
        if ($size)
            $friendsString .= ' data-size="' . $size . '"';
        if ($showCount == '0' || $showCount == 'false')
            $friendsString .= ' data-show-count="false"';
        else
            $friendsString .= ' data-show-count="true"';
        if ($action)
            $friendsString .= ' data-action="' . $action .'"';

        $friendsString .= '></div></div>';
        return $friendsString;
    }

    static function getJFBCRecommendationsBar($paramList)
    {
        $href = '';
        $trigger = '';
        $readTime = '';
        $action = '';
        $side = '';
        $site = '';
        $ref = '';
        $numRecommendations = '';
        $maxAge = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'href':
                            $href = $paramValues[1];
                            break;
                        case 'trigger':
                            $trigger = $paramValues[1];
                            break;
                        case 'read_time':
                            $readTime = $paramValues[1];
                            break;
                        case 'action' :
                            $action = $paramValues[1];
                            break;
                        case 'side' :
                            $side = $paramValues[1];
                            break;
                        case 'site':
                            $site = $paramValues[1];
                            break;
                        case 'ref':
                            $ref = $paramValues[1];
                            break;
                        case 'num_recommendations':
                            $numRecommendations = $paramValues[1];
                            break;
                        case 'max_age':
                            $maxAge = $paramValues[1];
                            break;
                    }
                }
            }
        }

        if (!$href)
            $href = SCSocialUtilities::getStrippedUrl();

        $tagString = '<div class="jfbcrecommendationsbar"><div class="fb-recommendations-bar"';
        $tagString .= ' data-href="' . $href . '"';

        if ($trigger)
            $tagString .= ' data-trigger="' . $trigger . '"';
        if ($readTime)
            $tagString .= ' data-read-time="' . $readTime . '"';
        if ($action)
            $tagString .= ' data-action="' . $action . '"';
        if ($side)
            $tagString .= ' data-side="' . $side . '"';
        if ($site)
            $tagString .= ' data-site="' . $site . '"';
        if ($ref)
            $tagString .= ' data-ref="' . $ref . '"';
        if ($numRecommendations)
            $tagString .= ' data-num-recommendations="' . $numRecommendations . '"';
        if ($maxAge)
            $tagString .= ' data-max-age="' . $maxAge . '"';

        $tagString .= '></div></div>';
        return $tagString;
    }

    static function getJFBCRecommendations($paramList)
    {
        $site = '';
        $width = '';
        $height = '';
        $header = '';
        $colorScheme = '';
        $font = '';
        $linkTarget = '';
        $action = '';
        $ref = '';
        $max_age = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'site':
                            $site = $paramValues[1];
                            break;
                        case 'width':
                            $width = $paramValues[1];
                            break;
                        case 'height':
                            $height = $paramValues[1];
                            break;
                        case 'colorscheme' :
                            $colorScheme = $paramValues[1];
                            break;
                        case 'header' :
                            $header = $paramValues[1];
                            break;
                        case 'font':
                            $font = $paramValues[1];
                            break;
                        case 'link_target':
                            $linkTarget = $paramValues[1];
                            break;
                        case 'action':
                            $action = $paramValues[1];
                            break;
                        case 'ref':
                            $ref = $paramValues[1];
                            break;
                        case 'max_age':
                            $max_age = $paramValues[1];
                            break;
                    }
                }
            }
        }

        $recString = '<div class="jfbcrecommendations"><div class="fb-recommendations"';

        if ($header == "false" || $header == "0")
            $recString .= ' data-header="false"';
        else
            $recString .= ' data-header="true"';

        if ($site)
            $recString .= ' data-site="' . $site . '"';
        if ($width)
            $recString .= ' data-width="' . $width . '"';
        if ($height)
            $recString .= ' data-height="' . $height . '"';
        if ($colorScheme)
            $recString .= ' data-colorscheme="' . $colorScheme . '"';
        if ($font)
            $recString .= ' data-font="' . $font . '"';
        if ($linkTarget)
            $recString .= ' data-linktarget="' . $linkTarget . '"';
        if ($action)
            $recString .= ' data-action="' . $action . '"';
        if ($ref)
            $recString .= ' data-ref="' . $ref . '"';
        if ($max_age)
            $recString .= ' data-max-age="' . $max_age . '"';
        $recString .= '></div></div>';
        return $recString;
    }

    static function getJFBCRequest($paramList)
    {
        $requestID = '';
        $linkText = '';
        $linkImage = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'request_id':
                            $requestID = $paramValues[1];
                            break;
                        case 'link_text':
                            $linkText = $paramValues[1];
                            break;
                        case 'link_image':
                            $linkImage = $paramValues[1];
                            break;
                    }
                }
            }
        }
        $tagString = '';
        if ($requestID != '')
        {
            JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_jfbconnect/models');
            $requestModel = JModelLegacy::getInstance('Request', "JFBConnectModel");
            $request = $requestModel->getData($requestID);

            if ($request && $request->published)
            {
                $message = str_replace("\r\n", " ", $request->message);
                $linkValue = $linkText;
                if ($linkImage != '')
                    $linkValue = '<img src="' . $linkImage . '" alt="' . $request->title . ' "/>';

                $tagString = '<div class="jfbcrequest">';
                $tagString .= '<a href="javascript:void(0)" onclick="jfbc.request.popup(' . $requestID . '); return false;">' . $linkValue . '</a>';
                $tagString .= '</div>';
                $tagString .=
                        <<<EOT
                        <script type="text/javascript">
    var jfbcRequests = Object.prototype.toString.call(jfbcRequests) == "[object Array]" ? jfbcRequests : [];
    var jfbcRequest = new Object;
    jfbcRequest.title = "{$request->title}";
    jfbcRequest.message = "{$message}";
    jfbcRequest.destinationUrl = "{$request->destination_url}";
    jfbcRequest.thanksUrl = "{$request->thanks_url}";
    jfbcRequests[{$requestID}] = jfbcRequest;
</script>
EOT;
            }
        }
        return $tagString;
    }

    static function getJFBCSubscribe($paramList)
    {
        $href = '';
        $layout = '';
        $showFaces = '';
        $colorScheme = '';
        $font = '';
        $width = '';

        $params = SCEasyTags::_splitIntoTagParameters($paramList);
        foreach ($params as $param)
        {
            if ($param != null)
            {
                $paramValues = explode('=', $param, 2);
                if (count($paramValues) == 2) //[0] name [1] value
                {
                    $paramValues[0] = strtolower(trim($paramValues[0]));
                    $paramValues[1] = trim($paramValues[1]);

                    switch ($paramValues[0])
                    {
                        case 'href':
                            $href = $paramValues[1];
                            break;
                        case 'layout':
                            $layout = $paramValues[1];
                            break;
                        case 'show_faces':
                            $showFaces = strtolower($paramValues[1]);
                            break;
                        case 'width':
                            $width = $paramValues[1];
                            break;
                        case 'font':
                            $font = $paramValues[1];
                            break;
                        case 'colorscheme':
                            $colorScheme = $paramValues[1];
                            break;
                    }
                }
            }
        }

        $tagText = '<div class="jfbcfollow"><div class="fb-follow" data-href="' . $href . '"';
        if ($showFaces == "false" || $showFaces == "0")
            $tagText .= ' data-show-faces="false"';
        else
            $tagText .= ' data-show-faces="true"';

        if ($layout)
            $tagText .= ' data-layout="' . $layout . '"';
        if ($width)
            $tagText .= ' data-width="' . $width . '"';
        if ($font)
            $tagText .= ' data-font="' . $font . '"';
        if ($colorScheme)
            $tagText .= ' data-colorscheme="' . $colorScheme . '"';
        $tagText .= '></div></div>';
        return $tagText;
    }

    static function extendJoomlaUserForms($loginTag)
    {
        $option = JRequest::getCmd('option');
        $view = JRequest::getCmd('view');
        $user = JFactory::getUser();

        if (($option == 'com_user' && $view == 'login') || ($option == 'com_users' && $view == 'login'))
        {
            if ($user->guest)
            {
                $document = JFactory::getDocument();
                $output = $document->getBuffer('component');
                $contents = $output . $loginTag;
                $document->setBuffer($contents, 'component');
            }
        }
    }
}