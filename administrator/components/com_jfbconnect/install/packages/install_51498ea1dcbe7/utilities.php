<?php
/**
 * @package SourceCoast Extensions (JFBConnect, JLinked)
 * @copyright (C) 2011-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.file');
jimport('joomla.user.helper');

define('CHECK_NEW_MAPPING_JLINKED', 'jLinkedCheckNewMapping');
define('CHECK_NEW_MAPPING_JFBCONNECT', 'jfbcCheckNewMapping');
define('LOGIN_TASK_JLINKED', 'loginLinkedInUser');
define('LOGIN_TASK_JFBCONNECT', 'loginFacebookUser');

define('AUTONAME_EXT', '0');
define('AUTONAME_FIRSTLAST', '1');
define('AUTONAME_FIRLAS', '2');
define('AUTONAME_EMAIL', '3');

class SCSocialUtilities
{
    static function isJFBConnectInstalled()
    {
        return SCSocialUtilities::isComponentEnabled('com_jfbconnect', JPATH_ROOT.'/components/com_jfbconnect/libraries/facebook.php');
    }

    static function isJLinkedInstalled()
    {
        return SCSocialUtilities::isComponentEnabled('com_jlinked', JPATH_ROOT.'/components/com_jlinked/libraries/linkedin.php');
    }

    static function isComponentEnabled($option, $libraryFile)
    {
        $isComponentEnabled = false;
        if (JFile::exists($libraryFile))
        {
            $isComponentEnabled = SCSocialUtilities::isJoomlaComponentEnabled($option);
        }
        return $isComponentEnabled;
    }

    static function isJoomlaComponentEnabled($option)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('extension_id AS id, element AS "option", params, enabled');
        $query->from('#__extensions');
        $query->where($query->qn('type') . ' = ' . $db->quote('component'));
        $query->where($query->qn('element') . ' = ' . $db->quote($option));
        $db->setQuery($query);

        $component = $db->loadObject();
        $isComponentEnabled = $component && $component->enabled;
        return $isComponentEnabled;
    }

    static function areJFBConnectTagsEnabled()
    {
        return JPluginHelper::isEnabled('system', 'jfbcsystem');
    }

    static function areJLinkedTagsEnabled()
    {
        return JPluginHelper::isEnabled('system', 'jlinkedsystem');
    }

    static function getJFBConnectAppId()
    {
        $libFile = JPATH_ROOT . '/components/com_jfbconnect/libraries/facebook.php';
        if (!JFile::exists($libFile))
            return '';

        require_once($libFile);
        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $appId = $jfbcLibrary->facebookAppId;
        return $appId;
    }

    static function getJFBConnectRenderKeySetting()
    {
        $libFile = JPATH_ROOT . '/components/com_jfbconnect/libraries/facebook.php';
        if (!JFile::exists($libFile))
            return '';

        require_once($libFile);
        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $renderKey = $jfbcLibrary->getSocialTagRenderKey();
        return $renderKey;
    }

    static function getJLinkedRenderKeySetting()
    {
        $libFile = JPATH_ROOT.'/components/com_jlinked/libraries/linkedin.php';
        if (!JFile::exists($libFile))
            return '';

        require_once($libFile);
        $jLinkedLibrary = JLinkedApiLibrary::getInstance();
        $renderKey = $jLinkedLibrary->getSocialTagRenderKey();
        return $renderKey;
    }

    static function getJFBConnectRenderKey()
    {
        $renderKey = SCSocialUtilities::getJFBConnectRenderKeySetting();
        if($renderKey != '')
            $renderKeyString = " key=" . $renderKey;
        else
            $renderKeyString = '';

        return $renderKeyString;
    }

    static function getJLinkedRenderKey()
    {
        $renderKey = SCSocialUtilities::getJLinkedRenderKeySetting();
        if($renderKey != '')
            $renderKeyString = " key=" . $renderKey;
        else
            $renderKeyString = '';

        return $renderKeyString;
    }

    static function getExtraShareButtons($url, $dataCount, $showFacebookLikeButton, $showFacebookSendButton, $showTwitterButton, $showGooglePlusButton, $renderKeyString, $showLinkedInButton = false, $fbWidth = 50)
    {
        if ($dataCount == "top" || $dataCount == "box_count")
        {
            $fbButtonStyle = "box_count";
            $fbButtonAttributes = '';
            $li_dataCount = "top";
            $gp_size = 'tall';
            $gp_annotation = 'bubble';
            $tw_size = 'vertical';
        }
        else if ($dataCount == 'right' || $dataCount == "button_count")
        {
            $fbButtonStyle = "button_count";
            $fbButtonAttributes = '';
            $li_dataCount = "right";
            $gp_size = 'medium';
            $gp_annotation = 'bubble';
            $tw_size = 'horizontal';
        }
        else
        {
            $fbButtonStyle = 'standard';
            $fbButtonAttributes = ' width=' . $fbWidth . ' show_faces=false';
            $li_dataCount = 'no_count';
            $gp_size = 'standard';
            $gp_annotation = 'none';
            $tw_size = 'none';
        }

        $extraButtonText = '';

        if($url == '')
            $url = SCSocialUtilities::getStrippedUrl();

        if($showLinkedInButton)
        {
            if(SCSocialUtilities::isJLinkedInstalled() && SCSocialUtilities::areJLinkedTagsEnabled())
            {
                $renderString = SCSocialUtilities::getJLinkedRenderKey();
                $extraButtonText .= '{JLinkedShare href='. $url . ' counter=' . $li_dataCount . $renderString . '}';
            }
            else
                $extraButtonText .= '{JLinkedShare href='. $url . ' counter=' . $li_dataCount . $renderKeyString . '}';
        }
        if($showTwitterButton)
        {
            $extraButtonText .= '{SCTwitterShare href='. $url . ' data_count=' . $tw_size . $renderKeyString .'}';
        }
        if($showGooglePlusButton)
        {
            $extraButtonText .= '{SCGooglePlusOne href=' . $url . ' annotation=' . $gp_annotation . ' size=' . $gp_size . $renderKeyString . '}';
        }
        if($showFacebookLikeButton)
        {
            $sendString = $showFacebookSendButton ? "true" : "false";

            if(SCSocialUtilities::isJFBConnectInstalled() && SCSocialUtilities::areJFBConnectTagsEnabled())
            {
                $renderString = SCSocialUtilities::getJFBConnectRenderKey();
                $extraButtonText .= '{JFBCLike href='. $url . ' layout=' . $fbButtonStyle . ' show_send_button=' . $sendString . $fbButtonAttributes . $renderString . '}';
            }
            else
                $extraButtonText .= '{JFBCLike href='. $url . ' layout=' . $fbButtonStyle . ' show_send_button=' . $sendString . $fbButtonAttributes . $renderKeyString . '}';
        }

        return $extraButtonText;
    }

    static function getStrippedUrl()
    {
        $href = JURI::current();

        $juri = JURI::getInstance();
        // Delete some common, unwanted query params to at least try to get at the canonical URL
        $juri->delVar('fb_comment_id');
        $juri->delVar('tp');
        $juri->delVar('notif_t');
        $juri->delVar('ref');
        $query = $juri->getQuery();

        if ($query)
            $href .= '?' . $query;

        return $href;
    }

    static function stripSystemTags(&$description, $metadataTag)
    {
        $replace = false;

        //Full Match
        if (preg_match_all('/\{' . $metadataTag . '.*?\}/i', $description, $matches, PREG_SET_ORDER))
        {
            $replace = true;
            foreach ($matches as $match)
            {
                $description = str_replace($match, '', $description);
            }
        }
        //Partial Match
        if (preg_match('/\{' . $metadataTag . '+(.*?)/i', $description, $matches))
        {
            $replace = true;
            $trimPoint = strpos($description, '{' . $metadataTag);
            if ($trimPoint == 0)
                $description = '';
            else
                $description = substr($description, 0, $trimPoint);
        }

        return $replace;
    }

    static function setJFBCNewMappingEnabled()
    {
        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        SCSocialUtilities::setNewMappingEnabled($jfbcLibrary, CHECK_NEW_MAPPING_JFBCONNECT);
    }

    static function setJLinkedNewMappingEnabled()
    {
        $jLinkedLibrary = JLinkedApiLibrary::getInstance();
        SCSocialUtilities::setNewMappingEnabled($jLinkedLibrary, CHECK_NEW_MAPPING_JLINKED);
    }

    static function setNewMappingEnabled($socialLibrary = null, $checkNewMappingSetting = CHECK_NEW_MAPPING_JLINKED)
    {
        $session = JFactory::getSession();
        $session->set($checkNewMappingSetting, true);

        if($socialLibrary == null) //Backwards compatibility with JLinked 1.1
            $socialLibrary = JLinkedApiLibrary::getInstance();

        $socialLibrary->checkNewMapping = true;
    }

    static function clearJFBCNewMappingEnabled()
    {
        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        SCSocialUtilities::clearNewMappingEnabled($jfbcLibrary, CHECK_NEW_MAPPING_JFBCONNECT);
    }

    static function clearJLinkedNewMappingEnabled()
    {
        $jLinkedLibrary = JLinkedApiLibrary::getInstance();
        SCSocialUtilities::clearNewMappingEnabled($jLinkedLibrary, CHECK_NEW_MAPPING_JLINKED);
    }

    static function clearNewMappingEnabled($socialLibrary = null, $checkNewMappingSetting = CHECK_NEW_MAPPING_JLINKED)
    {
        $session = JFactory::getSession();
        $session->clear($checkNewMappingSetting);

        if($socialLibrary == null) //Backwards compatibility with JLinked 1.1
            $socialLibrary = JLinkedApiLibrary::getInstance();

        $socialLibrary->checkNewMapping = false;
    }

    static function getCurrentReturnParameter(&$return, &$menuItemId, $loginTaskSetting = LOGIN_TASK_JLINKED)
    {
        // setup return url in case they should be redirected back to this page
        $uri = JURI::getInstance();

        // Save the current page to the session, allowing us to redirect to it on login or logout if configured that way
        $isLoginRegister = JRequest::getCmd('view') == "loginregister";
        $isLoginReturning = JRequest::getCmd('task') == $loginTaskSetting;
        $isLogout = JRequest::getCmd('task') == "logout";

        //NOTE: Not checking option=com_blah because of system cache plugin
        if(!$isLoginRegister && !$isLoginReturning && !$isLogout)
        {
            $return = $uri->toString(array('path', 'query'));
            if ($return == "")
                $return = 'index.php';
        }

        //Save the current return parameter
        $returnParam = JRequest::getVar('return', '');
        if ($returnParam != "")
        {
            $return = urlencode($returnParam); // Required for certain SEF extensions
            $return = rawurldecode($return);
            $return = base64_decode($return);

            $returnURI = JURI::getInstance($return);
            $menuItemId = $returnURI->getVar('Itemid','');

            $filterInput = JFilterInput::getInstance();
            $menuItemId = $filterInput->clean($menuItemId, 'INT');
            //$menuItemId = JFilterInput::clean($menuItemId, 'INT');

        }
        else
            $menuItemId = JRequest::getInt('Itemid', 0);
    }

    static function getRandomPassword(&$newPassword)
    {
        $newPassword = JUserHelper::genRandomPassword();
        $salt = JUserHelper::genRandomPassword(32);
        $crypt = JUserHelper::getCryptedPassword($newPassword, $salt);
        return $crypt . ':' . $salt;
    }

    static function getRemoteContent($url)
    {
        // Parts of this function inspired by JomSocial's implementation (c) Slashes 'n Dots azrul.com
        if (!$url)
            return false;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, true);

        $response = curl_exec($ch);

        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);

        if ($curl_errno != 0)
        {
            // Find a better way to show errors only when reporting is enabled
/*            $mainframe = JFactory::getApplication();
            $err = 'CURL error : ' . $curl_errno . ' ' . $curl_error;
            $mainframe->enqueueMessage($err, 'error');*/
        }

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // TODO: Add recursion allowing for more redirects? Unsure if multi redirects would happen...
        if ($code == 301 || $code == 302)
        {
            list($headers, $body) = explode("\r\n\r\n", $response, 2);

            preg_match("/(Location:|URI:)(.*?)\n/", $headers, $matches);

            if (!empty($matches) && isset($matches[2]))
            {
                $url = JString::trim($matches[2]);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HEADER, true);
                $response = curl_exec($ch);
            }
        }

        list($headers, $body) = explode("\r\n\r\n", $response, 2);

        curl_close($ch);
        return $body;
    }
}

class SCStringUtilities
{
    static function endswith($string, $test)
    {
        $strlen = strlen($string);
        $testlen = strlen($test);
        if ($testlen > $strlen)
            return false;
        return substr_compare($string, $test, -$testlen) === 0;
    }

    static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    static function trimNBSP($htmlText)
    {
        // turn some HTML with non-breaking spaces into a "normal" string
        $converted = strtr($htmlText, array_flip(get_html_translation_table(HTML_ENTITIES, ENT_QUOTES)));
        $converted = trim($converted," \t\n\r\0\x0B".chr(0xC2).chr(0xA0)); // UTF encodes it as chr(0xC2).chr(0xA0)
        return $converted;
    }

    //Recursively implode and trim an array (of strings/arrays)
    static function r_implode($glue, $pieces)
    {
        foreach($pieces as $r_pieces)
        {
            if(is_array($r_pieces))
            {
                unset($r_pieces['id']); // Remove id key which is useless to import
                $retVal[] = SCStringUtilities::r_implode($glue, $r_pieces);
            }
            else
            {
                $retVal[] = trim($r_pieces);
            }
        }
        return implode($glue, $retVal);
    }

    static function substr($str, $start, $length, $encoding = 'UTF-8')
    {
        if(function_exists('mb_substr'))
            $str = mb_substr($str, $start, $length, $encoding);
        else
            $str = substr($str, $start, $length);

        return $str;
    }

    static function strtolower($str)
    {
        if(function_exists('mb_strtolower'))
            $str = mb_strtolower($str);
        else
            $str = strtolower($str);
        return $str;
    }

    /**
     * Loads the translation strings -- this is an internal function, called automatically
     */
    static function loadLanguage($extension, $basePath=JPATH_BASE)
    {
        // Load translations
        $jlang = JFactory::getLanguage();
        $jlang->load($extension, $basePath, 'en-GB', true); // Load English (British)
       // $jlang->load($extension, JPATH_BASE, $jlang->getDefault(), true); // Load the site's default language
        $jlang->load($extension, $basePath, null, true); // Load the currently selected language
    }
}

class SCUserUtilities
{
    static function getAutoUsername($firstName, $lastName, $email, $genericPrefixStr, $socialUserId, $usernamePrefixFormat)
    {
        // Scrub the first/last name for invalid characters that Joomla will deny:
        $stripChars = array('<', '>', '\\', '"', "'", '%', ';', '(', ')', '&');
        $firstName = str_replace($stripChars, '', $firstName);
        $lastName = str_replace($stripChars, '', $lastName);

        if ($usernamePrefixFormat == AUTONAME_EXT) //fb_ or li_
        {
            return $genericPrefixStr . $socialUserId;
        }
        else if ($usernamePrefixFormat == AUTONAME_FIRSTLAST) //first.last
        {
            $firstName = SCStringUtilities::strtolower($firstName);
            $lastName = SCStringUtilities::strtolower($lastName);
            $prefix = $firstName . "." . $lastName;
        }
        else if ($usernamePrefixFormat == AUTONAME_FIRLAS) //firlas
        {
            $firstName = SCStringUtilities::strtolower($firstName);
            $lastName = SCStringUtilities::strtolower($lastName);

            $firstPrefix = SCStringUtilities::substr($firstName, 0, 3);
            $lastPrefix = SCStringUtilities::substr($lastName, 0, 3);
            $prefix = $firstPrefix . $lastPrefix;
        }
        else if ($email != '') //email
        {
            $prefix = $email;
        }

        if($prefix != '')
        {
            $suffix = SCUserUtilities::getUsernameUniqueNumber($prefix);
            return $prefix . $suffix;
        }
        else
            return '';
    }

    static function getUsernameUniqueNumber($prefix)
    {
        $dbo = JFactory::getDBO();
        // First, check if any user has this name
        $query = 'SELECT COUNT(*) FROM #__users WHERE `username` = ' . $dbo->quote($prefix);
        $dbo->setQuery($query);
        $count = $dbo->loadResult();
        if ($count == 0)
            $suffix = "";
        else
        {
            // Get a very strict match to see the last similar username
            $query = 'SELECT CAST(REPLACE(username, ' . $dbo->quote($prefix) . ', "") AS UNSIGNED) suffix FROM #__users WHERE `username` REGEXP "^' . $prefix . '[[:digit:]]+$" ORDER BY `suffix` DESC LIMIT 1';
            $dbo->setQuery($query);
            $suffix = $dbo->loadResult();
            if ($suffix)
            { # increment the last user's number
                $suffix++;
            }
            else
                $suffix = 1;
        }
        return $suffix;
    }

    static function getPostData($postData, $setting)
    {
        $postDataValue = '';
        if(array_key_exists('jform', $postData))
            $postDataValue = $postData['jform'][$setting];
        return $postDataValue;
    }

    static function getDisplayPassword($configModel, $loginRegisterModel, $generatePasswordSetting='generate_random_password')
    {
        if($configModel->getSetting($generatePasswordSetting))
        {
            $newPassword = $loginRegisterModel->generateRandomPassword();
        }
        else
        {
            $newPassword = '';
        }
        return $newPassword;
    }

    static function getDisplayUsername($postData, $firstName, $lastName, $email, $liMemberId, $configModel, $loginRegisterModel,
                                       $generateUsernameSetting='registration_generate_username', $autoUsernameSetting='auto_username_format')
    {
        $postUsername = SCUserUtilities::getPostData($postData, 'username');
        if($postUsername != '')
        {
            $liUsername = $postUsername;
        }
        else if($configModel->getSetting($generateUsernameSetting))
        {
            $usernamePrefixFormat = $configModel->getSetting($autoUsernameSetting);
            $liUsername = $loginRegisterModel->generateUsername($firstName, $lastName, $email, $liMemberId, $usernamePrefixFormat);
        }
        else
        {
            $liUsername = '';
        }
        return $liUsername;
    }

    static function getDisplayNameByFullName($postData, $name)
    {
        $postName = SCUserUtilities::getPostData($postData, 'name');
        if($postName != '')
        {
            $liName = $postName;
        }
        else
        {
            $liName = $name;
        }

        return $liName;
    }

    static function getDisplayNameByFirstLast($postData, $firstName, $lastName)
    {
        $postName = SCUserUtilities::getPostData($postData, 'name');
        if($postName != '')
        {
            $liName = $postName;
        }
        else
        {
            $liName = $firstName . ' ' . $lastName;
        }

        return $liName;
    }

    /**
     * Check passed in email to see if it's already in Joomla
     * If so, return blank, forcing the user to input an email address (and getting validation error if using the same)
     * If not, pre-populate the form with the user's FB address
     * @param string $email Users email address
     * @param string $email1 Returns user's email 1
     * @param string $email2 Returns user's email 2
     * @return string Email value that will be shown on registration form
     */
    static function getDisplayEmail($postData, $email, &$email1, &$email2)
    {
        $postEmail1 = SCUserUtilities::getPostData($postData, 'email1');
        $postEmail2 = SCUserUtilities::getPostData($postData, 'email2');

        if($postEmail1 != '' || $postEmail2 != '')
        {
            $email1 = $postEmail1;
            $email2 = $postEmail2;
        }
        else
        {
            $dbo = JFactory::getDBO();
            $query = "SELECT id FROM #__users WHERE email=" . $dbo->quote($email);
            $dbo->setQuery($query);
            $jEmail = $dbo->loadResult();
            if ($jEmail != null)
            {
                $email1 = "";
                $email2 = "";
            }
            else
            {
                $email1 = $email;
                $email2 = $email;
            }
        }
    }
}