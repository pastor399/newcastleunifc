<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.event.plugin');
jimport('sourcecoast.openGraph');
jimport('sourcecoast.utilities');
jimport('sourcecoast.easyTags');

class plgSystemJFBCSystem extends JPlugin
{
    var $configModel;
    var $jfbcLibrary;
    var $jfbcCanvas;

    var $tagsToReplace = array(
        'jfbclogin' => 'getJFBCLogin',
        'jfbclike' => 'getJFBCLike',
        'jfbcsend' => 'getJFBCSend',
        'jfbccomments' => 'getJFBCComments',
        'jfbccommentscount' => 'getJFBCCommentsCount',
        'jfbcfan' => 'getJFBCFan',
        'jfbcfeed' => 'getJFBCFeed',
        'jfbcfriends' => 'getJFBCFriends',
        'jfbcrecommendations' => 'getJFBCRecommendations',
        'jfbcrecommendationsbar' => 'getJFBCRecommendationsBar',
        'jfbcrequest' => 'getJFBCRequest',
        'jfbcsubscribe' => 'getJFBCSubscribe',
        "sctwittershare" => 'getSCTwitterShare',
        "scgoogleplusone" => 'getSCGPlusOne',
        "jlinkedshare" => 'getJLinkedShare'
    );

    var $metadataTagsToStrip = array('JFBC', 'JLinked', 'SCTwitterShare', 'SCGooglePlusOne');

    static $cssIncluded = false;

    function __construct(& $subject, $config)
    {
        jimport('joomla.filesystem.file');
        $libFile = JPATH_ROOT . '/components/com_jfbconnect/libraries/facebook.php';
        if (!JFile::exists($libFile))
            JError::raiseError(0, "File missing: " . $libFile . "<br/>Please re-install JFBConnect or disable the JFBCSystem Plugin");
        require_once($libFile);
        $this->jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $this->configModel = $this->jfbcLibrary->getConfigModel();

        $canvasFile = JPATH_ROOT . '/components/com_jfbconnect/libraries/canvas.php';
        if (!JFile::exists($canvasFile))
            JError::raiseError(0, "File missing: " . $canvasFile . "<br/>Please re-install JFBConnect or disable the JFBCSystem Plugin");
        require_once($canvasFile);
        $this->jfbcCanvas = JFBConnectCanvasLibrary::getInstance();

        parent::__construct($subject, $config);
    }

    public function onAfterInitialise()
    {
        $app = JFactory::getApplication();
        if (!$app->isAdmin())
        {
            $this->jfbcCanvas->setupCanvas();

            // Need to disable Page caching so that values fetched from Facebook are not saved for the next user!
            // Do this by setting the request type to POST. In the Cache plugin, it's checked for "GET". can't be that.
            $option = JRequest::getCmd("option");
            $view = JRequest::getCmd("view");
            if ($option == 'com_jfbconnect' && $view == 'loginregister')
                $_SERVER['REQUEST_METHOD'] = 'POST';
        }

        // Need to load our plugin group early to be able to hook into to every step after
        JPluginHelper::importPlugin('opengraph');
    }

    public function onAfterRoute()
    {
        $app = JFactory::getApplication();
        if (!$app->isAdmin())
        {
            $app = JFactory::getApplication();
            $app->triggerEvent('onOpenGraphAfterRoute');
        }
    }

    // Called after the component has executed and it's output is available in the buffer
    // Modules have *not* executed yet
    public function onAfterDispatch()
    {
        $app = JFactory::getApplication();
        if (!$app->isAdmin())
        {
            $this->jfbcLibrary->initDocument();

            foreach ($this->metadataTagsToStrip as $metadataTag)
            {
                $this->replaceTagInMetadata($metadataTag);
            }

            $doc = JFactory::getDocument();
            if ($doc->getType() == 'html')
            {
                $doc->addCustomTag('<JFBConnectSCTwitterJSPlaceholder />');
                $doc->addCustomTag('<JFBConnectSCGooglePlusOneJSPlaceholder />');
                $doc->addCustomTag('<JLinkedJfbcJSPlaceholder />');
            }

            //Add Login with FB button to com_users login view and mod_login
            if ($this->configModel->getSetting('show_login_with_joomla_reg'))
            {
                $renderKey = SCSocialUtilities::getJFBConnectRenderKey();
                $loginTag = '{JFBCLogin logout=false' . $renderKey . '}';
                SCEasyTags::extendJoomlaUserForms($loginTag);
            }

            // Add the Open Graph links to the user edit form.
            if ($this->jfbcLibrary->userIsConnected()
                    && ((JRequest::getCmd('option') == 'com_users' && JRequest::getCmd('view') == 'profile') ||
                            (JRequest::getCmd('option') == 'com_user' && JRequest::getCmd('view') == 'user'))
                    && $this->showOpenGraphProfileLinks()
            )
            {
                $lang = JFactory::getLanguage();
                $lang->load('com_jfbconnect');

                $document = JFactory::getDocument();
                $output = $document->getBuffer('component');
                $newLinks = '<a href="' . JRoute::_('index.php?option=com_jfbconnect&controller=opengraph&layout=activity') . '">' . JText::_('COM_JFBCONNECT_TIMELINE_ACTIVITY_LINK') . '</a>';
                $newLinks .= '<br/><a href="' . JRoute::_('index.php?option=com_jfbconnect&controller=opengraph&layout=settings') . '">' . JText::_('COM_JFBCONNECT_TIMELINE_CHANGESETTINGS') . '</a>';
                $contents = $output . $newLinks;
                $document->setBuffer($contents, 'component');
            }

            JPluginHelper::importPlugin('opengraph');
            $app->triggerEvent('onOpenGraphAfterDispatch');
        }
    }

    // Called right before the page is rendered
    public function onBeforeRender()
    {
        // Can't use until we ditch J1.5
    }

    public function onAfterRender()
    {
        $app = JFactory::getApplication();
        if (!$app->isAdmin())
        {
            $body = JResponse::getBody();

            // Add FB built-in and custom OG tag for namespace, if applicable
            $ogNamespaces = 'xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#" ';
            $appConfig = $this->configModel->getSetting('autotune_app_config', array());
            if (array_key_exists('namespace', $appConfig))
            {
                $appNamespace = $appConfig['namespace'];
                if ($appNamespace != '')
                    $ogNamespaces .= 'xmlns:' . $appNamespace . '="http://ogp.me/ns/fb/' . $appNamespace . '#" ';
            }
            $body = str_ireplace("<html ", '<html ' . $ogNamespaces, $body);

            $loginModalDiv = $this->getModalLogin();
            $body = str_ireplace("</body>", $loginModalDiv . "</body>", $body);

            $fbApiJs = $this->getJavascript($this->jfbcLibrary->facebookAppId);

            if (preg_match('/\<body[\s\S]*?\>/i', $body, $matches))
            {
                $newBody = str_replace($matches[0], $matches[0] . $fbApiJs, $body);
                JResponse::setBody($newBody);
            }

            $this->doTagReplacements();
        }
        return true;
    }

    private function showOpenGraphProfileLinks()
    {
        $db = JFactory::getDbo();
        $db->setQuery('SELECT COUNT(*) FROM #__opengraph_action WHERE published = 1');
        $numOGActionsEnabled = $db->loadResult();

        $user = JFactory::getUser();
        $db->setQuery('SELECT COUNT(*) FROM #__opengraph_activity WHERE user_id = ' . $user->id . ' AND status=1');
        $numActivities = $db->loadResult();

        return ($numOGActionsEnabled > 0) || ($numActivities > 0);
    }

    private function replaceTagInMetadata($metadataTag)
    {
        $doc = JFactory::getDocument();
        $description = $doc->getDescription();
        $replace = SCSocialUtilities::stripSystemTags($description, $metadataTag);

        if ($replace)
        {
            $description = SCStringUtilities::trimNBSP($description);
            $doc->setDescription($description);
        }
    }

    private function getLocale()
    {
        $fbLocale = $this->jfbcLibrary->getFacebookOverrideLocale();

        // Get the language to use
        if ($fbLocale == '')
        {
            $lang = JFactory::getLanguage();
            $locale = $lang->getTag();
        } else
        {
            $locale = $fbLocale;
        }

        $locale = str_replace("-", "_", $locale);
        return $locale;
    }

    private function getJavascript($appId)
    {
        $locale = $this->getLocale();
        // get Event Notification subscriptions
        $subs = "\nFB.Event.subscribe('comment.create', jfbc.social.comment.create);";
        $subs .= "\nFB.Event.subscribe('edge.create', jfbc.social.like.create);";
        if ($this->configModel->getSetting('social_notification_google_analytics'))
            $subs .= "\njfbc.social.googleAnalytics.trackFacebook();";

        $fbsiteurl = JURI::root();
        $channelurl = $fbsiteurl . 'components/com_jfbconnect/assets/jfbcchannel.php';
        if ($this->jfbcCanvas->get('resizeEnabled', false))
            $resizeCode = "window.setTimeout(function() {\n" .
                    "  FB.Canvas.setAutoGrow();\n" .
                    "}, 250);";
        else
            $resizeCode = "";

        if ($this->jfbcCanvas->get('canvasEnabled', false))
            $canvasCode = "jfbc.canvas.checkFrame();";
        else
            $canvasCode = "";

        // Figure out if status:true should be set. When false, makes page load faster
        $user = JFactory::getUser();
        $guest = $user->guest;
        // Check cookie to make sure autologin hasn't already occurred once. If so, and we try again, there will be loops.
        $autoLoginPerformed = JRequest::getInt('jfbconnect_autologin_disable', 0, 'COOKIE');
        if ($this->configModel->getSetting('facebook_auto_login') && $guest && !$autoLoginPerformed)
        {
            $status = 'status: true,';
            // get Event Notification subscriptions
            $subs .= "\nFB.Event.subscribe('auth.authResponseChange', function(response) {jfbc.login.on_login();});";
        } else
            $status = 'status: false,';

        if ($appId)
            $appIdCode = "appId: '" . $appId . "', ";
        else
            $appIdCode = "";
        $javascript =
                <<<EOT
<div id="fb-root"></div>
<script type="text/javascript">
{$canvasCode}\n
window.fbAsyncInit = function() {
FB.init({{$appIdCode}{$status} cookie: true, xfbml: true, oauth: true, channelUrl: '{$channelurl}'});{$subs}{$resizeCode}
};
(function(d){
     var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/{$locale}/all.js";
     d.getElementsByTagName('head')[0].appendChild(js);
   }(document));
</script>
EOT;

        return $javascript;
    }

    private function getModalLogin()
    {
        // Should the modal popup be displayed?
        $showLoginModal = $this->configModel->getSetting('facebook_login_show_modal');
        if ($showLoginModal)
        {
            $lang = JFactory::getLanguage();
            $lang->load('com_jfbconnect');
            $loginModalDiv = '<div style="display:none;position:absolute"><div id="jfbcLoginModal" style="width:500px;text-align:center;position:absolute;height:50px;top:50%;margin-top:-13px;color:#000">' . JText::_('COM_JFBCONNECT_LOGIN_POPUP') . '</div></div>';
        } else
            $loginModalDiv = "";

        return $loginModalDiv;
    }

    private function doTagReplacements()
    {
        $twitterTagFound = false;
        $googlePlusTagFound = false;
        $jLinkedTagFound = false;

        $tagsFound = false;
        $tagKeys = array_keys($this->tagsToReplace);
        foreach ($tagKeys as $tag)
        {
            $lowercaseTag = strtolower($tag);

            /*
             * Code to strip any {JFBCxyz} tags from head. Experimental setting for now.
             */
            //Get the head
            $experimentalSettings = $this->configModel->getSetting('experimental');
            $stripTags = $experimentalSettings->get('strip_head', true);
            if ($stripTags)
            {
                $content = JResponse::getBody();
                $regex = '|<head(.*)?</head>|sui';
                if (preg_match($regex, $content, $matches))
                {
                    if (count($matches) == 2) // more than one head is a problem, don't do anything
                    {
                        //Remove the tag if it's in the head
                        $newHead = preg_replace('|{' . $tag . '(.*?)}|ui', '', $matches[0], -1, $count);

                        if ($count > 0)
                        {
                            //Replace the head
                            $content = preg_replace('|<head(.*)?</head>|sui', $newHead, $content, -1, $count);
                            if ($count == 1) // Only update the body if exactly one head was found and replaced
                                JResponse::setBody($content);
                        }
                    }
                }
            }

            //Tag has passed in values
            $regex = '/\{' . $tag . '\s+(.*?)\}/i';
            $currentTag1Found = $this->replaceTag($lowercaseTag, $regex);
            $tagsFound = $currentTag1Found || $tagsFound;

            //Tag with no values
            $regex = '/\{' . $tag . '}/i';
            $currentTag2Found = $this->replaceTag($lowercaseTag, $regex);
            $tagsFound = $currentTag2Found || $tagsFound;

            if ($currentTag1Found || $currentTag2Found)
            {
                if ($lowercaseTag == 'sctwittershare')
                    $twitterTagFound = true;
                else if ($lowercaseTag == 'scgoogleplusone')
                    $googlePlusTagFound = true;
                else if ($lowercaseTag == 'jlinkedshare')
                    $jLinkedTagFound = true;
            }
        }
        $this->replaceGraphTags();
        $this->replaceJSPlaceholders($twitterTagFound, $googlePlusTagFound, $jLinkedTagFound);
    }

    private function replaceJSPlaceholders($twitterTagFound, $googlePlusTagFound, $jLinkedTagFound)
    {
        $uri = JURI::getInstance();
        $scheme = $uri->getScheme();

        //Twitter
        $twitterPlaceholder = '<JFBConnectSCTwitterJSPlaceholder />';
        if ($twitterTagFound)
            $twitterJavascript = '<script src="' . $scheme . '://platform.twitter.com/widgets.js"></script>';
        else
            $twitterJavascript = '';

        //GooglePlus
        $googlePlaceholder = '<JFBConnectSCGooglePlusOneJSPlaceholder />';
        if ($googlePlusTagFound)
        {
            $googleJavascript = "<script type=\"text/javascript\">
              (function() {
                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                po.src = '" . $scheme . "://apis.google.com/js/plusone.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
              })();
            </script>";
        } else
            $googleJavascript = '';

        //JLinked
        $jLinkedPlaceholder = '<JLinkedJfbcJSPlaceholder />';
        $jLinkedEnabled = SCSocialUtilities::isJLinkedInstalled() && SCSocialUtilities::areJLinkedTagsEnabled();
        if ($jLinkedTagFound && !$jLinkedEnabled)
        {
            $jLinkedJavascript = '<script src="' . $scheme . '://platform.linkedin.com/in.js"></script>';
            if ($scheme == 'https')
            {
                $jLinkedJavascript .= '<script type="text/javascript">' .
                        "IN.Event.on(IN,'frameworkLoaded',function(){if(/^https:\/\//i.test(location.href)){IN.ENV.images.sprite='https://www.linkedin.com/scds/common/u/img/sprite/'+IN.ENV.images.sprite.split('/').pop()}});
                    </script>";
            }
        } else
            $jLinkedJavascript = '';

        //Replace placeholder with Javascript if needed
        $contents = JResponse::getBody();
        $contents = str_replace($twitterPlaceholder, $twitterJavascript, $contents);
        $contents = str_replace($googlePlaceholder, $googleJavascript, $contents);
        $contents = str_replace($jLinkedPlaceholder, $jLinkedJavascript, $contents);
        JResponse::setBody($contents);
    }

    private function replaceTag($method, $regex)
    {
        $replace = FALSE;
        $contents = JResponse::getBody();
        if (preg_match_all($regex, $contents, $matches, PREG_SET_ORDER))
        {
            $count = count($matches[0]);
            if ($count == 0)
                return true;

            $jfbcRenderKey = SCSocialUtilities::getJFBConnectRenderKeySetting();

            foreach ($matches as $match)
            {
                if (isset($match[1]))
                    $val = $match[1];
                else
                    $val = '';

                $cannotRender = SCEasyTags::cannotRenderEasyTag($val, $jfbcRenderKey);
                if ($cannotRender)
                    continue;

                if (array_key_exists($method, $this->tagsToReplace))
                {
                    $methodName = $this->tagsToReplace[$method];
                    $newText = call_user_func(array('SCEasyTags', $methodName), $val);
                    $replace = TRUE;

                    if (!self::$cssIncluded)
                    {
                        self::$cssIncluded = true;
                        $newText = '<link rel="stylesheet" href="' . JURI::base(false) . 'components/com_jfbconnect/assets/jfbconnect.css" type="text/css" />' . $newText;
                    }
                } else
                {
                    $newText = '';
                    $replace = FALSE;
                }

                $search = '/' . preg_quote($match[0], '/') . '/';
                $contents = preg_replace($search, $newText, $contents, 1);
            }
            if ($replace)
                JResponse::setBody($contents);
        }

        return $replace;
    }

    private function getGraphContents($regex, &$contents, &$newGraphTags)
    {
        if (preg_match_all($regex, $contents, $matches, PREG_SET_ORDER))
        {
            $count = count($matches[0]);
            if ($count == 0)
                return true;

            $jfbcRenderKey = SCSocialUtilities::getJFBConnectRenderKeySetting();
            $jLinkedRenderKey = SCSocialUtilities::getJLinkedRenderKeySetting();

            foreach ($matches as $match)
            {
                if (isset($match[1]))
                    $val = $match[1];
                else
                    $val = '';

                $cannotRenderJFBC = SCEasyTags::cannotRenderEasyTag($val, $jfbcRenderKey);
                $cannotRenderJLinked = SCEasyTags::cannotRenderEasyTag($val, $jLinkedRenderKey);

                if ($cannotRenderJFBC && $cannotRenderJLinked)
                    continue;

                $newGraphTags[] = $val;
                $contents = str_replace($match[0], '', $contents);
            }
        }
    }

    private function replaceGraphTags()
    {
        // TODO - deprecate JFBCGraph tags
        $placeholder = '<SCOpenGraphPlaceholder />';
        $regex1 = '/\{JFBCGraph\s+(.*?)\}/i';
        $regex2 = '/\{SCOpenGraph\s+(.*?)\}/i';

        $newGraphTags1 = array();
        $newGraphTags2 = array();

        $contents = JResponse::getBody();
        $this->getGraphContents($regex1, $contents, $newGraphTags1);
        $this->getGraphContents($regex2, $contents, $newGraphTags2);

        $newGraphTags = array_merge($newGraphTags1, $newGraphTags2);

        //Replace Placeholder with new Head tags
        $defaultGraphFields = $this->configModel->getSetting('social_graph_fields');
        $locale = $this->getLocale();

        $openGraphLibrary = OpenGraphLibrary::getInstance();
        $openGraphLibrary->addOpenGraphEasyTags($newGraphTags);
        $openGraphLibrary->addDefaultSettingsTags($defaultGraphFields);
        $openGraphLibrary->addAutoGeneratedTags($locale);
        $graphTags = $openGraphLibrary->buildCompleteOpenGraphList();

        $contents = $openGraphLibrary->removeOverlappingTags($contents);
        $search = '/' . preg_quote($placeholder, '/') . '/';
        $contents = preg_replace($search, $graphTags, $contents, 1);
        $contents = str_replace($placeholder, '', $contents); //If JLinked attempts to insert, ignore
        JResponse::setBody($contents);
    }
}
