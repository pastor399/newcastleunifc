<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

$params = &JComponentHelper::getParams( 'COM_LENDR' );
$feeds = explode(';', trim($params->get( 'xml_feedlist' )));
$title = array("0"=>"Football Federation Australia", "1"=>"Northern NSW Football", "2"=>"Newcastle Football");

?>

    <div id="j-sidebar-container" class="span2">
        <span>Quick Links</span>
    </div>
    <div id="j-main-container" style="border-radius:5px !important;" class="span10">
		<h3 style="text-align: center">Ajax Syndicated News Loader</h3>
		
		<img style="padding:10px; float:left" src="http://upload.wikimedia.org/wikipedia/en/thumb/4/43/Feed-icon.svg/128px-Feed-icon.svg.png"/>
    	<div style="padding:10px; min-height:128px;">
    		The <b>Ajax Syndicated News Loader</b> component provides a link between joomla modules and <b>Rich Site Summary (RSS)</b> sources.  RSS is a format for delivering regularly changing web content. Many news-related sites, weblogs and other online publishers syndicate their content as an RSS Feed to whoever wants it.
			RSS solves a problem for people who regularly use the web. However the loading times for large RSS feeds can severly impact the load times of Joomla websites.
			By harnessing the power of <b>jQuery</b> the <b>ASNL</b> component alows your webpage to continue to load while the RSS feeds are fetched in the background.
    	</div>
    <br>
    <hr>
    <br>
        <table style="margin:auto !important;">
    		<tr><th>ID</th><th>Title</th><th>Link</th><th>Status</th></tr>
    		<?php $i=1; foreach($feeds as $feed)
    		{
    		echo "<tr>";
    			echo "<td>".$i."</td>";
    			echo "<td>".$title[$i-1]."</td>";
    			echo "<td><a href=".$feed." title=".$feed.">".substr($feed,0,35)."...</a></td>";
    			echo "<td></td>";
    		echo "</tr>";
    		$i++;
    		} ?>
    	</table>
    <br>
    <br>
    </div>
   