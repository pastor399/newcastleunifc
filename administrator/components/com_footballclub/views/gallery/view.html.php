<?php
/**
 * @version     1.0.5
 * @package     com_footballclub
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Footballclub.
 */
class FootballclubViewGallery extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
 
        print_r (JRequest::getVar('limitstart', 0, '', 'int'));

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			throw new Exception(implode("\n", $errors));
		}
        
		FootballclubHelper::addSubmenu('gallery');
        
		$this->addToolbar();
        
        $this->sidebar = JHtmlSidebar::render();
        
        //Get The List Of Albums And Cover_PID.
        $this->AlbumList = $this->get('AlbumList');
        
        //print_r ($this->AlbumList);       
		
        parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/footballclub.php';

		JToolBarHelper::title(JText::_('COM_FOOTBALLCLUB_TITLE_GALLERY'), 'gallery.png');

		if (true) {
			JToolBarHelper::preferences('com_footballclub');
		}
        
        $this->extra_sidebar = '';
        
        
	}
    

    
}
