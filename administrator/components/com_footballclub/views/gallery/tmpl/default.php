<?php
/**
 * @version     1.0.5
 * @package     com_footballclub
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_footballclub/assets/css/footballclub.css');

$user	= JFactory::getUser();
$userId	= $user->get('id');

?>

<?php
//Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar)) {
    $this->sidebar .= $this->extra_sidebar;
}
?>

<form action="<?php echo JRoute::_('index.php?option=com_footballclub&view=gallery'); ?>" method="post" name="adminForm" id="adminForm">
<?php if(!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
    
		<div id="filter-bar" class="btn-toolbar">

		</div>        
		<div class="clearfix"> </div>
		<table class="table table-striped" id="albumList">

			<tfoot>

			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); 
                          echo $this->pagination->getLimitBox();
                          echo $this->pagination->getPagesCounter();
                    ?>
				</td>
			</tr>
			</tfoot>
            <tbody>
                <div class="row-fluid show-grid">
                    <?php //print_r($this->AlbumList); print_r ($this->pagination); ?>
                    <?php 
                    foreach($this->AlbumList as $album)
                    {
                        echo '<article class="span3">';
                        echo '<div class="gallery_thumb" style="background-image: url('. $album['src_small'] .'); "> </div>';
                        echo '<div class="album_title">'. $album['name'] . '</div>';
                        echo '</article>';   
                    }
                    ?>
                </div>
            </tbody>
		</table>
        
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>        

		
