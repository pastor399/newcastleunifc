<?php
/**
 * @version     1.0.0
 * @package     com_footballclub
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Footballclub.
 */
class FootballclubViewOvals extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			throw new Exception(implode("\n", $errors));
		}
        
		FootballclubHelper::addSubmenu('sponsors');
        FootballclubHelper::addSubmenu('training');
        FootballclubHelper::addSubmenu('fixtures');
        FootballclubHelper::addSubmenu('ovals', 1);
        FootballclubHelper::addSubmenu('gallery');
        FootballclubHelper::addSubmenu('registration');
        FootballclubHelper::addSubmenu('history');
        FootballclubHelper::addSubmenu('contact');
        
		$this->addToolbar();
        
        $this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/footballclub.php';

		$state	= $this->get('State');

		JToolBarHelper::title(JText::_('COM_FOOTBALLCLUB_TITLE_OVALS'), 'sponsors.png');
        JToolbarHelper::back( JText::_( 'HOME' ) , 'index.php?option=com_footballclub' );  
        
		if (true) {
			JToolBarHelper::preferences('com_footballclub');
		}
        
        //Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_footballclub&view=ovals');
        
        $this->extra_sidebar = '';
	}
        
}