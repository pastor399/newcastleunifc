<?php
/**
 * @version     1.0.0
 * @package     com_footballclub
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */

// no direct access
defined('_JEXEC') or die;

$base = JURI::root();

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');

    $params = JComponentHelper::getParams('com_footballclub');
    //$url = "http://nothing.rap";
    $url = $params->get('url');
    // Import CSS
    $document = JFactory::getDocument();
    $document->addStyleSheet('components/com_footballclub/assets/css/footballclub.css');
    



?>
<form action="<?php echo JRoute::_('index.php?option=com_footballclub&view=sponsors'); ?>" >

    <?php if(!empty($this->sidebar)): ?>
    	<div id="j-sidebar-container" class="span2">
    		<?php echo $this->sidebar; ?>
    	</div>
    	<div id="j-main-container" class="span10">
    <?php else : ?>
    	<div id="j-main-container">
    <?php endif;?>
    	







<table width="100%">
  <tr>
    <td width="65%">
        <h4><b>Provides Updated Status Details For Grounds Managed By <a href="http://www.theforum.org" target="blank">NUSPORT</a>.</b></h4>
        <b>The oval status information is accessed by scraping a website where the information is displayed. <br>Utilizing <b>SimpleHtmlDom</b> the
        server loads the webpage and finds the information that is relevant and discards the remainder. <br /><br />
        Basic Options Are Available Through The <b><b>Options</b></b> Button Above.
        <br />
        <br />
        <h4>A Preview Of The Output Is Seen Below.</h4>
    </td>
    <td width="35%" style="text-align: center; background: #DFDFDF;">
        <h4>Page To Read Details From</h4>
        <a href="<?php echo $url; ?>" target="blank"><?php echo $url ?></a>
        <br />
        <?php
            if(@get_headers($url) == false)
            {
                echo ('<span style="font-weight: bold; color: red;">');
                echo 'URL ERROR';
                echo '</span>';   
            }else{
                echo ('<span style="font-weight: bold; color: green;">');
                echo 'URL REACHABLE';
                echo '</span>';
            }
        ?>
        <br /><br />
        <span style="font-size: 10px;"><b>IF THIS PRODUCES AN ERROR THE SYSTEM WILL NOT WORK<br>CHANGE THE URL IN THE OPTIONS</b></span>
    </td>
  </tr>
</table>
<div>
    <div id="raw_preview">
        <div class="preview"></div>
        <div class="loader" style="background: url(<?php echo $this->baseurl; ?>/components/com_footballclub/assets/images/loader.gif);"></div>
    </div>
</div>

</form>   







<script>
jQuery(document).ready(function() {
    jQuery.get('<?php echo $base; ?>/index.php?option=com_footballclub&view=ovals&format=raw', function(data) {
        jQuery('.result').html(data);
          jQuery('.preview').fadeOut(0);
          jQuery(".loader").delay(1500).fadeOut('slow');
          jQuery('.preview').delay(1600).append(data).fadeIn('slow');
    });
});
</script>

<style>
#raw_preview { 
    width: 350px;
    background: white; 
    height: 300px; 
    border-radius: 10px; 
}
.loader { 
    position: absolute;
    width:350px;
    height:300px;
    background-position: center !important;
    background-repeat: no-repeat !important;
}
.preview { width: 350px; position: absolute; dispaly: none; text-align: center; }
.preview ul { list-style: none; margin: 0 !important; }
</style>

