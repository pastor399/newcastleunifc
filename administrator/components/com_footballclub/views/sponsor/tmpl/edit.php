<?php
/**
 * @version     1.0.0
 * @package     com_footballclub
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_footballclub/assets/css/footballclub.css');
?>
<script type="text/javascript">
    
    
	Joomla.submitbutton = function(task)
	{
        if(task == 'sponsor.cancel'){
            Joomla.submitform(task, document.getElementById('sponsor-form'));
        }
        
		if (document.formvalidator.isValid(document.id('sponsor-form'))) {
			Joomla.submitform(task, document.getElementById('sponsor-form'));
		}
		else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>
        <form action="<?php echo JRoute::_('index.php?option=com_footballclub&layout=edit&id='.(int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="sponsor-form" class="form-validate">
        	<div class="row-fluid">
        		<div class="span12 form-horizontal">
                    <div style="width: calc(50% - 3px); float:left;">
                        <fieldset class="adminform" style="border-radius: 5px 0 0 5px !important; padding-left: 10px;">
                			<div class="control-group" style=" padding-top: 10px; ">
                				<div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
                				<div class="controls"><?php echo $this->form->getInput('id'); ?></div>
                			</div>
                			<div class="control-group">
                				<div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
                				<div class="controls"><?php echo $this->form->getInput('state'); ?></div>
                			</div>
                			<div class="control-group">
                				<div class="control-label"><?php echo $this->form->getLabel('created_by'); ?></div>
                				<div class="controls"><?php echo $this->form->getInput('created_by'); ?></div>
                			</div>
                			<div class="control-group">
                				<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
                				<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
                			</div>
                			<div class="control-group">
                				<div class="control-label"><?php echo $this->form->getLabel('description'); ?></div>
                				<div class="controls"><?php echo $this->form->getInput('description'); ?></div>
                			</div>
                			<div class="control-group">
                				<div class="control-label"><?php echo $this->form->getLabel('website'); ?></div>
                				<div class="controls"><?php echo $this->form->getInput('website'); ?></div>
                			</div>
                			<div class="control-group">
                				<div class="control-label"><?php echo $this->form->getLabel('directions'); ?></div>
                				<div class="controls"><?php echo $this->form->getInput('directions'); ?></div>
                			</div>
                			<div class="control-group">
                				<div class="control-label"><?php echo $this->form->getLabel('image_banner'); ?></div>
                				<div class="controls"><?php echo $this->form->getInput('image_banner'); ?></div>
                			</div>
                			<div class="control-group">
                				<div class="control-label"><?php echo $this->form->getLabel('image_frontpage'); ?></div>
                				<div class="controls"><?php echo $this->form->getInput('image_frontpage'); ?></div>
                			</div>
                			<div class="control-group">
                				<div class="control-label"><?php echo $this->form->getLabel('image_small'); ?></div>
                				<div class="controls"><?php echo $this->form->getInput('image_small'); ?></div>
                			</div>
                			<div class="control-group">
                				<div class="control-label"><?php echo $this->form->getLabel('image_mini'); ?></div>
                				<div class="controls"><?php echo $this->form->getInput('image_mini'); ?></div>
                			</div>
                        </fieldset>
                     </div>
                     <div style="width: calc(50% - 3px); float:left; border-radius: 0 5px 5px 0; background-color: #F5F5F5; height:656px;"
                        
                     </div>
            	</div>                
        
                <input type="hidden" name="task" value="" />
                <?php echo JHtml::_('form.token'); ?>
                
            </div>
        </form>