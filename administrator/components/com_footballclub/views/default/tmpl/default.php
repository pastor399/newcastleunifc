<?php
/**
 * @version     1.0.0
 * @package     com_footballclub
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$mainframe = JFactory::getApplication();
JHtml::_('behavior.modal');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_footballclub/assets/css/footballclub.css');

$user	= JFactory::getUser();
$userId	= $user->get('id');
?>
<table id="footballclub_panel">
	<tbody>
        <tr>
	       <td width="58%" valign="top" style="padding:10px">
	           <ul id="footballclub-items">
               
	               <li>
                        <a href="/NewcastleUniFC30/administrator/index.php?option=com_footballclub&amp;view=sponsors">
                            <img alt="Sponsors" src="<?php echo $this->baseurl; ?>/components/com_footballclub/assets/images/sponsors.png" />
                            <span class="item-title">
                                <span>Sponsors</span>
                            </span>
                        </a>
                   </li>
                   
	               <li>
                        <a href="/NewcastleUniFC30/administrator/index.php?option=com_footballclub&amp;view=training">
                            <img alt="Training" src="<?php echo $this->baseurl; ?>/components/com_footballclub/assets/images/training.png" />
                            <span class="item-title">
                                <span>Training</span>
                            </span>
                        </a>
                   </li>
                   
	               <li>
                        <a href="/NewcastleUniFC30/administrator/index.php?option=com_footballclub&amp;view=fixtures">
                            <img alt="Fixtures" src="<?php echo $this->baseurl; ?>/components/com_footballclub/assets/images/fixtures.png" />
                            <span class="item-title">
                                <span>Fixtures</span>
                            </span>
                        </a>
                   </li>

	               <li>
                        <a href="/NewcastleUniFC30/administrator/index.php?option=com_footballclub&amp;view=ovals">
                            <img alt="Oval Status" src="<?php echo $this->baseurl; ?>/components/com_footballclub/assets/images/ovals.png" />
                            <span class="item-title">
                                <span>Oval Status</span>
                            </span>
                        </a>
                   </li>

	               <li>
                        <a href="/NewcastleUniFC30/administrator/index.php?option=com_footballclub&amp;view=gallery">
                            <img alt="Gallery" src="<?php echo $this->baseurl; ?>/components/com_footballclub/assets/images/gallery.png" />
                            <span class="item-title">
                                <span>Photo Gallery</span>
                            </span>
                        </a>
                   </li>

	               <li>
                        <a href="/NewcastleUniFC30/administrator/index.php?option=com_footballclub&amp;view=registration">
                            <img alt="Registration" src="<?php echo $this->baseurl; ?>/components/com_footballclub/assets/images/registration.png" />
                            <span class="item-title">
                                <span>Registration</span>
                            </span>
                        </a>
                   </li>

	               <li>
                        <a href="/NewcastleUniFC30/administrator/index.php?option=com_footballclub&amp;view=history">
                            <img alt="History" src="<?php echo $this->baseurl; ?>/components/com_footballclub/assets/images/history.png" />
                            <span class="item-title">
                                <span>History</span>
                            </span>
                        </a>
                   </li>

	               <li>
                        <a href="/NewcastleUniFC30/administrator/index.php?option=com_footballclub&amp;view=contact">
                            <img alt="Contact" src="<?php echo $this->baseurl; ?>/components/com_footballclub/assets/images/contact.png" />
                            <span class="item-title">
                                <span>Contact</span>
                            </span>
                        </a>
                   </li>
                   
<!--
                   <li>
                       <a rel="{handler: 'iframe', size: {x: 780, y: 560}}" class="modal" href="index.php?option=com_jce&view=editor&layout=plugin&plugin=imgmanager"</a>
                   </li>
-->
                   
                </ul>
            </td>
            <td width="42%"></td>
        </tr>
    </tbody>
</table>

		
