CREATE TABLE IF NOT EXISTS `#__footballclub_sponsors` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`description` TEXT NOT NULL ,
`website` VARCHAR(255)  NOT NULL ,
`directions` VARCHAR(255)  NOT NULL ,
`image_banner` VARCHAR(255)  NOT NULL ,
`image_frontpage` VARCHAR(255)  NOT NULL ,
`image_small` VARCHAR(255)  NOT NULL ,
`image_mini` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_bin;

