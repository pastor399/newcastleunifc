<?php
/**
 * @version     1.0.5
 * @package     com_footballclub
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.application.component.helper');

/**
 * Methods supporting a list of Footballclub records.
 */
class FootballclubModelgallery extends JModelItem
{
    var $_total = null;
    var $_pagination = null;
    
    var $_limit = null;
    var $_limistart = null;
    
    protected $params;
    
    protected $AlbumList = null;
    protected $DisplayParams;
    
    function __construct()
    {
        parent::__construct();
        
        $app = JFactory::getApplication();
        $this->params = JComponentHelper::getParams('com_footballclub');
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        
        $this->_limit = $limit;
        $this->_limitstart = $limitstart;
        
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
    }
    
    public function UpdateAlbums()
    {   	
    	//QUERY FACEBOOK FOR THE CURRENT LIST OF ALBUMS AND DETAILS
    	//GET THE ID OF PAGE TO QUERY
    	$pageID = $this->params->get('pageid');
    	
    	//JFBConnect OR Facebook SDK
    	switch($this->params->api){
    		case 0:
    			$facebook = JFBConnectFacebookLibrary::getInstance();
    			break;
    		default:
    			require_once('components/com_footballclub/FacebookSDK/facebook.php');
    			$config = array();
    			$config['appId'] = $this->DisplayParams->AppId;
    			$config['secret'] = $this->DisplayParams->AppSecret;
    			$config['fileUpload'] = false; // optional
    			$facebook = new Facebook($config);
    	}
    	
    	$query = "SELECT aid, name, cover_pid FROM album WHERE owner = " . $pageID .
    	" AND type = 'normal' AND name != 'Cover Photos' AND name != 'Profile Pictures'";
    	$fql_albumlist = $facebook->api( array(
    			'method' => 'fql.query',
    			'query' => $query, ));
    	
    	//STORE THE CURRENT ALBUM LIST IN THE VARIABLE
    	$this->AlbumList = $fql_albumlist;
    	
    	//Get The List Of PID's Saved In The Database.
    	$dbo = $this->getDbo();
    	$dbQuery = $dbo->getQuery(true);
    	$dbQuery->select('pid');
    	$dbQuery->from('#__footballclub_gallery');
    	$dbo->setQuery($dbQuery);
    	$ext_pid = $dbo->loadObjectList();
    	//CONVERT THE OBJECT LIST TO AN ARRAY OF PID'S
    	$album_ids = array();
    	foreach($ext_pid as $album)
    	{
    		array_push($album_ids, $album->pid);
    	}
        $pid_array = $album_ids;
    	//FOR EACH ALBUM, SEARCH FOR ITS COVER_PID IN THE DATABASE
    	foreach ($this->AlbumList as &$Album)
    	{
    		//IF COVER_PID EXISTS, ADD ITS DETAILS TO THE CURRENT ALBUM OBJECT
    		if ($this->search_array($Album['cover_pid'], $pid_array) == true)
    		{
    			$dbo = $this->getDbo();
    			$dbQuery = $dbo->getQuery(true);
    			$dbQuery->select('src_small');
    			$dbQuery->from('#__footballclub_gallery');
    			$dbQuery->where('pid="' . $Album['cover_pid'] . '"');
    			$dbo->setQuery($dbQuery);
    			$src_small = $dbo->loadResult();
    			$Album['src_small'] = $src_small;
    		}else{
    			$photo = $this->fqlPhoto($Album['cover_pid']);
    			//CREATE A EMPTY OBJECT TO STORE PID DETAILS
    			$insert = new stdClass();
    			$insert->pid = $Album['cover_pid'];
    			$insert->album_name = $Album['name'];
    			$insert->src_small=$photo[0]['images'][6]['source'];
    			$insert->src_large=$photo[0]['images'][0]['source'];
    			$src_small = $insert->src_small;
    			try {
    				//INSERT NEW PID OBJECT INTO THE DATABASE
    				$result = JFactory::getDbo()->insertObject('#__footballclub_gallery', $insert);
    				unset($result);
    			}catch (Exception $e) {
    				return $e;
    			}
    		}
   		}
   		return $this->AlbumList;
    }  

    public function search_array($needle, $haystack)
    	{
    		if(in_array($needle, $haystack)) {
    			return true;
    		}
    		foreach($haystack as $element) {
    			if(is_array($element) && search_array($needle, $element))
    				return true;
    		}
    		return false;
    	}
        
        public function fqlPhoto($cover)
    	{
    		$jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
    		$thumb_query = 'SELECT images FROM photo WHERE pid = "' . $cover . '"';
    		$thumb = $jfbcLibrary->api( array(
    				'method' => 'fql.query',
    				'query' => $thumb_query,
    		));
    		return $thumb;
    	}

    public function getAlbumList()
    {
        if(empty($this->AlumbList))
        {
            $this->UpdateAlbums();    
        }
        $slice = array_slice($this->AlbumList, $this->_limitstart, $this->_limit, true);
    	return $slice;
    }
    
    public function getPagination()
    {
        // Load the content if it doesn't already exist
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
        }
        return $this->_pagination;
    }
    
    public function getTotal()
    {
        $this->getAlbumList();
        $this->_total = count($this->AlbumList);
        return $this->_total;
    }
}
