<?php
/**
 * @version     1.0.0
 * @package     com_footballclub
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */


// no direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_footballclub')) 
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

$controller	= JControllerLegacy::getInstance('Footballclub');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
