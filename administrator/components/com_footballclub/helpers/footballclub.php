<?php
/**
 * @version     1.0.0
 * @package     com_footballclub
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Footballclub helper.
 */
class FootballclubHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName = '', $active = 0)
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_FOOTBALLCLUB_TITLE_'.strtoupper($vName)),
			'index.php?option=com_footballclub&view='.ucfirst($vName),
			$active
		);

	}
    
    public static function addTitle( $vName = 'Menu' )
    {
        
    }

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_footballclub';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}
