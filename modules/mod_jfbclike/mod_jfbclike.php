<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.file');
$jfbcLibraryFile = JPATH_ROOT . '/components/com_jfbconnect/libraries/facebook.php';
if (!JFile::exists($jfbcLibraryFile))
{
    echo "JFBConnect not found. Please reinstall.";
    return;
}
require_once ($jfbcLibraryFile);

$fbClient = JFBConnectFacebookLibrary::getInstance();

require(JModuleHelper::getLayoutPath('mod_jfbclike'));

?>
