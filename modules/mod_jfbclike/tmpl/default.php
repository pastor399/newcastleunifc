<?php
/**
 * @package		JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');

$userIntro = $params->get('user_intro');
if ($userIntro != '') {
    echo '<div class="jfbclike_desc">'.$userIntro."</div>";
}	

$url = $params->get('url_to_like');
$href = '';
if ($url)
    $href = 'href='.$url;

$buttonStyle = $params->get('layout_style');
$showFaces = $params->get('show_faces');
$showSendButton = $params->get('show_send_button');
$width = $params->get('width');
$verbToDisplay = $params->get('verb_to_display');
$font = $params->get('font');
$colorScheme = $params->get('color_scheme');
$showFacesValue = ($showFaces?'true':'false');
$showSendButtonValue = ($showSendButton?'true':'false');
$ref = $params->get('ref');

$renderKey = $fbClient->getSocialTagRenderKey();
$renderKeyString = " key=".$renderKey;

?>
<div style="position: relative; top:0px; left:0px; z-index: 99;" class="jfbclike">
    <?php echo '{JFBCLike '.$href.
                    ' layout='.$buttonStyle.
                    ' show_faces='.$showFacesValue.
                    ' show_send_button='.$showSendButtonValue.
                    ' width='.$width.
                    ' action='.$verbToDisplay.
                    ' font='.$font.
                    ' colorScheme='.$colorScheme.
                    ' ref='.$ref.
                    $renderKeyString.
                '}';?>
</div>
<?php
require(JPATH_ROOT.'/components/com_jfbconnect/assets/poweredBy.php');
?>