<?php
defined('_JEXEC') or die('Restricted access');

$url = (JURI::root() . "index.php?option=com_footballclub&view=ovals&format=raw");

echo ('<div id="ovals-container"><div id="ovals"></div></div>');
?>

<script>
    jQuery(document).ready(function(){
       jQuery.get('<?php echo $url ?>', function(data){
        jQuery('div#ovals').hide().delay('100').html(data).fadeIn('slow');
       }); 
       
//       jQuery('#ovals').click(function() {
//            jQuery.get('<?php echo $url ?>', function(data){
//            jQuery('div#ovals').html("").delay('100').html(data).fadeIn('slow');
//            });
//        });
       
    });
</script>