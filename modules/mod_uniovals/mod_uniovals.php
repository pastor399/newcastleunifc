<?php

	// no direct access
	defined('_JEXEC') or die;
	
	$doc =& JFactory::getDocument();
	$doc->addStyleSheet( 'modules/mod_uniovals/tmpl/default.css' );
	
	// Include The Layout and Data
	require_once dirname(__FILE__).'/tmpl/default.php';

?>