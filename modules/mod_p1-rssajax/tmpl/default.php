<?php $helper_path = '"' . JURI::root() . 'index.php?option=com_lendr&view=rssdata&format=raw"'; ?> 

<div id="refresh">Refresh</div>
<div id="rssOutput"><span id="refresh">Refresh</span>RSS-feed will be listed here...</div>

<script>

jQuery(document).ready(showRSS());

function showRSS()
{
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			rss_news = xmlhttp.responseText;
			document.getElementById("rssOutput").innerHTML=rss_news;
		}
	}
	xmlhttp.open("GET",<?php echo $helper_path; ?>,true);
	xmlhttp.send();
}

jQuery("#refresh").click( function(){ 
	document.getElementById("rssOutput").innerHTML="Refreshing RSS Feeds!";
	showRSS();
});

</script>