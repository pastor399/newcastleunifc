<?php
defined('_JEXEC') or die('Restricted access');

class sponsorHelper {

   function getSponsors()
   {
        $db =& JFactory::getDBO();
        $query = "SELECT id, name, image_mini, website 
                  FROM #__footballclub_sponsors 
                  WHERE state = '1' AND image_mini<>''
                  ORDER BY RAND()
                 ";
        $db->setQuery($query);
        $rows = $db->loadObjectList();

        return $rows;
   }
}