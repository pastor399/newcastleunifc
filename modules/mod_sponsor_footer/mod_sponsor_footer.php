<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

    require(dirname(__FILE__).DS.'helper.php');
    
    $sponsors = sponsorHelper::getSponsors();
    
    require(JModuleHelper::getLayoutPath('mod_sponsor_footer'));
?>
