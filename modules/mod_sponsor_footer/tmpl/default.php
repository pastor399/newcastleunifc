<?php
defined('_JEXEC') or die('Restricted access');

include(dirname(__FILE__).DS.'default.css');
?>

<div class="footer_sponsors">
    <table width="100%">
        <tr>
    <?php foreach($sponsors as $sponsor)
          {
            echo '<td>';
            echo '<a href="'.$sponsor->website.'" target="blank" title="'.$sponsor->name.'" ><img class="footer_sponsor" src="'.$sponsor->image_mini.'" /></a>';
            echo '</td>';
          }
    ?>
        </tr>
    </table>
</div>