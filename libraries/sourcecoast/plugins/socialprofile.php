<?php
/**
 * @package        JLinked
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');
jimport('joomla.application.component.controller');
jimport('joomla.filesystem.file');
jimport('sourcecoast.utilities');

class SocialProfilePlugin extends JPlugin
{
    var $defaultSettings;
    var $settings;
    var $_componentFile = '';
    var $_componentFolder = '';
    var $network;
    var $profileLibrary;
    var $joomlaId;
    var $socialId;

    var $profileName;
    var $db;
    var $_importEnabled = false; // Can this plugin import previous FB connections
    private $componentLoaded = null;

    var $_settingShowRegistrationFields = null;
    var $_settingShowImportedFields = null;

    function __construct(&$subject, $params)
    {
        $this->profileName = $params['name'];
        $this->db = JFactory::getDBO();
        $this->defaultSettings = new JRegistry();

        // Don't even fully construct this plugin if the component isn't found
        if ($this->componentLoaded())
            parent::__construct($subject, $params);
    }


    public function socialProfilesGetPlugins()
    {
        return $this;
    }

    /**
     * Called after registration occurs
     * Good for importing the profile on first registration
     */
    public function socialProfilesOnRegister($network, $joomlaId, $socialId)
    {
        $this->loadSettings($network, $joomlaId, $socialId);
        $this->onRegister();

        return true;
    }

    /**
     * OnRegister
     * Will call (optional) functions in the profile plugin to create the user and log them in for the first time
     *
     * @param $joomlaId
     * @param $fbUserId
     */
    protected function onRegister()
    {
        if (empty($this->joomlaId) || empty($this->socialId))
            return; // Something's wrong, we should have both of these

        $profileData = $this->fetchProfileFromFieldMap(true);

        // Create the new user in the 3rd party extension with profile information imported or input on the registration page
        $this->createUser($profileData);

        if ($this->settings->get('import_avatar'))
            $this->importSocialAvatar();

        if ($this->settings->get('import_status'))
            $this->importSocialStatus();
    }

    /**
     * Called after registration occurs
     * Good for importing the profile on first registration
     */
    function socialProfilesOnLogin($network, $joomlaId, $socialId)
    {
        $this->loadSettings($network, $joomlaId, $socialId);
        $this->onLogin();

        return true;
    }

    protected function onLogin()
    {
        if (empty($this->joomlaId) || empty($this->socialId))
            return; // Something's wrong, we should have both of these

        if ($this->settings->get('import_always'))
        {
            $this->importSocialProfile(); // This will import fields from the user's profile

            if ($this->settings->get('import_avatar'))
                $this->importSocialAvatar();

            if ($this->settings->get('import_status'))
                $this->importSocialStatus();
        }
    }

    /*
     * Called on user request or other times like mapping
     */
    public function socialProfilesOnImportProfile($network, $joomlaId, $socialId)
    {
        $this->loadSettings($network, $joomlaId, $socialId);
        if (empty($this->joomlaId) || empty($this->socialId))
            return; // Something's wrong, we should have both of these

        if ($this->getProfileImportPermission())
        {
            $this->importSocialProfile(); // This will import fields from the user's profile

            if ($this->settings->get('import_avatar'))
                $this->importSocialAvatar();

            if ($this->settings->get('import_status'))
                $this->importSocialStatus();
        }
    }

    public function socialProfilesGetRequiredScope($network)
    {
        $this->loadSettings($network);
        $scope = array();
        $fieldMap = $this->settings->get('field_map');
        $scope = array_merge($scope, $this->profileLibrary->getPermissionsForFields($fieldMap));
        if ($this->network == 'facebook')
        {
            // If the "import_status" setting is used, and enabled, also request the user_status perm from FB
            if ($this->settings->get('import_status', 0))
                $scope[] = "user_status";

        }
        return $scope;
    }

    /**
     * Profile plugin (or integration component) will send the new user emails.
     * In this case, the JFBConnect/JLinked/etc will not send the admin/user emails
     * @return bool
     */
    public function socialProfilesSendsNewUserEmails()
    {
        return false;
    }

    /**
     * Profile will add its form validation script. If no custom validation is required,
     * default validation will be performed
     * @return bool
     */
    public function socialProfilesAddFormValidation()
    {
        return false;
    }


    protected function importSocialStatus()
    {
        $socialStatus = $this->profileLibrary->fetchStatus();
        if (!empty($socialStatus))
            $this->setStatus($socialStatus);
    }

    public function getConfigurationTemplate($network)
    {
        $this->loadSettings($network);

        $file = JPATH_SITE . '/plugins/socialprofiles/' . $this->profileName . '/' . $this->profileName . '/tmpl/configuration.php';

        if (!JFile::exists($file))
            return "No configuration is required for this profile plugin";

        $this->profileFields = $this->getProfileFields();

        // Fetch in the template file included below
        if ($network == 'facebook')
            $socialNetworkProfileFields = JFBConnectProfileLibrary::$profileFields;
        else if ($network == 'linkedin')
            $socialNetworkProfileFields = JLinkedProfileLibrary::$profileFields;
        ob_start();
        include_once($file);
        $config = ob_get_clean();
        return $config;
    }

    function getName()
    {
        return $this->profileName;
    }

    /**
     * Get field names and inputs to request additional information from users on registration
     * @return string HTML of form fields to display to user on registration
     */
    public function socialProfilesOnShowRegisterForm($network)
    {
        $this->loadSettings($network);
        $profileData = $this->fetchProfileFromFieldMap(false);
        $html = $this->getRegistrationForm($profileData);
        return $html;
    }

    protected function getRegistrationForm($profileData)
    {
        $showRegistrationFields = $this->settings->get('registration_show_fields');
        $showImportedFields = $this->settings->get('imported_show_fields');

        $html = "";

        $profileFields = $this->getProfileFields();

        foreach ($profileFields as $profileFieldName => $profileFieldLabel)
        {
            $fieldName = $this->settings->get('field_map.' . $profileFieldName, 0);
            $showField = $showRegistrationFields == "1" &&
                    ($showImportedFields == "1" || ($showImportedFields == "0" && !$fieldName));

            if (!$showField)
            {
                if ($fieldName != '0')
                    $this->set('performsSilentImport', 1);
                continue;
            }

            $fieldValue = $profileData->getFieldWithUserState($profileFieldName);

            $html .= '<label for="' . $profileFieldName . '">' . $profileFieldLabel . '</label>';
            $html .= '<input type="text" name="' . $profileFieldName . '" id="' . $profileFieldName . '" value="' . $fieldValue . '" /><br/>';
        }

        return $html;
    }

    /*
     * Fetches the user profile from the social network based on the field mapping settings
     * Then, merges the data with any POSTed fields from a possible registration submission
     *
     * Social network profile is saved to the session on the first time to avoid fetching it on every submission or after successful registration
     * @return JRegistry with profile data
     */
    protected function fetchProfileFromFieldMap($permissionNeeded = false)
    {
        $fieldMap = $this->settings->get('field_map');
        $app = JFactory::getApplication();

        $sessionKey = 'plg_socialprofiles.' . $this->network . '.' . $this->getName();
//        $sessionProfile = $app->getUserState($sessionKey, null);
        // TODO: Enable the get from session above, but ensure that it's cleared when Login with FB is clicked again
        $sessionProfile = null;
        if ($sessionProfile)
        {
            // Bit of hoops here because we can't save the original xyzProfileData class to the session, only a JRegistry value
            if ($this->network == 'facebook')
                $socialProfile = new JFBConnectProfileData($sessionProfile);
            // TODO: Add JLinked..
            $socialProfile->setFieldMap($this->settings->get('field_map'));
        } else // fetch the profile and save it to the session
        {
            if ($permissionNeeded)
                $permissionGranted = $this->getProfileImportPermission();
            else
                $permissionGranted = true;
            $socialProfile = $this->profileLibrary->fetchProfileFromFieldMap($fieldMap, $permissionGranted);
            $app->setUserState($sessionKey, $socialProfile->toString());
        }
        return $socialProfile;
    }

    /**
     * Used for plugins to check any credentials or information as necessary
     * Return true if login should proceed, false if not
     */
    public function socialProfilesOnAuthenticate($network, $joomlaId, $socialId)
    {
        $this->loadSettings($network, $joomlaId, $socialId);

        return $this->onAuthenticate();
    }

    protected function onAuthenticate()
    {
        $response = new profileResponse();
        $response->status = true;
        return $response;
    }

    /**
     * Determine if the Login Register view needs to give user the option to approve profile import
     * Required for LinkedIn, which requires explicit permission to import the users profile
     * @return bool if permission is needed, false if not
     */
    public function socialProfilesNeedsImportPermission($network)
    {
        if ($network == 'facebook')
            return false;
        else if ($network == 'linkedin')
            return $this->get('performsSilentImport', false);
    }

    /*     * *
     *
     * ************ END Triggered functions ************
     *
     * ** */

    /*     * *
     * ************ Direct call functions **************
     */


    /*     * ***
     * ************* END Direct call functions ********8
     */

    // These functions should be overridden by the plugins
    protected function getProfileFields()
    {
        return array();
    }

    protected function getProfileImportPermission()
    {
        if ($this->network == 'facebook')
            return true;
        else if ($this->network == 'linkedin')
        {
            return JRequest::getBool('socialProfileImportPermission', false);
        }
    }

    protected function importSocialProfile()
    {
        $fieldMap = $this->settings->get('field_map');
        $socialProfile = $this->fetchProfileFromFieldMap(true);
        if ($socialProfile)
        {
            foreach ($fieldMap as $fieldId => $socialField)
            {
                // Fetch the value from the POST data (if present) or the imported data from the Social Network
                $value = $socialProfile->getFieldWithUserState($fieldId);

                if ($value != null && $value != "")
                {
                    if (is_array($value))
                    { // This is a field with multiple, comma separated values
                        // Remove empty values to prevent blah, , blah as output
                        unset($value['id']); // Remove id key which is useless to import
                        $value = SCStringUtilities::r_implode(', ', $value);
                    }
                    $this->saveProfileField($fieldId, $value);
                }
            }
        }
    }

    protected function saveProfileField($fieldId, $value)
    {
        return true;
    }

    protected function importSocialAvatar()
    {
        jimport('joomla.filesystem.file');
        jimport('joomla.utilities.utility');

        $avatarURL = $this->profileLibrary->getAvatarURL($this->socialId, true);
        if ($avatarURL == null)
        {
            $this->setDefaultAvatar($this->joomlaId);
            return false;
        }

        $data = SCSocialUtilities::getRemoteContent($avatarURL);
        if ($data)
        {
            $baseImgPath = $this->getAvatarPath();
            $tmpImgName = 'scprofile_' . $this->joomlaId . '_pic_tmp.jpg';
            JFile::write($baseImgPath . '/' . $tmpImgName, $data);
            if ($this->setAvatar($tmpImgName))
                return true;
        }

        # there was a problem adding the avatar, use the default
        $this->setDefaultAvatar();
        return false;

    }

    protected function getAvatarPath()
    {
        $app = JFactory::getApplication();
        $tmpPath = $app->getCfg('tmp_path');
        return $tmpPath;
    }

    protected function setDefaultAvatar()
    {
        return true;
    }

    public function canImportConnections()
    {
        return $this->_importEnabled;
    }

    protected function componentLoaded()
    {
        if ($this->componentLoaded === null)
        {
            $this->componentLoaded = true;

            if ($this->_componentFile != '')
            {
                jimport('joomla.filesystem.file');
                $this->componentLoaded = JFile::exists($this->_componentFolder . '/' . $this->_componentFile);
            } else if ($this->_componentFolder != '')
            {
                jimport('joomla.filesystem.folder');
                $this->componentLoaded = JFolder::exists($this->_componentFolder);
            }
        }

        return $this->componentLoaded;
    }

    protected function loadSettings($network, $joomlaId = null, $socialId = null)
    {
        $this->joomlaId = $joomlaId;
        $this->socialId = $socialId;

        if ($network == $this->network)
            return $this->settings;

        $this->network = $network;
        if ($network == "facebook")
        {
            // Load the configuration settings from JFBConnect
            $query = $this->db->getQuery(true);
            $query->select("value");
            $query->from("#__jfbconnect_config");
            $query->where('setting="profile_' . $this->getName() . '"');
            $this->db->setQuery($query);
            $values = $this->db->loadResult();
            if (!empty($values))
            {
                $this->settings = new JRegistry();
                $this->settings->loadString($values);
            } else
                $this->settings = $this->defaultSettings;

            // Set the JFBConnect Profile library handle
            $this->profileLibrary = JFBConnectProfileLibrary::getInstance();
        } else if ($network == "linkedin")
        {
            $query = $this->db->getQuery(true);
            $query->select("value");
            $query->from("#__jlinked_config");
            $query->where('setting="profile_' . $this->getName() . '"');
            $this->db->setQuery($query);
            $values = $this->db->loadResult();
            if (!empty($values))
            {
                $this->settings = new JRegistry();
                $this->settings->loadString($values);
            } else
                $this->settings = $this->defaultSettings;

            // Set the JFBConnect Profile library handle
            $this->profileLibrary = JLinkedProfileLibrary::getInstance();
        }

        return $this->settings;
    }
}

if (!class_exists('profileResponse'))
{
    class profileResponse
    {

        var $status;
        var $message;

    }
}