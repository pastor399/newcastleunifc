<?php
/**
 * @version     $Id$
 * @package     JSNExtension
 * @subpackage  TPLFramework
 * @author      JoomlaShine Team <support@joomlashine.com>
 * @copyright   Copyright (C) 2012 JoomlaShine.com. All Rights Reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.joomlashine.com
 * Technical Support:  Feedback - http://www.joomlashine.com/contact-us/get-support.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Load framework defines
require_once dirname(__FILE__) . '/defines.php';

// Load class loader
require_once JSN_PATH_TPLFRAMEWORK . '/libraries/joomlashine/loader.php';

/**
 * Implement joomla events for template framework
 *
 * @package     TPLFramework
 * @subpackage  Plugin
 * @since       1.0.0
 */
class PlgSystemJSNTPLFramework extends JPlugin
{
	/**
	 * Joomla application instance
	 * @var JApplication
	 */
	protected $app;

	/**
	 * Template administrator object
	 *
	 * @var JSNTplTemplateAdmin
	 */
	private $_templateAdmin;

	/**
	 * Implement onAfterInitialise event
	 *
	 * @return  void
	 */
	public function onAfterInitialise ()
	{
		$this->app = JFactory::getApplication();
		$this->loadLanguage();

		// Register extension uninstall process hook
		$this->app->registerEvent('onExtensionAfterUninstall', 'PlgSystemJSNTPLFramework');

		// Stop system execution if a widget action is dispatched
		if (JSNTplWidget::dispatch() === true)
			jexit();
	}

	/**
	 * Save active form context to memory when editing an template
	 *
	 * @param   object  $context  Current context of template form
	 * @param   object  $data     Data of the form
	 * @return  void
	 */
	public function onContentPrepareForm ($context, $data)
	{
		if ($context->getName() == 'com_templates.style' && !empty($data))
		{
			$templateName		= is_object($data) ? $data->template : $data['template'];
			$templateManifest	= JSNTplHelper::getManifest($templateName);
			$templateGroup		= isset($templateManifest->group) ? trim((string) $templateManifest->group) : '';

			// Create template admin instance
			if (is_object($context) && $templateGroup == 'jsntemplate') {
				$this->_templateAdmin = JSNTplTemplateAdmin::getInstance($context);
			}
		}
	}

	/**
	 * Implement onBeforeRender event to register all needed asset files
	 *
	 * @return  void
	 */
	public function onBeforeRender ()
	{
		if ($this->_templateAdmin instanceOf JSNTplTemplateAdmin) {
			$this->_templateAdmin->registerAssets();
		}
	}

	/**
	 * Render template admin UI
	 *
	 * @return  void
	 */
	public function onAfterRender ()
	{
		if ($this->_templateAdmin instanceOf JSNTplTemplateAdmin) {
			$this->_templateAdmin->render();
		}

		if ($this->app->isSite())
		{
			$document = JFactory::getDocument();

			if (isset($document->helper) && $document->helper instanceOf JSNTplTemplateHelper && $document->compression > 0)
			{
				$body = JResponse::getBody();

				// Start compress CSS
				if ($document->compression == 1 || $document->compression == 2) {
					$body = preg_replace_callback('/(<link([^>]+)rel=["|\']stylesheet["|\']([^>]*)>\s*)+/i', array('JSNTplCompressCss', 'compress'), $body);
				}

				// Start compress JS
				if ($document->compression == 1 || $document->compression == 3) {
					$body = preg_replace_callback('/(<script([^>]+)src=["|\']([^"|\']+)["|\']([^>]*)>\s*<\/script>\s*)+/i', array('JSNTplCompressJs', 'compress'), $body);
				}

				JResponse::setBody($body);
			}
		}
	}

	/**
	 * Implement onExtensionAfterSave event to save template configuration params
	 *
	 * @param   string  $task  Extension executed task
	 * @param   mixed   $data  Data of task after executed
	 *
	 * @return  void
	 */
	public function onExtensionAfterSave ($task, $data)
	{
		if ($task != 'com_templates.style') {
			return;
		}

		$options = isset($_POST['jsn']) ? $_POST['jsn'] : array();

		// Auto strip slashes if magic_quote_gpc is on
		if (get_magic_quotes_runtime() || get_magic_quotes_gpc()) {
			$options = array_map('stripslashes', $options);
		}

		if (!empty($options)) {
			$data->params = json_encode($options);
			$data->store();
		}
	}

	/**
	 * Implement onExtensionAfterUninstall event to remove
	 * the template framework
	 *
	 * @return  void
	 */
	public static function onExtensionAfterUninstall ()
	{
		// Find all template installed in joomla system
		// column "custom_data" contains jsntemplate
		$dbo = JFactory::getDBO();
		$query = $dbo->getQuery(true);
		$query->select('COUNT(*)')
			  ->from('#__extensions')
			  ->where('type LIKE "template" AND custom_data LIKE "jsntemplate"');

		$dbo->setQuery($query);
		$hasTemplates = $dbo->loadResult();

		if ($hasTemplates == 0) {
			JFactory::getLanguage()->load('com_installer');

			// Find id of framework plugin
			$query = $dbo->getQuery(true);
			$query->select('extension_id')
				  ->from('#__extensions')
				  ->where('element LIKE "jsntplframework"');

			$dbo->setQuery($query);
			$pluginId = $dbo->loadResult();

			if (!empty($pluginId)) {
				$executeMethod = method_exists($dbo, 'query') ? 'query' : 'execute';

				$dbo->setQuery("UPDATE #__extensions SET protected=0 WHERE extension_id={$pluginId}");
				$dbo->{$executeMethod}();

				$installer = JInstaller::getInstance();
				$isUninstalled = $installer->uninstall('plugin', $pluginId);

				if ($isUninstalled) {
					JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_INSTALLER_UNINSTALL_SUCCESS', 'plugin'));
				}
			}
		}
	}
}
