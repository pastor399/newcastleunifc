<?php

/**
 * @version     1.0.0
 * @package     com_fbnews
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */
// No direct access
defined('_JEXEC') or die;


class FbnewsViewSubmit extends JViewLegacy {
	
	function display($tpl = null)
	{
		$input = new JInput;
		$this->text = $input->get('news', '', 'post');
		$this->image = $input->get('image', '', 'post');
		parent::display($tpl);
	}	
}