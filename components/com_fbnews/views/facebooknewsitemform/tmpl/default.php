<?php
/**
 * @version     1.0.0
 * @package     com_fbnews
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_fbnews', JPATH_ADMINISTRATOR);
?>

<script type="text/javascript">
    jQuery(document).ready(function(){
    	jQuery('#form-facebooknewsitem').submit(function(){
             
        });
        
    	jQuery('#fb-submit').click(function(){
        	text = jQuery("#jform_message").val();
        	img = jQuery('#jform_image').val();
    		var request = jQuery.ajax({
    			url: "index.php?option=com_fbnews&view=submit&format=raw",
    			type: "POST",
    			data: {news : text, image: img},
    			dataType: "html"
    			});
    			request.done(function( msg ) {
    			jQuery("div#fbnews-success").hide().css("color","green").text( "Success" );
    			jQuery("div#fbnews-success").fadeIn();
    			jQuery("input#jform_fbid").val( msg );
//     			alert( msg );
    			});
    			request.fail(function(jqXHR, textStatus) {
    			jQuery("div#fbnews-success").hide().css("color","red").html( "Request failed: " + textStatus );
    			jQuery("div#fbnews-success").fadeIn();
    			});
    	});

    });
        

</script>

<div class="facebooknewsitem-edit front-end-edit">
<?php if (!empty($this->item->id)): ?>
        <h1>Edit <?php echo $this->item->id; ?></h1>
    <?php else: ?>
        <h1>Add</h1>
    <?php endif; ?>

    <form id="form-facebooknewsitem" action="<?php echo JRoute::_('index.php?option=com_fbnews&task=facebooknewsitem.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
        <ul>
            <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('id'); ?></div>
			</div>

			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('title'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('title'); ?></div>
			</div>
			<div class="control-group message-control">
				<div class="control-label"><?php echo $this->form->getLabel('message'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('message'); ?></div>
			</div>
			<div class="control-group image-control">
				<div class="control-label"><?php echo $this->form->getLabel('image'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('image'); ?></div>
			</div>
			<div id="fb-news-create">
				 <div>
				 	<span id="fb-submit" class="modal btn" style="float:left">Post To Facebook</span>
				 	<div id="fbnews-success" class="success" style="float:left">Success</div>
				 	<div style="clear:both"></div>
				 </div>
			</div>
			<div class="control-group">
				<?php $canState = false; ?>
					<?php $canState = $canState = JFactory::getUser()->authorise('core.edit.state','com_fbnews'); ?>				<?php if(!$canState): ?>
				<div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
					<?php
						$state_string = 'Unpublish';
						$state_value = 0;
						if($this->item->state == 1):
							$state_string = 'Publish';
							$state_value = 1;
						endif;
					?>
					<div class="controls"><?php echo $state_string; ?></div>
					<input type="hidden" name="jform[state]" value="<?php echo $state_value; ?>" />
				<?php else: ?>
					<div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('state'); ?></div>					<?php endif; ?>
				</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('created_by'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('created_by'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('fbid'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('fbid'); ?></div>
			</div>

        </ul>

        <div>
            <button type="submit" class="validate"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
<?php echo JText::_('or'); ?>
            <a href="<?php echo JRoute::_('index.php?option=com_fbnews&task=facebooknewsitem.cancel'); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>

            <input type="hidden" name="option" value="com_fbnews" />
            <input type="hidden" name="task" value="facebooknewsitemform.save" />
<?php echo JHtml::_('form.token'); ?>
        </div>
    </form>
</div>
