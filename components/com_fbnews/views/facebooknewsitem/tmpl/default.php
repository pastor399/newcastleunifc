<?php
/**
 * @version     1.0.0
 * @package     com_fbnews
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_fbnews', JPATH_ADMINISTRATOR);
?>
<?php if( $this->item ) : ?>

    <div class="item_fields">
        
        <ul class="fields_list">

			<li><?php echo JText::_('COM_FBNEWS_FORM_LBL_FACEBOOKNEWSITEM_ID'); ?>:
			<?php echo $this->item->id; ?></li>
			<li><?php echo JText::_('COM_FBNEWS_FORM_LBL_FACEBOOKNEWSITEM_ORDERING'); ?>:
			<?php echo $this->item->ordering; ?></li>
			<li><?php echo JText::_('COM_FBNEWS_FORM_LBL_FACEBOOKNEWSITEM_STATE'); ?>:
			<?php echo $this->item->state; ?></li>
			<li><?php echo JText::_('COM_FBNEWS_FORM_LBL_FACEBOOKNEWSITEM_CHECKED_OUT'); ?>:
			<?php echo $this->item->checked_out; ?></li>
			<li><?php echo JText::_('COM_FBNEWS_FORM_LBL_FACEBOOKNEWSITEM_CHECKED_OUT_TIME'); ?>:
			<?php echo $this->item->checked_out_time; ?></li>
			<li><?php echo JText::_('COM_FBNEWS_FORM_LBL_FACEBOOKNEWSITEM_CREATED_BY'); ?>:
			<?php echo $this->item->created_by; ?></li>
			<li><?php echo JText::_('COM_FBNEWS_FORM_LBL_FACEBOOKNEWSITEM_TITLE'); ?>:
			<?php echo $this->item->title; ?></li>
			<li><?php echo JText::_('COM_FBNEWS_FORM_LBL_FACEBOOKNEWSITEM_FBID'); ?>:
			<?php echo $this->item->fbid; ?></li>
			<li><?php echo JText::_('COM_FBNEWS_FORM_LBL_FACEBOOKNEWSITEM_IMAGE'); ?>:
			<?php echo $this->item->image; ?></li>


        </ul>
        
    </div>
    <?php if(JFactory::getUser()->authorise('core.edit.own', 'com_fbnews')): ?>
		<a href="<?php echo JRoute::_('index.php?option=com_fbnews&task=facebooknewsitem.edit&id='.$this->item->id); ?>">Edit</a>
	<?php endif; ?>
								<?php if(JFactory::getUser()->authorise('core.delete','com_fbnews')):
								?>
									<a href="javascript:document.getElementById('form-facebooknewsitem-delete-<?php echo $this->item->id ?>').submit()">Delete</a>
									<form id="form-facebooknewsitem-delete-<?php echo $this->item->id; ?>" style="display:inline" action="<?php echo JRoute::_('index.php?option=com_fbnews&task=facebooknewsitem.remove'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
										<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
										<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
										<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
										<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
										<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
										<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />
										<input type="hidden" name="jform[title]" value="<?php echo $this->item->title; ?>" />
										<input type="hidden" name="jform[fbid]" value="<?php echo $this->item->fbid; ?>" />
										<input type="hidden" name="jform[image]" value="<?php echo $this->item->image; ?>" />
										<input type="hidden" name="option" value="com_fbnews" />
										<input type="hidden" name="task" value="facebooknewsitem.remove" />
										<?php echo JHtml::_('form.token'); ?>
									</form>
								<?php
								endif;
							?>
<?php else: ?>
    Could not load the item
<?php endif; ?>
