<?php // no direct access

defined( '_JEXEC' ) or die( 'Restricted access' ); 
 
class LendrModelsRssdata extends JModelBase
{
  protected $channel_count;
  protected $channels;
  protected $channel_titles;
  protected $rawdata;
  protected $rssdata;
 
	public function listChannels()
	{
		$params = &JComponentHelper::getParams( 'COM_LENDR' );
		$feeds = explode(';', $params->get( 'xml_feedlist' ));
		$this->channel_count = 0;
		foreach($feeds as $feed)
		{
			$this->channels[$this->channel_count] = (string)$feed;
			$this->channel_count++;
		}
	}  
  
	public function channelArray()
	{
		for($i=0;$i<$this->channel_count;$i++)
		{
			$this->channel_titles[$i] = (string)$this->rawdata[$i]->channel->title;
		}
	} 

	public function getRawdata()
	{
		for($i=0; $i<$this->channel_count; $i++)
		{
			$xmlDoc = new DOMDocument();
			$xmlDoc->load($this->channels[$i]);
			$xpath = new DOMXPath($xmlDoc);
		
			$xml = simplexml_load_file(trim($this->channels[$i]));
			$this->rawdata[$i] = $xml;
		}
	}
  
	public function passChannel($id)
	{
		$raw = $this->rawdata[$id]->channel;		
		return $raw;	
	} 
	
	public function getRssdata()
	{
		$this->listChannels();
		
		$this->getRawdata();
		
		$this->channelArray();
		
		for($i=0;$i<$this->channel_count;$i++)
		{
			$this->rssdata[$i]['title'] = $this->channel_titles[$i];
			$this->rssdata[$i]['items'] = $this->passChannel($i);
		}
		$this->rssdata[0]['title'] = "Football Federation Australia";
		return $this->rssdata;
		//return $this->channels; 
	}
}
