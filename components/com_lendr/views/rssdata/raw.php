<?php defined( '_JEXEC' ) or die( 'Restricted access' ); 
 
class LendrViewsRssdataRaw extends JViewHtml
{
  function render()
  {  

  	$params = &JComponentHelper::getParams( 'COM_LENDR' );
  	$this->item_limit = explode(';', $params->get( 'feed_itemlimit' ))[0];
  	
  	$data = json_decode(json_encode($this->model->getRssdata()),true);
  	//print_r($data);
  	
	foreach($data as $channel)
	{
//	   print_r ($channel);
		echo "<h3>".$channel['title']."</h3>";
		echo "<ul>";
		$count = 0;
		foreach($channel['items']['item'] as $item)
		{
			if($count>=$this->item_limit){ break; }
			echo "<li class='rss-item'><div><a class='item-title' href='".$item['link']."'>".$item['title']."</a>".
											"<span class='item-time'>".$item['pubDate']."</span>".
									  "</div>".
					  				  "<div>".$item['description']."</div></li>";
			$count++;
		}
		echo "</ul>";
	}
  }
}