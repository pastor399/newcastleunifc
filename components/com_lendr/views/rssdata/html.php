<?php defined( '_JEXEC' ) or die( 'Restricted access' ); 
 
class LendrViewsRssdataHtml extends JViewHtml
{
  function render()
  {
  	$data = $this->model->getRssdata();
  	
	echo(json_encode($data));
  }
}