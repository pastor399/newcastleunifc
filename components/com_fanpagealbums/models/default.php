<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla modelitem library
jimport('joomla.application.component.modelitem');
 
class FanpageAlbumsModelDefault extends JModelItem
{

    protected $AlbumList;
    protected $DisplayParams;
    
    public function getDisplayParams()
    {
        $app = JFactory::getApplication();
        
        $comParams = JComponentHelper::getParams('com_fanpagealbums');
        $params = $app->getParams();
        
        $param = new stdClass();
        $param->PageID = $params->get('pageid');
        $param->Columns = $params->get('columns');
        $param->Rows = $params->get('rows');
        $param->Type = $comParams->get('libType');
        $param->AppId = $comParams->get('appId');
        $param->AppSecret = $comParams->get('appSecret');
        $this->DisplayParams = $param;
        return $this->DisplayParams;
    }
    
    public function UpdateAlbums()
    {
    	// THESE ARE HELPER FUNCTIONS REQUIRED BELOW //
    	function search_array($needle, $haystack)
    	{
    		if(in_array($needle, $haystack)) {
    			return true;
    		}
    		foreach($haystack as $element) {
    			if(is_array($element) && search_array($needle, $element))
    				return true;
    		}
    		return false;
    	}
    	function fqlPhoto($cover)
    	{
    		$jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
    		$thumb_query = 'SELECT images FROM photo WHERE pid = "' . $cover . '"';
    		$thumb = $jfbcLibrary->api( array(
    				'method' => 'fql.query',
    				'query' => $thumb_query,
    		));
    		return $thumb;
    	}

    	// END OF THE HELPER FUNCTIONS // 	
    	
    	//QUERY FACEBOOK FOR THE CURRENT LIST OF ALBUMS AND DETAILS
    	//GET THE ID OF PAGE TO QUERY
    	$pageID = $this->DisplayParams->PageID;
    	
    	//JFBConnect OR Facebook SDK
    	switch($this->DisplayParams->Type){
    		case 0:
    			$facebook = JFBConnectFacebookLibrary::getInstance();
    			break;
    		default:
    			require_once('components/com_fanpagealbums/FacebookSDK/facebook.php');
    			$config = array();
    			$config['appId'] = $this->DisplayParams->AppId;
    			$config['secret'] = $this->DisplayParams->AppSecret;
    			$config['fileUpload'] = false; // optional
    			$facebook = new Facebook($config);
    	}
    	
    	$query = "SELECT aid, name, cover_pid FROM album WHERE owner = " . $pageID .
    	" AND type = 'normal' AND name != 'Cover Photos' AND name != 'Profile Pictures'";
    	$fql_albumlist = $facebook->api( array(
    			'method' => 'fql.query',
    			'query' => $query, ));
    	
    	//STORE THE CURRENT ALBUM LIST IN THE VARIABLE
    	$this->AlbumList = $fql_albumlist;
    	
    	//Get The List Of PID's Saved In The Database.
    	$dbo = $this->getDbo();
    	$dbQuery = $dbo->getQuery(true);
    	$dbQuery->select('pid');
    	$dbQuery->from('#__fanpagealbums');
    	$dbo->setQuery($dbQuery);
    	$ext_pid = $dbo->loadObjectList();
    	//CONVERT THE OBJECT LIST TO AN ARRAY OF PID'S
    	$album_ids = array();
    	foreach($ext_pid as $album)
    	{
    		array_push($album_ids, $album->pid);
    	}
        $pid_array = $album_ids;
    	
    	//FOR EACH ALBUM, SEARCH FOR ITS COVER_PID IN THE DATABASE
    	foreach ($this->AlbumList as &$Album)
    	{
    		//IF COVER_PID EXISTS, ADD ITS DETAILS TO THE CURRENT ALBUM OBJECT
    		if (search_array($Album['cover_pid'], $pid_array) == true)
    		{
    			$dbo = $this->getDbo();
    			$dbQuery = $dbo->getQuery(true);
    			$dbQuery->select('src_small');
    			$dbQuery->from('#__fanpagealbums');
    			$dbQuery->where('pid="' . $Album['cover_pid'] . '"');
    			$dbo->setQuery($dbQuery);
    			$src_small = $dbo->loadResult();
    			$Album['src_small'] = $src_small;
    		}else{
    			$photo = fqlPhoto($Album['cover_pid']);
    			//CREATE A EMPTY OBJECT TO STORE PID DETAILS
    			$insert = new stdClass();
    			$insert->pid = $Album['cover_pid'];
    			$insert->album_name = $Album['name'];
    			$insert->src_small=$photo[0]['images'][6]['source'];
    			$insert->src_large=$photo[0]['images'][0]['source'];
    			$src_small = $insert->src_small;
    			try {
    				//INSERT NEW PID OBJECT INTO THE DATABASE
    				$result = JFactory::getDbo()->insertObject('#__fanpagealbums', $insert);
    				unset($result);
    			}catch (Exception $e) {
    				return $e;
    			}
    		}
   		}
   		return $this->AlbumList;
    }  

    public function getAlbumList()
    {
    	return $this->UpdateAlbums();
    }

}
