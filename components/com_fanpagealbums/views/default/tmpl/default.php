<?php
//NO DIRECT ACCESS ALLOWED
defined('_JEXEC') or die;

//print_r($this->AlbumList);
//print_r ($this->DisplayParams);

$Albums = $this->AlbumList;     //List Of Albums And There Details
$columns = $this->DisplayParams->Columns;       //Display Parameters - Table Columns
$rows = $this->DisplayParams->Rows;     //Display Parameters - Table Rows

$page = JRequest::getVar('page', 1);        //Which Page To Display?
$show = JRequest::getVar('show', false);    //Show All Albums On Page?

if($page < 1)       
    $page = 1;

$limit = $columns * $rows;
$nav_code = '';

if($show == 'all')
{
    $pages = ceil(count($Albums) / $limit);
    $nav_code .= ('<div id="fb_album_nav">');
    for ($i = 1; $i <= $pages; $i++)
        $nav_code .= ('<a class="btn" href=?page=' . $i . '><span>' . $i . '</span></a>');
    $nav_code .= ('<a class="btn btn-inverse" href=?show=all><span>All</span></a>');
    $nav_code .= ('</div>');
    $limit = count($Albums);
}else{
    $pages = ceil(count($Albums) / $limit);
    $nav_code .= ('<div id="fb_album_nav">');
    for ($i = 1; $i <= $pages; $i++)
    {
    	if($i == $page){ $col_code = "btn btn-inverse"; }else{ $col_code="btn";}
        $nav_code .= ('<a class="'. $col_code .'" href=?page=' . $i . '><span>' . $i . '</span></a>');
    }
    $nav_code .= ('<a class="btn" href=?show=all><span>All</span></a>');
    $nav_code .= ('</div>');
}


echo ("<div class='row-fluid show-grid'>");

$colEnd = 0;
$count_thumbs = 0;
$start = ($page - 1) * $limit;
$count = 0;
    foreach($Albums as $Album)
    {
        if($count >= $start)
        {
            if($count_thumbs < $limit)
            {
            	echo ("<article class='span3'>");
                	$src_small = $Album['src_small'];
                	echo("<a href='?option=com_fanpagealbums&view=album&aid=" . $Album['aid'] . "'><div class='fbL_album_thumb' style='background-image: url(" .
						$src_small. ")'> </div></a>");
                	echo("<a href='?option=com_fanpagealbums&view=album&aid=" . $Album['aid'] . "'><div class='album_title'>". $Album['name'] . "</div></a>");
                echo ("</article>");
                $count_thumbs++;
            }
        }else{
            $count++;
        }
    }
    
echo('</div>');
echo ($nav_code);
?>