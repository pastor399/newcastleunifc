<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the HelloWorld Component
 */
class FOOTBALLCLUBViewAlbum extends JViewLegacy
{
    // Overwriting JView display method
    function display($tpl = null) 
    {
            $document = JFactory::getDocument();

            // Send Album Id To The Model, From URL
                $model = & $this->getModel('Album');
                $model->setAlbumID(JRequest::getVar('aid'));

            // Get Data Back From The Model
                $this->DisplayParams = $this->get('DisplayParams');
                $this->AlbumID = $this->get('AlbumID');
                $this->AlbumName = $this->get('AlbumName');
                $this->PhotoList = $this->get('PhotoList');

            // Modify The Breadcrumb.
                $app	= JFactory::getApplication();
                $pathway = $app->getPathway();
                $pathway->addItem($this->AlbumName);
                
            // Display the view
                parent::display($tpl);
    }
}

?>
