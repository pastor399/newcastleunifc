jQuery(document).ready(function(){

	var  img_count = 0;
	var disp_img = 0 ;
	//alert(imageList.length);
	
	jQuery(document).keydown(function(e) 
			{
			        if (e.keyCode == 39) 
			        {  
			            img_count += 1;
			            if(img_count > imageList.length - 1){ img_count = 0; }
			            jQuery("img#img-container").trigger('click');
			        }
			        if(e.keyCode==37)
			        {
			            img_count -= 1;
			            if(img_count < 0){ img_count = imageList.length - 1; }
			            jQuery("img#img-container").trigger('click');
			        }

			});
	
	jQuery("img#img-container").click(function() {
	    jQuery("img#img-container").one("load", function() {
	    }).attr("src", imageList[img_count]['src_big']);
	});
	
	jQuery("img#img-container").trigger('click');
	
});
