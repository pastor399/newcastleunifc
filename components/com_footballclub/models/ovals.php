<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla modelitem library
jimport('joomla.application.component.modelitem');

require_once("simple_html_dom.php");
 
class FOOTBALLCLUBModelOvals extends JModelItem
{
    protected $target;
    protected $html_src;
    protected $ovals;
    protected $last_update;
    
    public function getDisplayParams()
    {
        $params = JComponentHelper::getParams('com_footballclub');
        $this->target = $params->get('url');
        return $this->target;
    }
		
	function getHtml_src()
	{
		$this->html_src = file_get_html($this->target);	
		return $this->html_src;
	}
	
	function getOvals()
	{
	    $src = $this->html_src;
		$replace = array('Ray Watt Oval'=>'Ray Watt Oval','University No 1 Oval' => 'University No 1', 'University No 2 Oval'=>'University No 2', 'University No 3 Oval'=> 'University No 3');
		$items = array();
		$test = "O";
			foreach($src->find("table#pricetable") as $table){
					for($i = 0; $i <= 3; $i++){ 
						$item['id'] = $replace[$table->find('td.feature, td.featureend',$i)->plaintext];
						$item['status'] = $table->find('td.plan-personal',$i)->plaintext;
						$item['long'] = $item['status'];
						$item['status'] = str_replace (" ", "", $item['status']);
						$item['status'] = substr($item['status'],0,1);
						if(strcmp($item['status'],$test) == 0){
							$item['status'] = 'Open';
						}else{
							$item['status'] = 'Closed';
							}
						$items[] = $item;
					}
			}
            $this->ovals = $items;
			return $this->ovals;
	}
	
	function getUpdateTime()
	{
	    $src = $this->html_src;
        if($src->find('conditions', 1)->plaintext == "")
           {
              return "Updated By NuSport";
           }else 
           {
                return $src->find('conditions', 1)->plaintext;
	       }
    }
}