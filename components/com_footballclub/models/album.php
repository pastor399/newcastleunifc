<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla modelitem library
jimport('joomla.application.component.modelitem');
 
class FOOTBALLCLUBModelAlbum extends JModelItem
{
    var $_total = null;
    var $_pagination = null;
    
    var $_limit = null;
    var $_limistart = null;
    
    protected $params;
    protected $AlbumID;
    protected $AlbumName;
    protected $Photos;
    protected $api;
    
    
    function __construct()
    {
        parent::__construct();
        
        $app = JFactory::getApplication();
        $this->params = JComponentHelper::getParams('com_footballclub');
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        
        $this->_limit = $limit;
        $this->_limitstart = $limitstart;
        
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
        
        $this->api = JFBConnectFacebookLibrary::getInstance();
        
        $this->AlbumID = JRequest::getVar('id');        
    }
    
    public function getAlbumID()
    {
        return $this->AlbumID;
    }
    
    function loadAlbumName()
    {
        $query = "SELECT name FROM album WHERE aid = '" . $this->AlbumID . "'";
        $result =  $this->api->api( array(
                                 'method' => 'fql.query',
                                 'query' => $query,
                         ));
        $this->AlbumName = $result[0]['name'];
    }
    
    public function getAlbumName()
    {   
        if($this->AlbumName == null){
            $this->loadAlbumName();
        }
        return $this->AlbumName;
    }
    
    public function loadPhotos()
    {
        $query = "SELECT pid, src, src_big, like_info, comment_info FROM photo WHERE aid = '" . $this->AlbumID . "'";
        $results =  $this->api->api( array(
                                 'method' => 'fql.query',
                                 'query' => $query, ));
         $this->Photos = $results;
    } 
       
    public function getPhotos()
    {
        if($this->Photos == null){
            $this->loadPhotos();
        }
         return $this->Photos;
    }
    
    public function getPagination()
    {
        // Load the content if it doesn't already exist
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
        }
        return $this->_pagination;
    }
    
    public function getTotal()
    {
        $this->getPhotos();
        $this->_total = count($this->Photos);
        return $this->_total;
    }
    
    public function getLimitBox()
    {
            $app = JFactory::getApplication();
            jimport('joomla.html.pagination');
                        
            // Initialise variables.
            $limits = array ();
     
            // Make the option list.
            $limits[] = JHtml::_('select.option', '8');
            $limits[] = JHtml::_('select.option', '12');
            $limits[] = JHtml::_('select.option', '16');
            $limits[] = JHtml::_('select.option', '20');
            $limits[] = JHtml::_('select.option', '24');
            $limits[] = JHtml::_('select.option', '0', 'ALL');
            
            $selected =  JRequest::getVar('limit', 20);
            //$selected = $this->_viewall ? 0 : $this->limit;
     
            // Build the select list.
            if ($app->isAdmin()) {
                    $html = JHtml::_('select.genericlist',  $limits, $this->prefix . 'limit', 'class="inputbox" size="1" onchange="submitform();"', 'value', 'text', $selected);
            }
            else {
                    $html = JHtml::_('select.genericlist',  $limits, $this->prefix . 'limit', 'class="inputbox" size="1" onchange="this.form.submit()"', 'value', 'text', $selected);
            }
            return $html;
    }

}