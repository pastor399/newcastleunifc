<?php
/**
 * @version     1.0.0
 * @package     com_footballclub
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */
 
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class FootballclubController extends JControllerLegacy
{

}