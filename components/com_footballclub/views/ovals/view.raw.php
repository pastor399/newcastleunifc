<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * RAW Output View The Serves Up The Oval Status Information As JSON
 */
class FOOTBALLCLUBViewOvals extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null) 
	{
        $document = JFactory::getDocument();
        
        //Get The List Of Albums And Cover_PID.
        $this->DispalyParams = $this->get('DisplayParams');
        $this->html_src = $this->get('Html_src');
        $this->ovals = $this->get('Ovals');
        $this->updatetime = $this->get('UpdateTime');

		// Display the view
		parent::display($tpl);
	}
}

?>
