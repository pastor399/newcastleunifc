<?php
	require_once("simple_html_dom.php");	

	$target = "http://www.theforum.org.au/ovals";
	$html_src;
	//$page_exists;
	$ovals = array();
	$last_update;
	
	$html_src = loadHTML($target);
	
	$this->$ovals = getOvals($html_src);
	$this->$update = getUpdateTime($html_src);
	
	function loadHTML($targ)
	{
		$html = file_get_html($targ);	
		return $html;
	}
	
	function getOvals($src)
	{
		$replace = array('Ray Watt Oval'=>'Ray Watt Oval','University No 1 Oval' => 'University No 1', 'University No 2 Oval'=>'University No 2', 'University No 3 Oval'=> 'University No 3');
		$items = array();
		$test = "O";
			foreach($src->find("table#pricetable") as $table){
					for($i = 0; $i <= 3; $i++){ 
						$item['id'] = $replace[$table->find('td.feature, td.featureend',$i)->plaintext];
						$item['status'] = $table->find('td.plan-personal',$i)->plaintext;
						$item['long'] = $item['status'];
						$item['status'] = str_replace (" ", "", $item['status']);
						$item['status'] = substr($item['status'],0,1);
						if(strcmp($item['status'],$test) == 0){
							$item['status'] = 'Open';
						}else{
							$item['status'] = 'Closed';
							}
						$items[] = $item;
					}
			}
			return $items;
	}
	
	function getUpdateTime($src)
	{
		return $src->find('conditions', 1)->plaintext;
	}
?>