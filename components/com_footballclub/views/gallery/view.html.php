<?php
/**
 * @version     1.0.5
 * @package     com_footballclub
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Footballclub.
 */
class FootballclubViewGallery extends JViewLegacy
{
	protected $pagination;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{

		$this->pagination	= $this->get('Pagination');
        $this->limitbox     = $this->get('LimitBox');
 
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			throw new Exception(implode("\n", $errors));
		}
               
        //Get The List Of Albums And Cover_PID.
        $this->AlbumList = $this->get('AlbumList');
        
        //print_r ($this->AlbumList);       
		
        parent::display($tpl);
        
        //print_r ($this);
	}


    
}

