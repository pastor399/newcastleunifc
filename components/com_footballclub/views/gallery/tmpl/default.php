<?php
/**
 * @version     1.0.5
 * @package     com_footballclub
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Andrew Pastor <pastor399@gmail.com> - www.part-one.net
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_footballclub/assets/css/footballclub.css');

$user	= JFactory::getUser();
$userId	= $user->get('id');

?>

<form action="<?php echo JRoute::_('index.php?option=com_footballclub&view=gallery'); ?>" method="post" name="adminForm" id="adminForm">
	<div id="j-main-container"> 
		<table class="table table-striped" id="album_list">
			<tfoot>
    			<tr>
    				<td id="pagination" colspan="<?php echo $colspan ?>">
    					<?php echo $this->pagination->getPagesLinks(); ?>
    				</td>
                </tr>
                <tr id="limit-box" >
                    <td colspan="<?php echo $colspan ?>">
                        <?php echo $this->limitbox; ?>
                    </td>
    			</tr>
			</tfoot>
            <tbody>
                <div class="row-fluid show-grid">
                    <?php //print_r($this->AlbumList); print_r ($this->pagination); ?>
                    <?php 
                    foreach($this->AlbumList as $album)
                    {
                        $link = JUri::current() . '?view=album&id=' . $album['aid'];
                        echo '<article class="span3">';
                        echo '<a href="'.$link.'" title="'.$album['name'].'">';
                        echo '<div class="gallery_thumb" style="background-image: url('. $album['src_small'] .'); "> </div>';
                        echo '<div class="album_title">'. $album['name'] . '</div>';
                        echo '</a>';
                        echo '</article>'; 
                    }
                    ?>
                </div>
            </tbody>
		</table>
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>        

		
