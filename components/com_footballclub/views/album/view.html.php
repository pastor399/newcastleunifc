<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * 
 */
class FOOTBALLCLUBViewAlbum extends JViewLegacy
{
    protected $pagination;
    
	// Overwriting JView display method
	function display($tpl = null) 
	{
	    $app = JFactory::getApplication();
        $document = JFactory::getDocument();
        
        $document->addStyleSheet(JURI::root().'components/com_footballclub/assets/shadowbox/shadowbox.css' );
        $document->addScript(JURI::root() . 'components/com_footballclub/assets/shadowbox/shadowbox.js' );
        
        $this->pagination	= $this->get('Pagination');
        $this->limitbox     = $this->get('LimitBox');
        
        // Check for errors.
		if (count($errors = $this->get('Errors'))) {
			throw new Exception(implode("\n", $errors));
		}
                
        $this->Name = $this->get('AlbumName');
        $this->Photos = $this->get('Photos');
        $this->Total = $this->get('Total');
        
        $pathway = $app->getPathway();
        $pathway->addItem($this->Name);
               
		// Display the view
		parent::display($tpl);
	}
}

?>
