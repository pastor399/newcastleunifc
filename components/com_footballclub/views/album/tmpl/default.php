<?php
//NO DIRECT ACCESS ALLOWED
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_footballclub/assets/css/footballclub.css');

?>

	<script type="text/javascript">
		Shadowbox.init();
	</script>
    
<form action="<?php echo JRoute::_('index.php?option=com_footballclub&view=album'); ?>" method="post" name="adminForm" id="adminForm">
	<div id="j-main-container"> 
        <table class="table table-striped" id="album_list">
            <tfoot>

            </tfoot>
            <tbody>
                <div class="row-fluid show-grid">
                    <?php
                    $i = 1;
                    foreach($this->Photos as $photo)
                    {
                        //$link = JUri::current() . '?view=album&id=' . $album['aid'];
                        echo '<article class="span3">';
                        echo '<a rel="shadowbox[My Gallery]" href="'.$photo['src_big'].'" title="'.$this->Name.'">';
                        echo '<div class="gallery_thumb" style="background-image: url('. $photo['src_big'].'); "> </div>';
                        
                        echo '<div class="album_title">';
                        echo '<div class="photo-like"></div>';
                        echo $i.'/'.$this->Total;
                        echo '<div class="photo-comment"></div>';
                        
                        echo '</div>';
                        echo '</a>';
                        echo '</article>';
                        $i++; 
                    }
                    ?>
                </div>
            </tbody>
        </table>
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>