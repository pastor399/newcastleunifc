<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');
jimport('joomla.user.helper');
jimport('sourcecoast.utilities');

class JFBConnectControllerLoginRegister extends JControllerLegacy
{
    var $_newUserPassword = "";

    function display($cachable = false, $urlparams = false)
    {

        #JRequest::setVar('tmpl', 'component');
        parent::display();
    }

    function createNewUser()
    {
        $redirect = '';
        $menuItemId = 0;
        SCSocialUtilities::getCurrentReturnParameter($redirect, $menuItemId, LOGIN_TASK_JFBCONNECT);
        $returnParam = '&return=' . base64_encode($redirect);

        $app = JFactory::getApplication();
        if (!JRequest::checkToken())
            $app->redirect(JRoute::_('index.php?option=com_jfbconnect&view=loginregister' . $returnParam), 'Your session timed out. Please try again', 'error');

        // Save the whole POST data since we may need some of the fields after a redirection (on user creation failure)
        $this->getLoginPostData();

        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $fbUserId = $jfbcLibrary->getFbUserId();

        // User Registration Controller code
        JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_users/models');
        jimport('joomla.form.form');
        JForm::addFormPath(JPATH_SITE . '/components/com_users/models/forms');
        JForm::addFieldPath(JPATH_SITE . '/components/com_users/models/fields');
        $language = JFactory::getLanguage();
        $language->load('com_users');

        $userModel = JModelLegacy::getInstance('Registration', 'UsersModel');
        $requestData = JRequest::getVar('jform', array(), 'post', 'array');

        // Validate the posted data.
        $form = $userModel->getForm();
        if (!$form)
        {
            JError::raiseError(500, $userModel->getError());
            return false;
        }
        $formData = $userModel->validate($form, $requestData);

        // Check for validation errors.
        if ($formData === false)
        {
            // Get the validation messages.
            $errors = $userModel->getErrors();

            // Push up to three validation messages out to the user.
            for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
            {
                if (JError::isError($errors[$i]))
                {
                    $app->enqueueMessage($errors[$i]->getMessage(), 'warning');
                } else
                {
                    $app->enqueueMessage($errors[$i], 'warning');
                }
            }

            // Save the data in the session.
            $app->setUserState('com_users.registration.data', $requestData);

            // Redirect back to the registration screen.
            $app->redirect(JRoute::_('index.php?option=com_jfbconnect&view=loginregister' . $returnParam, false));
            return false;
        }

        // Register the user and return the newly created user ID
        $useractivation = $this->getActivationMode();
        $loginRegisterModel = $this->getModel('LoginRegister', 'JFBConnectModel');
        $jUser = $loginRegisterModel->register($formData, $useractivation);

        // Check for errors.
        if ($jUser === false)
        {
            // Save the data in the session.
            $app->setUserState('com_users.registration.data', $requestData);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $loginRegisterModel->getError()), 'warning');
            $this->setRedirect(JRoute::_('index.php?option=com_jfbconnect&view=loginregister' . $returnParam, false));
            return false;
        }

        $lang = JRequest::getVar(JApplication::getHash('language'), '', 'COOKIE');
        $jUser->setParam('language', $lang);
        $jUser->save();

        // Flush the data from the session.
        $app->setUserState('com_users.registration.data', null);
        $app->setUserState('com_jfbconnect.registration.data', null);

        #Send the new user confirmation email and admin notify emails
        $this->_newUserPassword = $formData['password1'];

        $jfbcLibrary->setInitialRegistration();
        SCSocialUtilities::clearJFBCNewMappingEnabled();

        include_once (JPATH_ADMINISTRATOR . '/components/com_jfbconnect/models/usermap.php');
        $userMapModel = new JFBConnectModelUserMap();
        if ($userMapModel->mapUser($fbUserId, $jUser->id))
            $app->enqueueMessage(JText::_('COM_JFBCONNECT_MAP_USER_SUCCESS'));
        else
            $app->enqueueMessage(JText::_('COM_JFBCONNECT_MAP_USER_FAIL'));

        $this->login();
    }

    private function getLoginPostData()
    {
        $postData = JRequest::get('post');

        if (isset($postData['jform']['password1']))
            $postData['jform']['password1'] = '';
        if (isset($postData['jform']['password2']))
            $postData['jform']['password2'] = '';

        $app = JFactory::getApplication();
        $app->setUserState('com_jfbconnect.registration.data', $postData);
    }

    function login()
    {
        $app = JFactory::getApplication();
        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $configModel = $jfbcLibrary->getConfigModel();
        $fbUserId = $jfbcLibrary->getFbUserId();

        if (!$jfbcLibrary->validateToken() || !$fbUserId)
        {
            $session = JFactory::getSession();
            $session->clear('jfbcTokenRequestCount'); // Let user retry again if they want
            $app->enqueueMessage(JText::_('COM_JFBCONNECT_UNABLE_TO_RETRIEVE_USER'));
            $app->redirect('index.php');
        }

        JPluginHelper::importPlugin('socialprofiles');
        $userMapModel = new JFBConnectModelUserMap();
        $jUserId = $userMapModel->getJoomlaUserId($fbUserId);

        // Check if no mapping, and Automatic Registration is set. If so, create the new user.
        if (!$jUserId && !$configModel->getSetting('create_new_users'))
        { # User is not in system, should create their account automatically
            if ($this->createFacebookOnlyUser($fbUserId))
                $jUserId = $userMapModel->getJoomlaUserId($fbUserId);
        }

        // Fetch and store a long-lived access token to the database for possible offline access later
        $fbClient = $jfbcLibrary->getFbClient();
        $token = $fbClient->getPersistentData('access_token', null);
        if ($token && $token != $fbClient->getApplicationAccessToken()) // Should always be valid, but caution is good.
        {
            // Store the access token for the user, which should have already been turned into a long-lived one
            $userMapModel->updateUserToken($jUserId, $token);
        }

        $doLogin = true;
        if ($jfbcLibrary->initialRegistration)
        {
            if ($jUserId)
            {
                $jUser = JUser::getInstance($jUserId);

                $doLogin = $this->activateUser(); // Set to false if user has to activate
                $this->sendNewUserEmails($jUser);

                # New user, set their new user status and trigger the OnRegister event
                $args = array('facebook', $jUser->get('id'), $fbUserId);
                $app->triggerEvent('socialProfilesOnRegister', $args);
                $jfbcLibrary->setFacebookNewUserMessage();
            } else
                $doLogin = false;
        }

        if ($doLogin && $jUserId)
        {
            $options = array('silent' => 1); // Disable other authentication messages
            $loginSuccess = $app->login(array('username' => $configModel->getSetting('facebook_app_id'), 'password' => $configModel->getSetting('facebook_secret_key')), $options);
            // TODO: Move this to the JFBCUser plugin, shouldn't have to check result here
            if ($loginSuccess)
            {
                if (!$jfbcLibrary->initialRegistration)
                {
                    $args = array('facebook', $jUserId, $fbUserId);
                    $app->triggerEvent('socialProfilesOnLogin', $args);
                    $jfbcLibrary->setFacebookLoginMessage();
                }
            }
        }

        $loginRegisterModel = $this->getModel('LoginRegister', 'JFBConnectModel');
        $redirect = $loginRegisterModel->getLoginRedirect();

        $app->redirect($redirect);
    }

    function createFacebookOnlyUser($fbUserId)
    {
        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $jfbcLibrary->setInitialRegistration();

        $configModel = $jfbcLibrary->getConfigModel();
        $loginRegisterModel = $this->getModel('LoginRegister', 'JFBConnectModel');

        $profileLibrary = JFBConnectProfileLibrary::getInstance();
        $fbUser = $profileLibrary->fetchProfile($fbUserId, array('first_name', 'last_name', 'email', 'name'));
        if ($fbUser == null) # no information returned from FB
            return false;

        if ($fbUser->get('email') == null)
            $newEmail = $fbUser->get('first_name') . "_" . $fbUserId . "@unknown.com";
        else
            $newEmail = $fbUser->get('email');

        $fullname = $fbUser->get('name');

        $user['fullname'] = $fullname;
        $user['email'] = $newEmail;

        // Create random password for FB User Only, but save so we can email to the user on account creation
        if ($configModel->getSetting('generate_random_password'))
        {
            $this->_newUserPassword = JUserHelper::genRandomPassword();
            $salt = JUserHelper::genRandomPassword(32);
            $crypt = JUserHelper::getCryptedPassword($this->_newUserPassword, $salt);
            $user['password'] = $crypt . ':' . $salt;
        } else
        {
            $user['password_clear'] = "";
            $this->_newUserPassword = '';
        }

        $lang = JRequest::getVar(JApplication::getHash('language'), '', 'COOKIE');
        $user['language'] = $lang;

        $usernamePrefixFormat = $configModel->getSetting('auto_username_format');
        $username = $loginRegisterModel->getAutoUsername($fbUser, $fbUserId, $usernamePrefixFormat);
        $user['username'] = $username;

        $useractivation = $this->getActivationMode();
        $jUser = $loginRegisterModel->getBlankUser($user, $useractivation);

        if ($jUser->get('id', null)) // If it's not set, there was an error
        {
            SCSocialUtilities::clearJFBCNewMappingEnabled();

            #Map the new user
            include_once (JPATH_ADMINISTRATOR . '/components/com_jfbconnect/models/usermap.php');
            $userMapModel = new JFBConnectModelUserMap();
            $app = JFactory::getApplication();
            if ($userMapModel->mapUser($fbUserId, $jUser->get('id')))
            {
                $app->enqueueMessage(JText::_('COM_JFBCONNECT_MAP_USER_SUCCESS'));
                return true;
            } else
                $app->enqueueMessage(JText::_('COM_JFBCONNECT_MAP_USER_FAIL'));
        }
        return false; // User creation failed for some reason
    }

    function activateUser()
    {
        $useractivation = $this->getActivationMode();
        $language = JFactory::getLanguage();
        $app = JFactory::getApplication();

        # Send out the new registration email
        // figure out activation
        $language->load('com_users');
        if ($useractivation == 2)
            $app->enqueueMessage(JText::_('COM_USERS_REGISTRATION_COMPLETE_VERIFY'));
        else if ($useractivation == 1)
            $app->enqueueMessage(JText::_('COM_USERS_REGISTRATION_COMPLETE_ACTIVATE'));

        if ($useractivation == 0)
            return true;
        else
            return false;
    }

    function getActivationMode()
    {
        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $configModel = $jfbcLibrary->getConfigModel();

        if ($configModel->getSetting('joomla_skip_newuser_activation'))
        {
            return 0;
        } else
        {
            $params = JComponentHelper::getParams('com_users');
            $useractivation = $params->get('useractivation');
            return $useractivation;
        }
    }


    public function loginMap()
    {
        JRequest::checkToken('post') or jexit(JText::_('JInvalid_Token'));
        $app = JFactory::getApplication();

        SCSocialUtilities::setJFBCNewMappingEnabled();
        $redirect = '';
        $menuItemId = 0;
        SCSocialUtilities::getCurrentReturnParameter($redirect, $menuItemId, LOGIN_TASK_JFBCONNECT);
        $returnParam = '&return=' . base64_encode($redirect);

        // Populate the data array:
        $data = array();
        $data['username'] = JRequest::getVar('username', '', 'method', 'username');
        $data['password'] = JRequest::getString('password', '', 'post', 'string', JREQUEST_ALLOWRAW);

        // Perform the log in.
        $error = $app->login($data);

        // Check if the log in succeeded.
        if (JError::isError($error) || $error == false)
        {
            $app->redirect(JRoute::_('index.php?option=com_jfbconnect&view=loginregister' . $returnParam, false));
        } else //Logged in successfully
        {
            /* Don't import on just a mapping update, for now. Need to investigate.
            $jUser = JFactory::getUser();
            $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
            $fbUserId = $jfbcLibrary->getMappedFbUserId();
            $args = array($jUser->get('id'), $fbUserId);

            JPluginHelper::importPlugin('jfbcprofiles');
            $app->triggerEvent('scProfilesImportProfile', $args);
            $app->enqueueMessage('Profile Imported!');*/

            JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_jfbconnect/' . 'models');
            $loginRegisterModel = JModelLegacy::getInstance('LoginRegister', 'JFBConnectModel');
            $redirect = $loginRegisterModel->getLoginRedirect();
            $app->redirect($redirect);
        }
    }

    function checkUserNameAvailable()
    {
        $username = JRequest::getString('username');
        $dbo = JFactory::getDBO();
        $query = "SELECT id FROM #__users WHERE username=" . $dbo->quote($username);
        $dbo->setQuery($query);
        $result = $dbo->loadResult();

        if ($result)
            echo false;
        else
            echo true;
        exit;
    }

    function checkEmailAvailable()
    {
        $email = JRequest::getString('email');
        $dbo = JFactory::getDBO();
        $query = "SELECT id FROM #__users WHERE email=" . $dbo->quote($email);
        $dbo->setQuery($query);
        $result = $dbo->loadResult();

        if ($result)
            echo false;
        else
            echo true;
        exit;
    }

    /*
    * Send message to user notifying them of the new account and if they have to activate.
    * If default Mail email and name are not set, grab it from a super admin in the DB.
    */
    function sendNewUserEmails(&$user)
    {
        $app = JFactory::getApplication();
        $sendEmail = true;
        $profileEmails = $app->triggerEvent('socialProfilesSendsNewUserEmails');
        foreach ($profileEmails as $pe)
        {
            if ($pe)
                $sendEmail = false;
        }
        if (!$sendEmail)
            return;

        $useractivation = $this->getActivationMode();

        $newEmail = $user->get('email');
        if (SCStringUtilities::endswith($newEmail, "@unknown.com"))
            return;

        // Compile the notification mail values.
        $config = JFactory::getConfig();
        $language = JFactory::getLanguage();
        $language->load('com_users');
        SCStringUtilities::loadLanguage('com_jfbconnect');

        $data = $user->getProperties();
        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');
        $data['siteurl'] = JUri::base();

        $uri = JURI::getInstance();
        $base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
        $data['activate'] = $base . JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $data['activation'], false);

        $emailSubject = JText::sprintf(
            'COM_USERS_EMAIL_ACCOUNT_DETAILS',
            $data['name'],
            $data['sitename']
        );

        // Handle account activation/confirmation emails.
        if ($useractivation == 2)
        {
            // Set the link to confirm the user email.
            if ($this->_newUserPassword == '')
            {
                $emailBody = JText::sprintf('COM_JFBCONNECT_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY_NOPASSWORD',
                    $data['name'],
                    $data['sitename'],
                        $data['siteurl'] . 'index.php?option=com_users&task=registration.activate&token=' . $data['activation'],
                    $data['siteurl']
                );
            } else
            {
                $emailBody = JText::sprintf('COM_JFBCONNECT_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY',
                    $data['name'],
                    $data['sitename'],
                        $data['siteurl'] . 'index.php?option=com_users&task=registration.activate&token=' . $data['activation'],
                    $data['siteurl'],
                    $data['username'],
                    $this->_newUserPassword
                );
            }
        } else if ($useractivation == 1)
        {
            // Set the link to activate the user account.
            if ($this->_newUserPassword == '')
            {
                $emailBody = JText::sprintf(
                    'COM_JFBCONNECT_EMAIL_REGISTERED_WITH_ACTIVATION_BODY_NOPASSWORD',
                    $data['name'],
                    $data['sitename'],
                        $data['siteurl'] . 'index.php?option=com_users&task=registration.activate&token=' . $data['activation'],
                    $data['siteurl']
                );
            } else
            {
                $emailBody = JText::sprintf(
                    'COM_JFBCONNECT_EMAIL_REGISTERED_WITH_ACTIVATION_BODY',
                    $data['name'],
                    $data['sitename'],
                        $data['siteurl'] . 'index.php?option=com_users&task=registration.activate&token=' . $data['activation'],
                    $data['siteurl'],
                    $data['username'],
                    $this->_newUserPassword
                );
            }
        } else
        {
            $emailSubject = JText::sprintf(
                'COM_USERS_EMAIL_ACCOUNT_DETAILS',
                $data['name'],
                $data['sitename']
            );

            if ($this->_newUserPassword == '')
                $emailBody = JText::sprintf('COM_JFBCONNECT_EMAIL_REGISTERED_BODY_NOPASSWORD', $data['name'], $data['sitename'], $data['siteurl']);
            else
                $emailBody = JText::sprintf('COM_JFBCONNECT_EMAIL_REGISTERED_BODY', $data['name'], $data['sitename'], $data['siteurl'], $data['username'], $this->_newUserPassword);
        }

        // Send the registration email.
        $return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);

        if ($useractivation < 2)
        {
            $emailSubject = JText::sprintf(
                'COM_USERS_EMAIL_ACCOUNT_DETAILS',
                $data['name'],
                $data['sitename']
            );

            $emailBodyAdmin = JText::sprintf(
                'COM_USERS_EMAIL_REGISTERED_NOTIFICATION_TO_ADMIN_BODY',
                $data['name'],
                $data['username'],
                $data['siteurl']
            );

            // get all admin users
            $query = 'SELECT name, email, sendEmail' .
                    ' FROM #__users' .
                    ' WHERE sendEmail=1';

            $db = JFactory::getDBO();
            $db->setQuery($query);
            $rows = $db->loadObjectList();

            // Send mail to all superadministrators id
            foreach ($rows as $row)
            {
                $return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBodyAdmin);

                // Check for an error.
                if ($return !== true)
                {
                    $this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
                    return false;
                }
            }
        }

        // Check for an error.
        if ($return !== true)
        {
            $this->setError(JText::_('COM_USERS_REGISTRATION_SEND_MAIL_FAILED'));

            // Send a system message to administrators receiving system mails
            $db = JFactory::getDBO();
            $q = "SELECT id
        FROM #__users
        WHERE block = 0
        AND sendEmail = 1";
            $db->setQuery($q);
            $sendEmail = $db->loadColumn();
            if (count($sendEmail) > 0)
            {
                $jdate = new JDate();
                // Build the query to add the messages
                $q = "INSERT INTO `#__messages` (`user_id_from`, `user_id_to`, `date_time`, `subject`, `message`)
            VALUES ";
                $messages = array();
                foreach ($sendEmail as $userid)
                {
                    $messages[] = "(" . $userid . ", " . $userid . ", '" . $jdate->toSql() . "', '" . JText::_('COM_USERS_MAIL_SEND_FAILURE_SUBJECT') . "', '" . JText::sprintf('COM_USERS_MAIL_SEND_FAILURE_BODY', $return, $data['username']) . "')";
                }
                $q .= implode(',', $messages);
                $db->setQuery($q);
                $db->execute();
            }
            return false;
        }
    }
}
