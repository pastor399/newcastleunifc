<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
jimport('sourcecoast.utilities');

class JFBConnectViewLoginRegister extends JViewLegacy
{
    var $_configModel;
    var $_loginRegisterModel;

    function display($tpl = null)
    {
        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $fbUserId = $jfbcLibrary->getFbUserId();
        $profileLibrary = JFBConnectProfileLibrary::getInstance();
        $fbUserProfile = $profileLibrary->fetchProfile($fbUserId, array('first_name', 'last_name', 'email', 'name'));
        $this->_configModel = $jfbcLibrary->getConfigModel();
        $this->_loginRegisterModel = $this->getModel('loginregister');

        if ($fbUserId == null)
        {
            $app = JFactory::getApplication();
            $app->redirect('index.php');
        }

        $app = JFactory::getApplication();
        JPluginHelper::importPlugin('socialprofiles');
        $args = array('facebook');
        $profileFields = $app->triggerEvent('socialProfilesOnShowRegisterForm', $args);

        // Get previously filled in values
        $app = JFactory::getApplication();
        $postData = $app->getUserState('com_jfbconnect.registration.data', array());

        $fbEmail1 = '';
        $fbEmail2 = '';

        SCUserUtilities::getDisplayEmail($postData, $fbUserProfile->get('email'), $fbEmail1, $fbEmail2);
        $fbUsername = SCUserUtilities::getDisplayUsername($postData, $fbUserProfile->get('first_name'), $fbUserProfile->get('last_name'), $fbEmail1, $fbUserId, $this->_configModel, $this->_loginRegisterModel);
        $fbPassword = SCUserUtilities::getDisplayPassword($this->_configModel, $this->_loginRegisterModel);
        $fbMemberName = SCUserUtilities::getDisplayNameByFullName($postData, $fbUserProfile->get('name'));

        $language = JFactory::getLanguage();
        $language->load('com_users');
        JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_users/models');
        $userModel = JModelLegacy::getInstance('Registration', 'UsersModel');
        $this->data = $userModel->getData();
        JForm::addFormPath(JPATH_SITE . '/components/com_users/models/forms');
        JForm::addFieldPath(JPATH_SITE . '/components/com_users/models/fields');
        $this->form = $userModel->getForm();

        // Setup the fields we can pre-populate
        // To do: Give option to show/hide the name on the form
        $this->form->setValue('name', null, $fbMemberName);

        $this->form->setValue('username', null, $fbUsername);
        if (!$this->_configModel->getSetting('registration_show_username') && $fbUsername != '')
        {
            $this->form->setFieldAttribute('username', 'type', 'hidden');
        }

        $this->form->setValue('email1', null, $fbEmail1);
        $this->form->setValue('email2', null, $fbEmail2);
        if (!$this->_configModel->getSetting('registration_show_email') && $fbEmail1 != '' && $fbEmail2 != '')
        {
            $this->form->setFieldAttribute('email1', 'type', 'hidden');
            $this->form->setFieldAttribute('email2', 'type', 'hidden');
        }

        if (!$this->_configModel->getSetting('registration_show_password') && $fbPassword != '')
        {
            $this->form->setValue('password1', null, $fbPassword);
            $this->form->setValue('password2', null, $fbPassword);
            $this->form->setFieldAttribute('password1', 'type', 'hidden');
            $this->form->setFieldAttribute('password2', 'type', 'hidden');
        }

        // Set an inputbox style on all the input elements so that inherited template styles look better
        $this->form->setFieldAttribute('name', 'class', 'inputbox');
        $this->form->setFieldAttribute('username', 'class', 'validate-username inputbox');
        $this->form->setFieldAttribute('email1', 'class', 'inputbox');
        $this->form->setFieldAttribute('email2', 'class', 'inputbox');
        $this->form->setFieldAttribute('password1', 'class', 'validate-password inputbox');
        $this->form->setFieldAttribute('password2', 'class', 'validate-password inputbox');

        //Check for form validation from each of the plugins
        $areProfilesValidating = $app->triggerEvent('socialProfilesAddFormValidation');
        $defaultValidationNeeded = true;
        foreach ($areProfilesValidating as $hasDoneValidation)
        {
            if ($hasDoneValidation == true)
            {
                $defaultValidationNeeded = false;
                break;
            }
        }

        //Check to see if JLinked is installed
        $jLinkedLoginButton = "";
        if (SCSocialUtilities::isJLinkedInstalled())
        {
            require_once(JPATH_ROOT . '/components/com_jlinked/libraries/linkedin.php');
            $jLinkedLibrary = JLinkedApiLibrary::getInstance();

            SCStringUtilities::loadLanguage('com_jlinked');
            $loginText = JText::_('COM_JLINKED_LOGIN_USING_LINKEDIN');

            $jLinkedLoginButton = '<link rel="stylesheet" href="components/com_jlinked/assets/jlinked.css" type="text/css" />';
            $jLinkedLoginButton .= '<div class="jLinkedLogin"><a href="' . $jLinkedLibrary->getLoginURL() . '"><span class="jlinkedButton"></span><span class="jlinkedLoginButton">' . $loginText . '</span></a></div>';
        }

        // Setup the view appearance
        // TODO: Make the addStyleSheet into a Utilities function to be used elsewhere.
        $displayType = $this->_configModel->getSetting('registration_display_mode');;
        $css = JPath::find($this->_path['template'], 'loginregister.css');
        $css = str_replace(JPATH_ROOT.'/', JURI::base(), $css);
        $doc = JFactory::getDocument();
        $doc->addStyleSheet($css);

        // Set the session bit to check for a new login on next page load
        SCSocialUtilities::setJFBCNewMappingEnabled();

        $this->assignRef('fbUserId', $fbUserId);
        $this->assignRef('fbUserProfile', $fbUserProfile);
        $this->assignRef('configModel', $this->_configModel);
        $this->assignRef('profileFields', $profileFields);
        $this->assignRef('jLinkedLoginButton', $jLinkedLoginButton);
        $this->assignRef('defaultValidationNeeded', $defaultValidationNeeded);
        $this->assignRef('displayType', $displayType);

        parent::display($tpl);
    }

}
