<?php

/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
?>
<h1><?php echo JText::_('COM_JFBCONNECT_TIMELINE_ACTIVITY_TITLE');?></h1>
<p><?php echo JText::_('COM_JFBCONNECT_TIMELINE_ACTIVITY_DESC');?></p>
<p><?php echo JText::_('COM_JFBCONNECT_TIMELINE_DELETE_DESC');?></p>
<form action="<?php echo JRoute::_('index.php?option=com_jfbconnect&view=opengraph&layout=activity'); ?>" method="post" name="adminForm">
    <div class="og_activity">
        <?php
        if (count($this->rows) == 0)
            echo '<div class="row"><strong>'.JText::_('COM_JFBCONNECT_TIMELINE_NOACTIVITY').'</strong></div>';

        else
        {
            foreach ($this->rows as $row) :
                $user = JFactory::getUser($row->user_id);
                $object = $this->objectModel->getObject($row->object_id);
                $action = $this->actionModel->getAction($row->action_id);
                ?>

                <div class="row">
                    <?php echo $action->display_name ?> : <a
                        href="<?php echo $row->url;?>"><?php echo $object->display_name ?></a>
                        <?php echo JText::_('COM_JFBCONNECT_TIMELINE_POSTEDON');?><?php echo strftime("%Y-%m-%d", strtotime($row->created)) ?>
                    <a href="<?php echo JURI::base(true); ?>/index.php?option=com_jfbconnect&controller=opengraph&task=userdelete&actionid=<?php echo $row->id;?>">
                        <img src="<?php echo JURI::base(true);?>/administrator/components/com_jfbconnect/assets/images/icon-16-deny.png"/>
                        </a>
                </div>
            <?php endforeach;
        }
        ?>
    </div>
    <div class="pagination">
        <?php echo $this->pagination->getListFooter(); ?>
    </div>
</form>
<p>
    <a href="<?php echo JRoute::_('index.php?option=com_jfbconnect&controller=opengraph&layout=settings'); ?>"><?php echo JText::_('COM_JFBCONNECT_TIMELINE_CHANGESETTINGS');?></a>
</p>