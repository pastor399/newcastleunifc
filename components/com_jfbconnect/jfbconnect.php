<?php
/**
 * @package		JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

require_once JPATH_ROOT . '/components/com_jfbconnect/libraries/facebook.php';

$view = JRequest::getCmd('view', '');

//TODO: Fix this logic to load the controller based on the view name always (mainly requires updating all links to front-end features)
if ($view == "loginregister" || $view == 'opengraph')
{
    require_once JPATH_COMPONENT . '/controllers/' . $view . '.php';
    $classname = 'JFBConnectController'.$view;
}
else
{
    $controller = JRequest::getCmd('controller', '');
    if ($controller != '')
        require_once(JPATH_COMPONENT . '/controllers/' . $controller . ".php");

    require_once JPATH_COMPONENT . '/controller.php';
    $classname = 'JFBConnectController' . $controller;
}

$controller = new $classname();

$controller->execute(JRequest::getCmd('task'));
$controller->redirect();