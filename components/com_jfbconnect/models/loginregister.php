<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');
jimport('joomla.plugin.helper');
jimport('sourcecoast.utilities');

class JFBConnectModelLoginRegister extends JModelLegacy
{
    /**
     * Method to save the form data. Primary implementation from Joomla 1.6 com_users/models/registration.php
     *
     * @param    array        The form data.
     * @return    mixed        The user id on success, false on failure.
     * @since    1.6
     */
    public function register($temp, $useractivation)
    {
        // Initialise the table with JUser.
        JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_users/models');
        $userModel = JModelLegacy::getInstance('Registration', 'UsersModel');

        $user = new JUser;
        $data = (array)$userModel->getData();

        // Merge in the registration data.
        foreach ($temp as $k => $v)
        {
            $data[$k] = $v;
        }

        // Prepare the data for the user object.
        $data['email'] = $data['email1'];
        $data['password'] = $data['password1'];

        // Check if the user needs to activate their account.
        if (($useractivation == 1) || ($useractivation == 2))
        {
            jimport('joomla.user.helper');
            $data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());
            $data['block'] = 1;
        }

        // Bind the data.
        if (!$user->bind($data))
        {
            $this->setError(JText::sprintf('COM_USERS_REGISTRATION_BIND_FAILED', $user->getError()));
            return false;
        }

        // Load the users plugin group.
        JPluginHelper::importPlugin('user');

        // Store the data.
        if (!$user->save())
        {
            $this->setError(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $user->getError()));
            return false;
        }

        return $user;
    }

    function getAutoUsername($fbUser, $fbUserId, $usernamePrefixFormat)
    {
        return SCUserUtilities::getAutoUsername($fbUser->get('first_name'), $fbUser->get('last_name'), $fbUser->get('email'), 'fb_', $fbUserId, $usernamePrefixFormat);
    }

    function &getBlankUser($user, $activationMode)
    {
        jimport('joomla.application.component.helper');
        $config = JComponentHelper::getParams('com_users');

        $instance = JUser::getInstance();
        // Default to Registered.
        $defaultUserGroup = $config->get('new_usertype', 2);
        $instance->set('usertype', 'deprecated');
        $instance->set('groups', array($defaultUserGroup));

        $instance->set('id', 0);
        $instance->set('name', $user['fullname']);
        $instance->set('username', $user['username']);
        if($user['password'] != "")
            $instance->set('password', $user['password']);
        else
            $instance->set('password_clear', $user['password_clear']);
        $instance->set('email', $user['email']); // Result should contain an email (check)
        $instance->setParam('language', $user['language']);

        if ($activationMode != 0)
        {
            jimport('joomla.user.helper');
            $instance->set('activation', JApplication::getHash(JUserHelper::genRandomPassword()));
            $instance->set('block', 1);
        }
        else
            $instance->set('block', 0);


        if (!$instance->save())
        {
            return JError::raiseWarning('SOME_ERROR_CODE', $instance->getError());
        }

        return $instance;
    }

    public function getLoginRedirect()
    {
        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $configModel = $jfbcLibrary->getConfigModel();
        $app =JFactory::getApplication();
        
        if ($jfbcLibrary->initialRegistration && $configModel->getSetting('facebook_new_user_redirect_enable'))
        {
            $itemId = $configModel->getSetting('facebook_new_user_redirect', '0');
            $menu =& $app->getMenu();
            $item =& $menu->getItem($itemId);
            $link = $item->link . "&Itemid=" . $itemId;
            $redirect = JRoute::_($link, false);
        }
        else if ($configModel->getSetting('facebook_login_redirect_enable'))
        {
            $itemId = $configModel->getSetting('facebook_login_redirect', '0');
            $menu =& $app->getMenu();
            $item =& $menu->getItem($itemId);
            $link = $item->link . "&Itemid=" . $itemId;
            $redirect = JRoute::_($link, false);
        }
        else
        {
            $redirect = $this->getNewUserRedirectionURL();
        }

        return $redirect;
    }

    function getNewUserRedirectionURL()
    {
        $redirect = ''; $menuItemId = 0;
        SCSocialUtilities::getCurrentReturnParameter($redirect, $menuItemId, LOGIN_TASK_JFBCONNECT);
        return $redirect;
    }

    function generateRandomPassword()
    {
        $newUnencryptedPassword = '';
        SCSocialUtilities::getRandomPassword($newUnencryptedPassword);
        return $newUnencryptedPassword;
    }

    //TODO cleanup later?
    function generateUsername($firstName, $lastName, $email, $liMemberId, $usernamePrefixFormat)
    {
        return SCUserUtilities::getAutoUsername($firstName, $lastName, $email, "fb_", $liMemberId, $usernamePrefixFormat);
    }
}

?>
