<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('sourcecoast.utilities');

// Probably should work toward extending the JFBConnectFacebookLibrary for this class.
// Too much intertwined in the root library right now.
class JFBConnectProfileLibrary
{
    static $profileFields = array(
        '0' => 'None',
        'name' => 'User - Full Name',
        'first_name' => 'User - First Name',
        'middle_name' => 'User - Middle Name',
        'last_name' => 'User - Last Name',
        'profile_url' => 'User - Profile Link',
        'hometown_location.city' => 'Basic Info - Hometown City',
        'hometown_location.state' => 'Basic Info - Hometown State',
        'hometown_location.country' => 'Basic Info - Hometown Country',
        'hometown_location.name' => 'Basic Info - Hometown City/State', // user_hometown
        'current_location.city' => 'Basic Info - Current City',
        'current_location.state' => 'Basic Info - Current State',
        'current_location.country' => 'Basic Info - Current Country',
        'current_location.name' => 'Basic Info - Current City/State', // user_location
        'timezone' => 'Basic Info - Timezone',
        'sex' => 'Basic Info - Sex (Male / Female)',
        'birthday' => 'Basic Info - Birthday', // user_birthday
        'political' => 'Basic Info - Political View', // user_religion_politics
        'religion' => 'Basic Info - Religious Views', // user_religion_politics
        'about_me' => 'Basic Info - Bio', // user_about_me
        'profile_blurb' => 'Basic Info - Profile Blurb', // user_about_me
        'quotes' => 'Basic Info - Favorite Quotes', // user_about_me
        'music' => 'Likes & Interests - Music', // user_likes
        'books' => 'Likes & Interests - Books', // user_likes
        'movies' => 'Likes & Interests - Movies', // user_likes
        'tv' => 'Likes & Interests - TV', // user_likes
        'games' => 'Likes & Interests - Games', //user_likes
        'activities' => 'Likes & Interests - Activities', // user_activities
        'interests' => 'Likes & Interests - Interests', // user_interests
        'relationship_status' => 'Relationship - Relationship Status', // user_relationships
        //'significant_other_id' => 'Relationship - Significant Other',
        //'meeting_sex' => 'Relationship - Type of Relationship Looking For', // 3.0.2
        //'meeting_for' => 'Relationship - Reasons For Looking', // 3.0.2
        //'affiliations' => 'Network Affiliations', // 3.0.2
        'work.0.employer.name' => 'Education and Work - Employer', // user_work_history
        'work.0.location.name' => 'Education and Work - Location', // user_work_history
        'work.0.position.name' => 'Education and Work - Position', // user_work_history
        'work.0.start_date' => 'Education and Work - Start Date', // user_work_history
        'work.0.end_date' => 'Education and Work - End Date', // user_work_history
        'education.College.school.name' => 'Education and Work - College Name',
        'education.College.concentration.0.name' => 'Education and Work - College Degree',
        'education.College.year.name' => 'Education and Work - College Year',
        'education.High School.school.name' => 'Education and Work - High School', // user_education_history
        'education.High School.year.name' => 'Education and Work - High School Year', // user_education_history
        'email' => 'Contact - Email', // email
        'website' => 'Contact - Website' // user_website
    );

    public function getPermissionsForFields($fields)
    {
        $perms = array();
        if (!$fields)
            return $perms;

        foreach ($fields as $field)
        {
            if (strpos($field, "current_location") !== false)
                $perms[] = "user_location";
            else if (strpos($field, "hometown_location") !== false)
                $perms[] = "user_hometown";
            else if ($field == "activities" || $field == "birthday" || $field == "interests" || $field == "website")
                $perms[] = "user_" . $field;
            else if ($field == "about_me" || $field == "quotes" || $field == "profile_blurb")
                $perms[] = "user_about_me";
            else if ($field == "religion" || $field == "political")
                $perms[] = "user_religion_politics";
            else if ($field == "relationship_status")
                $perms[] = "user_relationships";
            else if ($field == "music" || $field == "books" || $field == "movies" || $field == "tv" || $field == "games")
                $perms[] = "user_likes";
            else if (strpos($field, "work") !== false)
                $perms[] = "user_work_history";
            else if (strpos($field, "education") !== false)
                $perms[] = "user_education_history";
        }
        return $perms;
    }

    /**
     *  Get all permissions that are required by Facebook for email, status, and/or profile, regardless
     *    of whether they're set to required in JFBConnect
     * @return string Comma separated list of FB permissions that are required
     */
    static private $requiredScope;

    static function getRequiredScope()
    {
        if (self::$requiredScope)
            return self::$requiredScope;

        self::$requiredScope = array();
        self::$requiredScope[] = "email";
        // Query to see if any actions are enabled. If so, request the proper permission
        $db = JFactory::getDBO();
        $db->setQuery("SELECT COUNT(*) FROM #__opengraph_action WHERE published = 1");
        $count = $db->loadResult();
        if ($count > 0)
            self::$requiredScope[] = "publish_actions";

        /*        // Check if any of the login/register wall posts are set, which require the publish_stream perm
  if ($this->getSetting('facebook_new_user_status_msg') != "" ||
      $this->getSetting('facebook_new_user_status_link') != "" ||
      $this->getSetting('facebook_new_user_status_picture') != "" ||
      $this->getSetting('facebook_login_status_msg') != "" ||
      $this->getSetting('facebook_login_status_link') != "" ||
      $this->getSetting('facebook_login_status_picture') != ""
  )
      self::$requiredScope[] = "publish_stream";*/

        JPluginHelper::importPlugin('socialprofiles');
        $app = JFactory::getApplication();
        $args = array('facebook');
        $perms = $app->triggerEvent('socialProfilesGetRequiredScope', $args);
        if ($perms)
        {
            foreach ($perms as $permArray)
                self::$requiredScope = array_merge(self::$requiredScope, $permArray);
        }

        $configModel = JFBConnectModelConfig::getSingleton();
        $customPermsSetting = $configModel->getSetting('facebook_perm_custom');
        if ($customPermsSetting != '')
        {
            //Separate into an array to be able to merge and then take out duplicates
            $customPerms = explode(',', $customPermsSetting);
            foreach ($customPerms as $customPerm)
                self::$requiredScope[] = trim($customPerm);
        }

        self::$requiredScope = array_unique(self::$requiredScope);
        self::$requiredScope = implode(",", self::$requiredScope);

        return self::$requiredScope;
    }

    static $instance;

    static public function getInstance()
    {
        if (!self::$instance)
            self::$instance = new JFBConnectProfileLibrary();

        return self::$instance;
    }

    /*
     * Fetch a user's profile based on a profile plugin field-mapping
     * @return JRegistry with profile field values
     */
    public function fetchProfileFromFieldMap($fieldMap, $permissionGranted = true)
    {
        $fields = array();
        if (is_object($fieldMap))
        {
            foreach ($fieldMap as $field)
            {
                $fieldArray = explode('.', $field);
                if (!empty($fieldArray[0]))
                    $fields[] = $fieldArray[0]; // Get the root field to grab from FB
            }
        }
        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $fbUserId = $jfbcLibrary->getFbUserId();

        $fields = array_unique($fields);
        $profile = $this->fetchProfile($fbUserId, $fields);
        $profile->setFieldMap($fieldMap);
        return $profile;

    }

    /* Fetch a user's profile based on the passed in array of fields
    * @return JRegistry with profile field values
    */
    public function fetchProfile($fbUserId, $fields)
    {
        $profile = new JFBConnectProfileData();
        if (!empty($fields))
        {
            $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
            $colFields = implode(",", $fields);
            $fql = "SELECT " . $colFields . " FROM user WHERE uid=" . $fbUserId;
            $params = array(
                'method' => 'fql.query',
                'query' => $fql,
            );
            $data = $jfbcLibrary->rest($params, TRUE);

            $profile->loadObject($data[0]);
        }
        return $profile;
    }

    public function fetchStatus()
    {
        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $response = $jfbcLibrary->api('/me/statuses');
        if (!isset($response['data'][0]))
            return;
        $socialStatus = $response['data'][0]['message'];
        return $socialStatus;
    }


    // Created for parity with JLinked/SourceCoast library
    // nullForDefault - If the avatar is the default image for the social network, return null instead
    // Prevents the default avatars from being imported
    function getAvatarUrl($fbUserId, $nullForDefault = true)
    {
        $type = 'pic_big'; // Can also do 'pic_big_with_logo'. Need a setting for that one day (Trac #407)
        $fql = "SELECT " . $type . " FROM user WHERE uid = " . $fbUserId;
        $params = array(
            'method' => 'fql.query',
            'query' => $fql,
        );
        $jfbcLibrary = JFBConnectFacebookLibrary::getInstance();
        $profileUrl = $jfbcLibrary->rest($params, FALSE);
        $avatarUrl = $profileUrl[0][$type];

        //No avatar ends with .gif
        if ($nullForDefault && SCStringUtilities::endswith($avatarUrl, '.gif'))
            $avatarUrl = null;

        return $avatarUrl;
    }

}

class JFBConnectProfileData extends JRegistry
{
    var $fieldMap;

    function get($path, $default = null)
    {
        if ($this->exists($path))
            $data = parent::get($path, $default);
        else
        {
            // Alternative fields that require extra parsing
            $parts = explode('.', $path);
            if ($parts[0] == 'education')
            {
                $edu = $this->data->education;
                foreach ($edu as $k => $node)
                {
                    if ($node->type == $parts[1])
                    {
                        unset($parts[0]);
                        unset($parts[1]);
                        $newPath = 'education.'.$k.'.'.implode('.', $parts);
                        $data = parent::get($newPath, $default);
                        break;
                    }
                }
            }
            else
                return $default;
        }

        if (!empty($data))
        {
            if (is_array($data))
            { // This is a field with multiple, comma separated values
                // Remove empty values to prevent blah, , blah as output
                unset($data['id']); // Remove id key which is useless to import
                $data = SCStringUtilities::r_implode(', ', $data);
            }
            // add custom field handlers here
            switch ($path)
            {
                case 'website':
                    $websites = explode("\n", $data);
                    if (count($websites) > 0)
                        $data = trim($websites[0]);
                    break;
            }
        }

        return $data;
    }

    function setFieldMap($fieldMap)
    {
        $this->fieldMap = $fieldMap;
    }

    function getFieldWithUserState($field)
    {
        $val = JRequest::getVar($field, '', 'POST');
        // Check if there's a session variable from a previous POST, and use that
        if (empty($val))
        {
            $app = JFactory::getApplication();
            $prevPost = $app->getUserState('com_jfbconnect.registration.data', array());
            if (array_key_exists($field, $prevPost))
                $val = $prevPost[$field];
        }

        if (empty($val))
            $val = $this->getFieldFromMapping($field);

        return $val;
    }

    function getFieldFromMapping($field)
    {
        if (!property_exists($this->fieldMap, $field))
            return "";

        $fieldName = $this->fieldMap->$field;
        return $this->get($fieldName, "");
    }

    /* bindData
     * Overridden function due to Joomla's checking of each variable to see if it's an associative array or not
     * We don't care, we want all arrays to be translated to a class
     */
    protected function bindData($parent, $data)
    {
        // Ensure the input data is an array.
        if (is_object($data))
        {
            $data = get_object_vars($data);
        } else
        {
            $data = (array)$data;
        }

        foreach ($data as $k => $v)
        {
            if (is_array($v) || is_object($v))
            {
                $parent->$k = new stdClass;
                $this->bindData($parent->$k, $v);
            } else
            {
                $parent->$k = $v;
            }
        }
    }
}