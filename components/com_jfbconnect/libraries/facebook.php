<?php
/**
 * @package        JFBConnect
 * @copyright (C) 2009-2013 by Source Coast - All rights reserved
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('sourcecoast.utilities');

include_once (JPATH_ADMINISTRATOR . '/components/com_jfbconnect/models/usermap.php');
include_once (JPATH_SITE . '/components/com_jfbconnect/libraries/profile.php');

class JFBConnectFacebookLibrary extends JObject
{

    // Override model's getInstance to really only get the instance
    var $facebookAppId;
    var $facebookSecretKey;
    protected $configModel;
    protected $fbUserId;
    protected $mappedFbUserId;
    private static $libraryInstance;
    private $apiError;

    public static function getInstance()
    {
        if (!isset(self::$libraryInstance))
        {
            self::$libraryInstance = new JFBConnectFacebookLibrary();
            // After the instance is grabbed, get the Javascript code to insert
            // Don't do this in the constructor as inititing the Javascript calls functions which need
            //   this very instance
            $app = JFactory::getApplication();
            if (!$app->isAdmin())
            {
                // Set whether, on this page load, the user should be checked for a new mapping (i.e. they were just on the login/register page)
                $session = JFactory::getSession();
                $checkNewMapping = $session->get('jfbcCheckNewMapping', false);
                self::$libraryInstance->checkNewMapping = $checkNewMapping;
            }
        }

        return self::$libraryInstance;
    }

    function __construct()
    {
        $this->getConfigModel();
        $this->facebookAppId = $this->configModel->getSetting('facebook_app_id');
        $this->facebookSecretKey = $this->configModel->getSetting('facebook_secret_key');
    }

    function getConfigModel()
    {
        if (!$this->configModel)
        {
            require_once(JPATH_ADMINISTRATOR . '/components/com_jfbconnect/models/config.php');
            $this->configModel = new JFBConnectModelConfig();
        }
        return $this->configModel;
    }

    var $initialRegistration = false;

    function setInitialRegistration()
    {
        $this->initialRegistration = true;
    }

    /**
     * Perform initialization of JFBConnect variables into the document. Currently adds:
     * ** The (dynamic) login/logout redirects, used by jfbconnect.js
     * ** The {scopengraphplaceholder} tag to be replaced/removed by the system plugin
     * @return none
     */
    function initDocument()
    {
        //$fbClient = $this->getFbClient();
        $doc = JFactory::getDocument();
        if ($doc->getType() != 'html')
            return; // Only insert javascript on HTML pages, not AJAX, RSS, etc

        $return = '';
        $menuItemId = 0;
        SCSocialUtilities::getCurrentReturnParameter($return, $menuItemId, LOGIN_TASK_JFBCONNECT);

        $requiredPerms = JFBConnectProfileLibrary::getRequiredScope();

        $fbUserId = $this->getMappedFbUserId();
        $logoutJoomlaOnly = $this->configModel->getSetting('logout_joomla_only');

        if ($fbUserId && !$logoutJoomlaOnly)
            $logoutFacebookJavascript = "jfbc.login.logout_facebook = true;";
        else
            $logoutFacebookJavascript = "jfbc.login.logout_facebook = false;";

        $user = JFactory::getUser();
        $showLoginModal = $this->configModel->getSetting('facebook_login_show_modal');
        if ($showLoginModal && $user->guest)
            JHTML::_('behavior.modal');

        $doc->addScript(JURI::base(true) . '/components/com_jfbconnect/includes/jfbconnect.js?v=434'); //4.3.4
        $doc->addCustomTag('<script type="text/javascript">' .
                $logoutFacebookJavascript . "\n" .
                "jfbc.base = '" . JURI::base() . "';\n" .
                "jfbc.return_url = '" . base64_encode($return) . "';\n" .
                "jfbc.login.scope = '" . $requiredPerms . "';\n" .
                "jfbc.login.show_modal = '" . $showLoginModal . "';\n" .
                "jfbc.login.auto = '" . $this->configModel->getSetting('facebook_auto_login') . "';\n" .
                "</script>");

        $doc->addCustomTag('<SCOpenGraphPlaceholder />');
    }

    function getFbClient()
    {
        static $_facebook;
        if (!isset($_facebook))
        {
            include_once(JPATH_ADMINISTRATOR . '/components/com_jfbconnect/assets/facebook-api/facebook.php');
            $_facebook = new JFBCFacebook(array(
                'appId' => $this->facebookAppId,
                'secret' => $this->facebookSecretKey,
                'cookie' => true,
            ));
        }

        return $_facebook;
    }

    function validateToken()
    {
        $fbUserId = $this->getFbUserId();
        $profileLibrary = JFBConnectProfileLibrary::getInstance();

        $fbUser = $profileLibrary->fetchProfile($fbUserId, array(0 => 'first_name', 1 => 'last_name'));
        if ($fbUser == null)
        {
            $session = JFactory::getSession();
            $validateCount = $session->get('jfbcTokenRequestCount', 0);
            if ($validateCount >= 1)
                return false;

            $session->set('jfbcTokenRequestCount', 1);

            $redirect = '';
            $menuItemId = 0;
            SCSocialUtilities::getCurrentReturnParameter($redirect, $menuItemId, LOGIN_TASK_JFBCONNECT);
            $returnParam = '&return=' . base64_encode($redirect);

            $uri = JURI::getInstance();
            $loginLink = $uri->toString(array('scheme', 'host')) . JRoute::_('index.php?option=com_jfbconnect&task=loginFacebookUser' . $returnParam, false);
            $requiredPerms = JFBConnectProfileLibrary::getRequiredScope();
            $fbClient = $this->getFbClient();
            $url = $fbClient->getLoginUrl(
                array(
                    'scope' => $requiredPerms,
                    'redirect_uri' => $loginLink,
                    'cancel_uri' => $loginLink
                )
            );
            $app = JFactory::getApplication();
            $app->redirect($url);
        }
        return true;
    }

    /*
     * TODO: Remove this function
     * Here for backward compatibility. Added in JFBConnect v5.0. Remove in 5.1+
     */
    function getUserProfile($fbUserId, $fields)
    {
        $profileLibrary = JFBConnectProfileLibrary::getInstance();
        return $profileLibrary->fetchProfile($fbUserId, $fields);
    }

    function api($api, $params = null, $callAsUser = true, $method = null, $suppressErrors = false)
    {
        $this->apiError = null;
        $fbClient = $this->getFbClient();
        if (!$method)
        {
            if ($params)
                $method = "POST";
            else
                $method = "GET";
        }

        if (!$callAsUser)
            $params['access_token'] = $this->facebookAppId . "|" . $this->facebookSecretKey;
        /*        else if (!$params || (is_array($params) && (!array_key_exists('access_token', $params))))
        {
            // Get the access token for the current user
            $jUser = JFactory::getUser();
            $userMapModel = new JFBConnectModelUserMap();
            $userMapModel->getData($jUser->get('id'));
            $accessToken = $userMapModel->_data->access_token;
            if ($accessToken != '' && $accessToken != null)
                $params['access_token'] = $accessToken;
        }*/

        try
        {
            if ($params != null) // Graph API call with paramters (either App call or POST call)
                $apiData = $fbClient->api($api, $method, $params);
            else // Graph API call to only get data
                $apiData = $fbClient->api($api);
        } catch (JFBCFacebookApiException $e)
        {
            $this->apiError = $e->getMessage();
            // Only display errors on the front-end if the config is set to do so
            $app = JFactory::getApplication();
            if (!$suppressErrors && ($app->isAdmin() || $this->configModel->getSetting('facebook_display_errors')))
            {
                $app->enqueueMessage(JText::_('COM_JFBCONNECT_FB_API_ERROR') . $e->getMessage(), 'error');
            }
            $apiData = null;
        }

        return $apiData;
    }

    public function getLastError()
    {
        $return = $this->apiError;
        $this->apiError = null;
        return $return;
    }

    function rest($params, $callAsUser = true)
    {
        $fbClient = $this->getFbClient();
        if (!$callAsUser)
            $params['access_token'] = $this->facebookAppId . "|" . $this->facebookSecretKey;

        try
        {
            $result = $fbClient->api($params);
        } catch (JFBCFacebookApiException $e)
        {
            // Only display errors on the front-end if the config is set to do so
            $app = JFactory::getApplication();
            if ($app->isAdmin() || $this->configModel->getSetting('facebook_display_errors'))
            {
                $app->enqueueMessage(JText::_('COM_JFBCONNECT_FB_API_ERROR') . $e->getMessage(), 'error');
            }
            $result = null;
        }

        // This should be decoded by the Facebook api, but for some reason, it returns not perfect
        // JSON encoding (difference between admin.getAppProperties and a FQL query
        // So, check if we're just getting a string and try a 2nd JSON decode, which seems to work.
        // .. ugh.
        if (is_string($result))
            $result = json_decode($result, true);

        return $result;
    }

    function getLoginButton($buttonSize = "medium", $showFaces = "false", $maxRows = '')
    {
        $perms = JFBConnectProfileLibrary::getRequiredScope();
        if ($perms != "")
            $perms = 'data-scope="' . $perms . '"'; // OAuth2 calls them 'scope'

        if ($showFaces == "1" || $showFaces == "true")
            $showFaces = 'data-show-faces="true" ';
        else
            $showFaces = 'data-show-faces="false" ';

        if ($maxRows != "")
            $maxRows = 'data-max-rows="' . $maxRows . '" ';

        SCStringUtilities::loadLanguage('com_jfbconnect');
        return '<div class="fb-login-button" '
                . 'data-size="' . $buttonSize . '" '
                . $showFaces
                . $maxRows
                . $perms
                . ' onlogin="javascript:jfbc.login.on_login();">'
                . JText::_('COM_JFBCONNECT_LOGIN_USING_FACEBOOK')
                . '</div>';
    }

    function getLogoutButton()
    {
        SCStringUtilities::loadLanguage('com_jfbconnect');
        $logoutStr = JText::_('COM_JFBCONNECT_LOGOUT');

        return '<input type="submit" name="Submit" id="jfbcLogoutButton" class="button" value="' . $logoutStr . '" onclick="javascript:jfbc.login.logout_button_click()" />';
    }

    /* getFbUserId
    * Gets the FB user id of user logged into Facebook. This is regardless of whether they are mapped to an
    *  existing Joomla account.
    * Use getMappedFbUserId if you want the FB ID for a user who is mapped.
    */
    function getFbUserId()
    {
        if ($this->get('fbUserId', null) == null)
        {
            $fbClient = $this->getFbClient();
            $fbId = $fbClient->getUser();
            if ($fbId != 0 && $fbId != null)
                $this->set('fbUserId', $fbId);
            else
                $this->set('fbUserId', null);
        }
        return $this->get('fbUserId');

    }

    /* getMappedFbUserId
    * Gets the FB user id of user logged into Facebook if they have a usermapping to a Joomla user
    * Returns null if user is not mapped (or not logged into Facebook).
    */
    function getMappedFbUserId()
    {
        $mappedFbUserId = $this->getFbUserId();
        $userMapModel = new JFBConnectModelUserMap();
        $jUser = JFactory::getUser();

        if ($userMapModel->getJoomlaUserId($mappedFbUserId) != $jUser->get('id') || $jUser->guest)
            $mappedFbUserId = null;

        $this->set('mappedFbUserId', $mappedFbUserId);
        return $this->get('mappedFbUserId');
    }

    public function userIsConnected()
    {
        static $connected = null;
        if ($connected == null)
            $connected = $this->getMappedFbUserId() == null ? false : true;

        return $connected;
    }

    function setFacebookMessage($message)
    {
        if ($message)
        {
            try
            {
                $currentMessage = '';

                $response = $this->api('/me/feed');
                if (isset($response['data'][0]))
                    $currentMessage = $response['data'][0]['message'];

                if ($currentMessage != $message['message'])
                {
                    if (is_array($message))
                        $response = $this->api('/me/feed', $message);
                    else
                        $response = $this->api('/me/feed', array('message' => $message));
                }
            } catch (JFBCFacebookApiException $e)
            {
                /*
                 Fatal error: Uncaught exception 'FacebookRestClientException' with message
                 'Updating status requires the extended permission status_update' in
                 .../com_jfbconnect/assets/facebook-api/facebookapi_php5_restlib.php:3007
                */
            }
        }
    }

    function setFacebookNewUserMessage()
    {
        $message = $this->configModel->getSetting('facebook_new_user_status_msg');
        $link = $this->configModel->getSetting('facebook_new_user_status_link');
        $picture = $this->configModel->getSetting('facebook_new_user_status_picture');

        if ($message == "")
            return;

        $post = array();
        $post['message'] = $message;
        if ($link != '')
            $post['link'] = $link;
        if ($picture != '')
            $post['picture'] = $picture;

        $this->setFacebookMessage($post);
    }

    function setFacebookLoginMessage()
    {
        $message = $this->configModel->getSetting('facebook_login_status_msg');
        $link = $this->configModel->getSetting('facebook_login_status_link');
        $picture = $this->configModel->getSetting('facebook_login_status_picture');

        if ($message == "")
            return;

        $post = array();
        $post['message'] = $message;
        if ($link != '')
            $post['link'] = $link;
        if ($picture != '')
            $post['picture'] = $picture;

        $this->setFacebookMessage($post);
    }

    function getFacebookOverrideLocale()
    {
        return $this->configModel->getSetting('facebook_language_locale');
    }

    function getSocialTagRenderKey()
    {
        return $this->configModel->getSetting('social_tag_admin_key');
    }

}