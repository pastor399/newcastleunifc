# Newcastle University Football Club
 
The University Of Newcastle Football Club's website is constantly a work in progress, new features, bug fixes
and testing.  This repository provies a central base for code to be updated and tested on a development enviroment
before it is sent to the server for Live! access.
 
 
## Development Envirmoent
 
#### Heroku
 
##### `http://www.heroku.com/` is a highly scalable deployment enviroment.  Using Droid.IO this bitBucket repository is automatically pushed onto the
Heroku cloud platform where it is available for testing, with access to mySQL clearDB.
 

##### Preview
 
The development website can be viewed at `http://newcastleunifc.herokuapp.com/`.
The enviroment is scaled right down to provide a Free! service, but this may result in long loading times. 

    :::python
        Copyright:
        * Copyright (C) 2013 Newcastle University Football Club. All rights reserved.